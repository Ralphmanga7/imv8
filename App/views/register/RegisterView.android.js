import React, { Component } from "react";
import { GlobalStyles } from "../../resources/style/style";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Image,
  TouchableOpacity,
  StatusBar,
  TextInput,
  ScrollView,
  Keyboard,
  ActivityIndicator,
  Alert,
  AsyncStorage
} from "react-native";
import LinearGradient from "react-native-linear-gradient";
import i18n from "../../AppGlobalConfig/Localization/i18n";
import { Signup, Login } from "../../services/UserService";
import * as Animatable from "react-native-animatable";
import { Form } from "native-base";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import Email from "../UtilsComponents/Inputs/Email";
import Name from "../UtilsComponents/Inputs/Name";
import Phone from "../UtilsComponents/Inputs/phone";
import Password from "../UtilsComponents/Inputs/Password";
import Btn from "../UtilsComponents/Buttons/Button";
import InputText from "../UtilsComponents/Inputs/InputText";
import ComeBack from "../UtilsComponents/Buttons/comeBack";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";

export default class LoginSiginView extends Component {
  constructor(props) {
    super(props);

    this.state = {
      inputs: []
    };

    this.state.formError = "";
  }
  goToRecord = () => {
    this.props.navigation.navigate("MyAudioView");
  };

  changeInputFocus = index => () => {
    try{
    if (index === 0) {
      // console.warn(this.state.inputs[index + 1]);
      this.state.inputs[index + 1].state.inputRef.focus(); // eslint-disable-line
    } else if (index != this.state.inputs.length - 1){
      if(this.state.inputs[index + 1])
      this.state.inputs[index + 1].state.inputRef.focus();
    }
    else this.Signup();
    }catch(e){
      console.warn(e);
    }

  };

  updateCanRegisterState = input => {
    console.warn(input, "password");
  };

  noAvaiblable = () => {
    //alert(i18n.t("globals.no-available"));
  };

  goToHome = () => {
    this.props.navigation.navigate("AuthLoading");
  };

  goToLogin = () => {
    this.props.navigation.navigate("AuthLoading");
  };

  show_loading = () => {
    if (this.state.loading) {
      return (
        <View style={[Activity.container, Activity.horizontal]}>
          <ActivityIndicator size="large" color="#FFFFFF" />
        </View>
      );
    }
  };

  errorTemplate = () => {
    if (this.state.formError != "") {
      return (
        <View style={[GlobalStyles.containerRow]}>
          <Text
            style={[
              { color: "red", paddingBottom: 10 },
              GlobalStyles.mediumText_android
            ]}
          >
            {this.state.formError}
          </Text>
        </View>
      );
    } else {
      return null;
    }
  };

  updatePhone = input => {
    //console.warn(input, "phone");
  };
  updateName = input => {
    //console.warn(input, "name");
  };
  updateEmail = input => {
    //console.warn(input, "email");
  };
  updatePassword = input => {
    //console.warn(input, "password");
  };
  updateConfirmPassword = input => {
    //console.warn(input, "updateConfirmPassword");
  };

  doLogin = async (username, password) => {
    this.setState({ loading: true });

    try {
      const data = {
        username: username,
        password: password
      };

      let response = await Login(data);
      console.warn("status", response.status);

      switch (response.status) {
        case 500:
          this.setState({ loading: false });
          Alert.alert(
            i18n.t("errors.sorry"),
            i18n.t("errors.500"),
            [
              {
                text: "OK",
                onPress: () => this.state.inputs[0].state.inputRef.focus()
              }
            ],
            { cancelable: false }
          );
          break;
        case 409:
          this.setState({ loading: false });
          Alert.alert(
            i18n.t("register.email_exist_title"),
            i18n.t("register.email_exist"),
            [{ text: "OK", onPress: () => {} }],
            { cancelable: false }
          );
          break;
        case 401:
          //this.setState({formError: i18n.t("login.invalid_username_password")});
          Alert.alert(
            i18n.t("login.invalid_username_password_title"),
            i18n.t("login.invalid_username_password"),
            [
              {
                text: "OK",
                onPress: () => this.state.inputs[0].state.inputRef.focus()
              }
            ],
            { cancelable: false }
          );
          this.setState({ loading: false });
          break;
        case 200:
          let session = await response.json();
          console.warn("session", session);
          await AsyncStorage.setItem("userToken", JSON.stringify(session));
          this.goToHome();
          this.setState({ loading: false });

          break;
        default:
          this.setState({ loading: false });
          Alert.alert(
            i18n.t("errors.sorry"),
            i18n.t("errors.500"),
            [
              {
                text: "OK",
                onPress: () => this.state.inputs[0].state.inputRef.focus()
              }
            ],
            { cancelable: false }
          );
      }
    } catch (e) {
      this.setState({ loading: false });

      Alert.alert(
        i18n.t("errors.sorry"),
        i18n.t("errors.500"),
        [
          {
            text: "OK",
            onPress: () => this.state.inputs[0].state.inputRef.focus()
          }
        ],
        { cancelable: false }
      );
      this.setState({ loading: false });
    }
  };

  Signup = async () => {
    Keyboard.dismiss();

    if (!this.state.inputs[0].state.isCorrect) {
      Alert.alert(
        i18n.t("register.invalid_name_field_title"),
        i18n.t("register.invalid_name_field"),
        [
          {
            text: "OK",
            onPress: () => this.state.inputs[0].state.inputRef.focus()
          }
        ],
        { cancelable: false }
      );

      return;
    }

    if (!this.state.inputs[1].state.isCorrect) {
      Alert.alert(
        i18n.t("register.invalid_email_field_title"),
        i18n.t("register.invalid_email_field"),
        [
          {
            text: "OK",
            onPress: () => this.state.inputs[1].state.inputRef.focus()
          }
        ],
        { cancelable: false }
      );

      return;
    }

    if (!this.state.inputs[2].state.isCorrect) {
      Alert.alert(
        i18n.t("register.invalid_phone_field_title"),
        i18n.t("register.invalid_phone_field"),
        [
          {
            text: "OK",
            onPress: () => this.state.inputs[2].state.inputRef.focus()
          }
        ],
        { cancelable: false }
      );

      return;
    }

    if (!this.state.inputs[3].state.isCorrect) {
      Alert.alert(
        i18n.t("register.invalid_password_field_title"),
        i18n.t("register.invalid_password_field"),
        [
          {
            text: "OK",
            onPress: () => this.state.inputs[3].state.inputRef.focus()
          }
        ],
        { cancelable: false }
      );

      return;
    }

    if (
      !this.state.inputs[0].state.isCorrect ||
      !this.state.inputs[1].state.isCorrect ||
      !this.state.inputs[2].state.isCorrect ||
      !this.state.inputs[3].state.isCorrect ||
      !this.state.inputs[4].state.isCorrect
    ) {
      Alert.alert(
        i18n.t("register.fill_form_title"),
        i18n.t("register.fill_form"),
        [{ text: "OK", onPress: () => {} }],
        { cancelable: false }
      );
    } else {
      if (
        this.state.inputs[3].state.value != this.state.inputs[4].state.value
      ) {
        Alert.alert(
          i18n.t("register.passwords_not_match_title"),
          i18n.t("register.passwords_not_match"),
          [
            {
              text: "OK",
              onPress: () => this.state.inputs[3].state.inputRef.focus()
            }
          ],
          { cancelable: false }
        );
        //this.setState({ formError : i18n.t("register.passwords_not_match") })
        return;
      }

      if (this.state.inputs[3].state.value.length < 8) {
        Alert.alert(
          i18n.t("register.invalid_password_field_title"),
          i18n.t("register.invalid_password_field"),
          [
            {
              text: "OK",
              onPress: () => this.state.inputs[3].state.inputRef.focus()
            }
          ],
          { cancelable: false }
        );
        return;
      }

      this.setState({ loading: true });

      try {
        const data = {
          username: this.state.inputs[1].state.value,
          first_name: this.state.inputs[0].state.value,
          last_name: this.state.inputs[0].state.value,
          phone: this.state.inputs[2].state.value,
          password: this.state.inputs[3].state.value,
          client: "imv8"
        };

        let response = await Signup(data);

        switch (response.status) {
          case 500:
            Alert.alert(
              i18n.t("errors.sorry"),
              i18n.t("errors.500"),
              [
                {
                  text: "OK",
                  onPress: () => this.state.inputs[0].state.inputRef.focus()
                }
              ],
              { cancelable: false }
            );
            this.setState({ loading: false });
            break;
          case 409:
            let msg = await response.json();
            console.warn("msg", msg);

            if (msg.error_id.username) {
              Alert.alert(
                i18n.t("register.email_exist_title"),
                i18n.t("register.email_exist"),
                [
                  {
                    text: "OK",
                    onPress: () => this.state.inputs[3].state.inputRef.focus()
                  }
                ],
                { cancelable: false }
              );
            } else if (msg.error_id.phone) {
              Alert.alert(
                i18n.t("register.phone_exist_title"),
                i18n.t("register.phone_exist"),
                [
                  {
                    text: "OK",
                    onPress: () => this.state.inputs[3].state.inputRef.focus()
                  }
                ],
                { cancelable: false }
              );
            }
            //this.setState({ formError : i18n.t("register.email_exist") })

            this.setState({ loading: false });
            break;
          case 200:
            let user = await response.json();
            this.doLogin(data.username, data.password);
            break;
          default:
            this.setState({ loading: false });
            Alert.alert(
              i18n.t("errors.sorry"),
              i18n.t("errors.500"),
              [
                {
                  text: "OK",
                  onPress: () => this.state.inputs[0].state.inputRef.focus()
                }
              ],
              { cancelable: false }
            );
        }
      } catch (e) {
        this.setState({ loading: false });

        Alert.alert(
          i18n.t("errors.sorry"),
          i18n.t("errors.500"),
          [
            {
              text: "OK",
              onPress: () => this.state.inputs[0].state.inputRef.focus()
            }
          ],
          { cancelable: false }
        );
        this.setState({ loading: false });

        console.warn("error fetch", e);
      }
    }
  };

  render() {
    return (
      <ImageBackground
        style={[
          GlobalStyles.containerRow,
          GlobalStyles.backgroundColor,
          styles.linearGradient
        ]}
        source={require("../../resources/img//background.png")}
      >
        <ComeBack />
        <View colors={["#828282", "#e0e0e0"]} style={styles.containerPadding}>
          <KeyboardAwareScrollView style={styles.containerRegister}>
            <View style={[styles.containerRow]}>
              <Text
                style={{
                  color: "#fff",
                  fontSize: GlobalStyles.titleText_android.fontSize,
                  paddingTop: 10,
                  paddingBottom: 25
                }}
              >
                {i18n.t("register.sign-in")}
              </Text>
            </View>
            {this.state.formError != "" ? this.errorTemplate() : null}
            <Form>
              <View style={[GlobalStyles.containerRow_Android, styles.Bottompadding]}>
                <Name
                  changeFocus={this.changeInputFocus(0)}
                  update={this.updateName}
                  placeholder={i18n.t("register.name")}
                  special
                  ref={ref => {
                    this.state.inputs[0] = ref;
                  }}
                  placeholderTextColor="white"
                />
              </View>
              <View style={[GlobalStyles.containerRow_Android, styles.Bottompadding]}>

                <Email
                  changeFocus={this.changeInputFocus(1)}
                  update={this.updateEmail}
                  special
                  ref={ref => {
                    this.state.inputs[1] = ref;
                  }}
                  placeholderTextColor="white"
                />
              </View>
              <View style={[GlobalStyles.containerRow_Android, styles.Bottompadding]}>
                <Phone
                  changeFocus={this.changeInputFocus(2)}
                  update={this.updatePhone}
                  special
                  placeholder={i18n.t("register.phone")}
                  ref={ref => {
                    this.state.inputs[2] = ref;
                  }}
                  placeholderTextColor="white"
                  customStyle={{
                    fontSize: GlobalStyles.regularText_android.fontSize
                  }}
                />
              </View>

              <View style={[GlobalStyles.containerRow_Android, styles.Bottompadding]}>
                <Password
                  changeFocus={this.changeInputFocus(3)}
                  update={this.updatePassword}
                  special
                  ref={ref => {
                    this.state.inputs[3] = ref;
                  }}
                  placeholderTextColor="white"
                  customStyle={{
                    fontSize: GlobalStyles.regularText_android.fontSize
                  }}
                />
              </View>
              <View style={[GlobalStyles.containerRow_Android, styles.Bottompadding]}>
                <Password
                  changeFocus={this.changeInputFocus(4)}
                  update={this.updateConfirmPassword}
                  placeholder={i18n.t("register.confirmPassword")}
                  returnKeyType="go"
                  special
                  ref={ref => {
                    this.state.inputs[4] = ref;
                  }}
                  placeholderTextColor="white"
                  customStyle={{
                    fontSize: GlobalStyles.regularText_android.fontSize
                  }}
                />
              </View>

              <View style={styles.containerRow}>
              {(this.state.loading) ? this.show_loading() : (<Btn
                  onPress={this.Signup}
                  text={i18n.t("register.register")}
                  customStyle={{
                    borderRadius: wp('6%'),
                    flexDirection: "row",
                    width: "80%",
                    width: "100%",
                    color: "#828282",
                    backgroundColor: "#fff"
                  }}
                />)}
              </View>
            </Form>
          </KeyboardAwareScrollView>
        </View>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center"
  },
  containerRow: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    width: "100%",
  },
  containerPadding: {
    minHeight: "100%",
    maxHeight: "100%",
    width: wp("100%")
  },

  containerRegister: {
    height: hp("100%"), // 70% of height device screen
    width: wp("100%"), // 80% of width device screen
    paddingLeft: 5,
    paddingRight: 5,
    paddingTop: 5,
    paddingBottom: 5
  },
  Bottompadding: {
    paddingBottom:hp('2.5%')
  }
});

const Activity = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  horizontal: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10
  }
});
