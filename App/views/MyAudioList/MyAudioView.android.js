import React, { Component } from "react";

import {
  Platform,
  StyleSheet,
  View,
  FlatList,
  TouchableOpacity,
  Text,
  Vibration,
  Image,
  ProgressBarAndroid,
  Alert,
  ToastAndroid,
  Picker,
  AsyncStorage
} from "react-native";

import prompt from 'react-native-prompt-android';
import Slider from "react-native-slider";
import AudioRecorderPlayer from "react-native-audio-recorder-player";
import LinearGradient from "react-native-linear-gradient";
import * as Animatable from "react-native-animatable";
import { GlobalStyles } from "../../resources/style/style";
import Header from "../UtilsComponents/Seccions/header";
import Audio from "../UtilsComponents/Audio/Audio";
import TabMenu from "../UtilsComponents/Seccions/TabMenu";
import PlayList from "../../provider/playList";
import Reproductor from "../UtilsComponents/Seccions/Reproductor";
import Promo from "../UtilsComponents/Seccions/promo";
import Search from "../UtilsComponents/Seccions/search";
import i18n from "../../AppGlobalConfig/Localization/i18n";
import TimerMixin from "react-timer-mixin";
import RNFetchBlob from "rn-fetch-blob";
import Share from "react-native-share";
import PopoverTooltip from 'react-native-popover-tooltip';
var Sound = require("react-native-sound");

export default class MyAudioView extends Component {
  constructor(props) {
    super(props);

    this.state = {
      ActiveTab: 1,
      playList: [],
      link: "",
      playableAudio: false,
      isRecording: false,
      bgColor: "#333",
      audio: "",
      onTransmission: true,
      url: "",
      index: -1,
      session : false
    };

    this._bootstrapAsync();
  }

  // Fetch the token from storage then navigate to our appropriate place
  _bootstrapAsync = async () => {
    const user = await AsyncStorage.getItem("userToken");
    this.setState({ session : JSON.parse(user) });
    
    if(user){
        this.loadAudioList();
    }

  };

  componentDidMount() {
    this.audioRecorderPlayer = new AudioRecorderPlayer();
    this.song = null;
    this.timer = null;
  }

  componentWillReceiveProps(props) {
    this.loadAudioList();
  }

  loadAudioList = async () => {
    console.log("getting audios", this.state.session);
    //await AsyncStorage.removeItem("audios");
    await PlayList.get(this.state.session.user._id).then(data => {
      console.log("lista de reproducción", data);
      this.setState({
        playList: data
      });
    });
  };

  onPausePlay = async () => {
    this.song.pause();
    this.setState({
      onTransmission: true
    });
  };

  onStopRecord = async () => {
    const result = await this.audioRecorderPlayer.stopRecorder();
    this.audioRecorderPlayer.removeRecordBackListener();
    this.setState({
      recordSecs: 0,
      isRecording: false,
      playableAudio: true,
      bgColor: "#333",
      audio: result,
      onTransmission: true
    });
    console.log(result);
  };

  onStartPlayWithoutUrl = async () => {
    if(this.state.index <= -1){
        this.selectAudio(this.state.playList[0]);
      return;
    }
    
    this.song.play();
    this.setState({
      isPlaying: true,
      isPaused: false,
      onTransmission: false
      //currentPositionSec: e.current_position,
      //currentDurationSec: e.duration,
      //playTime: this.audioRecorderPlayer.mmssss(Math.floor(e.current_position)),
      //duration: this.audioRecorderPlayer.mmssss(Math.floor(e.duration))
    });
  };

 onStartPlay = url => {
    if (this.timer) TimerMixin.clearInterval(this.timer);

    this.song = new Sound(url, Sound.MAIN_BUNDLE, error => {
      if (error) {
        console.warn("failed to load the sound", error);
        return;
      }

      this.song.setCategory("Playback");
      //activate audio in silence mode ios

      this.timer = TimerMixin.setInterval(() => {
        this.song.getCurrentTime(current => {
          let hours = Math.floor(current / 3600);
          let minutes = Math.floor(current / 60);
          let seconds = ~~current % 60;

          this.setState({
            currentPositionSec: Math.floor(current),
            current : current,
            progress :  current / this.song.getDuration(),
            playTime: `${minutes < 10 ? "0" + minutes : minutes}:${
              seconds < 10 ? "0" + seconds : seconds
            }`
          });

          if(this.state.current == this.song.getDuration()){
            if(this.state.isPlaying){
                clearInterval(this.timer)
                this.onStopPlay();
                return;       
            }
          }

        });

      }, 1000);

      let duration = this.song.getDuration();
      let hours = Math.floor(duration / 3600);
      let minutes = Math.floor(duration / 60);
      let seconds = ~~duration % 60;

      this.setState({
        currentDurationSec: duration,
        duration: `${minutes < 10 ? "0" + minutes : minutes}:${
          seconds < 10 ? "0" + seconds : seconds
        }`
      });

      this.song.play(success => {
        if (!success) {
          ToastAndroid.show("Error when play Sound :(((", ToastAndroid.SHORT);
        }

        //this.song.reset();
      });
    });

    this.setState({
      isPlaying: true,
      isPaused: false,
      onTransmission: false
    });
  };

  onStopPlay = async () => {
    this.state.playTime = "00:00";
    this.state.duration = "00:00";
    this.audioRecorderPlayer.stopPlayer();
    this.song.stop();
    this.song.release();
    TimerMixin.clearInterval(this.timer);

    this.setState({
      isPlaying: false,
      isPaused: false,
      onTransmission: true
    });
  };

  selectAudio = audio => {
    try {
      if (this.state.isPlaying) {
        this.onStopPlay();
      }

      this.setState({
        activate: audio.id,
        index: this.state.playList.indexOf(audio)
      });

      //  console.warn(audio.url)

      if (audio.url) {
        //let audioPath = `${RNFetchBlob.fs.dirs.SDCardDir}/${audio.url}`;

        //console.warn(audioPath);

        this.onStartPlay(`/${audio.url}`);
      }
    } catch (e) {
      console.warn("playing audio: " + e);
    }
  };

  rewind = async () => {
    if (this.state.isPlaying) {

      let totalDuration = this.state.currentDurationSec;
      let current = this.state.currentPositionSec;
      let rewind = Math.floor(current - 15);

      console.warn(rewind);

      if (rewind > 0) await this.song.setCurrentTime(rewind);
      else await this.song.setCurrentTime(0);
    }
  };

  forward = async () => {
    if (this.state.isPlaying) {
      let totalDuration = this.state.currentDurationSec;
      let current = this.state.currentPositionSec;
      let forward = Math.floor(current + 15);

      console.warn(forward);

      if (forward <= totalDuration) await this.song.setCurrentTime(forward);
      else alert("Sorry current audio duration lower or equal to 15 seconds");
    }
  };

  next = () => {
    this.state.playTime = "00:00:00";
    this.state.duration = "00:00:00";
    this.audioRecorderPlayer.stopPlayer();
    this.audioRecorderPlayer.removePlayBackListener();
    delete this.state.progress;
    try {
      return this.selectAudio(this.state.playList[this.state.index + 1]);
    } catch (e) {
      this.selectAudio(this.state.playList[0]);
    }
  };

  previous = () => {
    this.state.playTime = "00:00:00";
    this.state.duration = "00:00:00";
    this.audioRecorderPlayer.stopPlayer();
    this.audioRecorderPlayer.removePlayBackListener();
      delete this.state.progress;
    try {
      return this.selectAudio(this.state.playList[this.state.index - 1]);
    } catch (e) {
      this.selectAudio(this.state.playList[0]);
    }
  };

  onShareAudio = async audio => {
    //console.warn(audio, "audio");

    // Vibration.vibrate([300]);
    let stats = await RNFetchBlob.fs.stat(audio.url);
    let raw  = await RNFetchBlob.fs.readFile(audio.url, 'base64');

    console.warn("raw", raw);

    let options = {
      title: "IMV8",
      message: "Shared imv8 audio",
      url: ("data:audio/m4a;base64,"+raw)
    };

    await Share.open(options);
    alert(i18n.t("globals.audio-was-shared-ok"));
    //let fileUrl = audio.url;
    //console.warn(fileUrl, "FILE URL");

    /*let audioPath =
      RNFetchBlob.fs.dirs.CacheDir.replace("Caches", "tmp").replace(
        "/Library",
        ""
      ) +
      "/" +
      audio.url.split("/")[audio.url.split("/").length - 1];*/



    //let know to the user that the audio was shared succesful
  };
  getTypeSearch = data => {
    const type = "audios";
    if (data == "name") {
      PlayList.getOrderName(type).then(data => {
        //alert(data.length)
        this.setState({
          playList: data
        });
      });
    }
    if (data.type == "filter") {
      let value = data.value;

      PlayList.getByCriteria(value).then(data => {
        this.setState({
          playList: data
        });
      });
    }
    if (data == "date") {
      PlayList.get().then(data => {
        this.setState({
          playList: data
        });
      });
    }
    if (data == "Lenght") {
      PlayList.getOrderLenght().then(data => {
        this.setState({
          playList: data
        });
      });
    }
    if (data == "size") {
      PlayList.getOrderSize().then(data => {
        this.setState({
          playList: data
        });
      });
    }
    if (data == "oldest") {
      PlayList.getOldestSize().then(data => {
        this.setState({
          playList: data
        });
      });
    }
    if (data == "newest") {
      PlayList.getNewestSize().then(data => {
        this.setState({
          playList: data
        });
      });
    }
  };

  rename = (audio, cb) => {
       prompt(
        'Audio Name',
        'Please type audio name',
        [
         {text: 'Cancel', onPress: () => console.warn('Cancel Pressed'), style: 'cancel'},
         {text: 'Rename', onPress: async (name)=>{

                let update_audio = {
                  date: audio.date,
                  newTitle: name
                };

                PlayList.setName(update_audio).then((data)=>{
                    this.loadAudioList(this.state.session.user._id);
                });
         }},
        ],
        {
            type: 'email-address',
            cancelable: false,
            defaultValue: audio.title,
            placeholder: 'Audio Name'
        }
    );
  }

  deleteAudio = id => {
    // Works on both iOS and Android
    Alert.alert("Confirmation", "Are you sure to delete this Audio?", [
      {
        text: "Cancel",
        onPress: () => console.log("delete canceled"),
        style: "cancel"
      },
      {
        text: "Delete",
        onPress: () => {
          try {
            PlayList.delete(id).then(data => {
              this.setState({
                playList: data
              });

              alert("Audio removed succesful");
            });
          } catch (e) {
            alert("Audio can't be removed");
          }
        }
      }
    ]);
  };

  render() {
    return (
      <View style={styles.container}>
        <Animatable.View
          animation="fadeInLeft"
          delay={1200}
          duration={700}
          style={[styles.container]}
        >
          <LinearGradient
            start={{ x: 0.0, y: 0.25 }}
            end={{ x: 1.5, y: 0.5 }}
            colors={["#5ccbfa", "#33a2f2"]}
            style={styles.linearGradient}
          >
          <Header updateSearch={this.getTypeSearch} />
          </LinearGradient>
          <TabMenu
            update={this.changeTab}
            onPress={this.onStopPlay}
            value={1}
            onLeave={this.onStopPlay}
          />

          <Promo />

          <FlatList
            data={this.state.playList}
            extraData={this.state}
            onRefresh={() => {
              this.loadAudioList();
              this.setState({ refreshing: true });
              TimerMixin.setTimeout(() => {
                this.setState({ refreshing: false });
              }, 1000);
            }}
            refreshing={this.state.refreshing ? true : false}
            renderItem={({ item }) => (
              <Audio
                sound={item}
                delete={this.deleteAudio}
                rename={this.rename}
                activate={this.state.activate}
                onPress={this.selectAudio}
                onLongPress={this.onShareAudio}
                keyExtractor={(item, index) => item.id}
              />
            )}
          />

          <View style={styles.reproductorContainer}>
            <View style={styles.reproductorContainerRow}>
              <TouchableOpacity
                style={[
                  styles.reproductorContainerImage,
                  { alignItems: "flex-end" }
                ]}
                onPress={this.rewind}
              >
                <Image
                  style={styles.reproductorReturnAudio}
                  source={require("../../resources/img/15left.png")}
                />
              </TouchableOpacity>
              <TouchableOpacity
                style={[styles.reproductorContainerImage]}
                onPress={this.previous}
              >
                <Image
                  style={styles.reproductorBtnNext}
                  source={require("../../resources/img/leftSong.png")}
                />
              </TouchableOpacity>

              {this.state.onTransmission == true ? (
                <TouchableOpacity style={styles.reproductorBtnPlayContainer} onPress={this.onStartPlayWithoutUrl}>
                  <Image
                    style={styles.reproductorBtnPlay}
                    source={require("../../resources/img/play.png")}
                  />
                </TouchableOpacity>
              ) : (
                <TouchableOpacity
                  style={styles.reproductorBtnPlayContainer}
                  onPress={this.onPausePlay}
                >
                <Image
                  style={[styles.reproductorBtnPlay]}
                  source={require("../../resources/img/paused.png")}
                />
                </TouchableOpacity>
              )}
              <TouchableOpacity
                style={styles.reproductorContainerImage}
                onPress={this.next}
              >
                <Image
                  style={styles.reproductorBtnNext}
                  source={require("../../resources/img/rightSong.png")}
                />
              </TouchableOpacity>
              <TouchableOpacity
                style={[
                  styles.reproductorContainerImage,
                  { alignItems: "flex-start" }
                ]}
                onPress={this.forward}
              >
                <Image
                  style={styles.reproductorReturnAudio}
                  source={require("../../resources/img/15right.png")}
                />
              </TouchableOpacity>
            </View>
            <View style={[styles.reproductorContainerLine]}>
              <View styles={[styles.reproductorTimes]}>
                <Text
                  style={[styles.reproductorTimeText, { paddingRight: 10 }]}
                >
                  {this.state.playTime || "00:00"}
                </Text>
              </View>

              <View styles={styles.reproductorLineTime}>
                  <Slider
                    value={this.state.progress}
                    thumbStyle = {{width: 10, height: 10}}
                    thumbTintColor = {"#33a2f2"}
                    minimumTrackTintColor = {"#33a2f2"}
                    trackStyle = {{ backgroundColor: "#808080", height : 1 }}
                    style={{ width: 208, height: 1}}
                    onValueChange={value => this.setState({ value })}
                  />
              </View>
              <View styles={styles.reproductorTimes}>
                <Text style={[styles.reproductorTimeText, { paddingLeft: 10 }]}>
                  {this.state.duration || "00:00"}
                </Text>
              </View>
            </View>
          </View>
        </Animatable.View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  reproductorContainer: {
    display: "flex",
    width: "100%",
    height: 100,
    bottom: 0,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#e0e0e0",
    borderTopColor: "#cad0d4",
    borderTopWidth: 1
  },
  reproductorContainerRow: {
    flexDirection: "row",
    width: "90%",
    alignItems: "center",
    justifyContent: "center",
    paddingBottom: 10,
    height: 30
  },
  reproductorContainerLine: {
    flexDirection: "row",
    width: "90%",
    alignItems: "center",
    justifyContent: "center",
    paddingBottom: 10,
    height: 30
  },
  reproductorContainerImage: {
    width: "20%",
    height: 50,
    alignItems: "center",
    justifyContent: "center"
  },
  reproductorTimes: {
    width: "10%"
  },
  reproductorTimeText: {
    fontSize: 12,
    color: "#828282"
  },
  reproductorLineTime: {
    width: "80%"
  },

  reproductorBtnNext: {
    width: 19,
    height: 22
  },
  reproductorReturnAudio: {
    width: 20.92,
    height: 23.4
  },
  reproductorBtnPlay: {
    width: 62,
    height: 62
  },

  reproductorBtnPlayContainer  : {
    top:-20
  }
});



/*



*/
