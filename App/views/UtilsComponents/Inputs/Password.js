import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  Platform,
  StyleSheet,
  Dimensions,
  Text,
  View,
  Button,
  TouchableOpacity,
  StatusBar,
  TextInput,
  ScrollView
} from "react-native";
import EStyleSheet from "react-native-extended-stylesheet";
import i18n from "../../../AppGlobalConfig/Localization/i18n";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";

export default class Password extends Component {
  constructor() {
    super();
    this.state = {
      inputRef: null,
      value: "",
      isCorrect: false,
      required: true,
      error: ""
    };
  }

  errorTemplate = () => {
    if (this.state.error) {
      return (
        <Text style={{ textAlign: "center", color: "red", padding: 4 }}>
          {this.state.error}
        </Text>
      );
    } else {
      return null;
    }
  };

  checkIfIsCorrect = () => {
    if (this.state.value !== "") {
      this.state.isCorrect = true;
      delete this.state.error;
    } else {
      if (this.state.value == "") {
        this.state.error = i18n.t("login.password_field_required");
        this.state.isCorrect = false;
      } else {
        this.state.error = i18n.t("login.password_field_required");
        this.state.isCorrect = false;
      }
    }

    this.setState(this.state);
    this.props.update(this.state.value);
  };

  clearInput = () => {
    this.state.inputRef._root.setNativeProps({ text: "" }); // eslint-disable-line
    this.setState({ isCorrect: 0, value: "" });
  };

  updateText = value => {
    if (value != "") {
      this.setState({ value: value, isCorrect: true });
    } else {
      if (this.state.value == "") {
        this.state.error = i18n.t("login.password_field_required");
        this.state.isCorrect = false;
      } else {
        this.state.error = i18n.t("login.password_field_required");
        this.state.isCorrect = false;
      }
    }
  };

  render() {
    let keyType = "done";
    if (this.props.special) {
      keyType = "next";
    }
    return (
       <View style={{ width: '100%', padding:4}}>
        <TextInput
          style={[styles.input, EStyleSheet.create(this.props.customStyle)]}
          blurOnSubmit={!this.props.special}
          returnKeyType={this.props.returnKeyType}
          ref={ref => {
            this.state.inputRef = ref;
          }}
          autoCapitalize="none"
          placeholder={this.props.placeholder}
          onSubmitEditing={this.props.changeFocus}
          secureTextEntry
          onChangeText={this.updateText}
          onEndEditing={this.checkIfIsCorrect}
          placeholderTextColor={this.props.placeholderTextColor}
        />
        {this.state.error != "" ? this.errorTemplate() : null}
      </View>
    );
  }
}

isIphoneXorAbove = (s, n)=> {
  const dimen = Dimensions.get('window');
  
  if((
    Platform.OS === 'ios' &&
    !Platform.isPad &&
    !Platform.isTVOS &&
    ((dimen.height === 812 || dimen.width === 812) || (dimen.height === 896 || dimen.width === 896))
  )){

    return s;

  }

  return n;
}

const styles = StyleSheet.create({
  input: {
    color: "white",
    height: isIphoneXorAbove(hp('6%'), hp('8%')), // 70% of height device screen
    width: wp('95%'), 
    borderRadius: wp('6%'),   
    borderWidth: 2,
    borderColor: "#fff",
    paddingLeft: 20,
    fontSize: 16
  }
});
Password.propTypes = {
  update: PropTypes.func.isRequired,
  changeFocus: PropTypes.func.isRequired,
  special: PropTypes.bool,
  placeholder: PropTypes.string
};

Password.defaultProps = {
  special: false,
  placeholder: "Password",
  placeholderTextColor: "gray",
  customStyle: {},
  returnKeyType: "next"
};
