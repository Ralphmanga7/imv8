import React, { Component } from "react";
import PropTypes from "prop-types";
import { TouchableOpacity, Image, View } from "react-native";
import { withNavigation } from "react-navigation";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

class ComeBack extends Component {
  constructor(props) {
    super(props);
  }
  goBack = () => {
    console.warn("entro");
  };
  render() {
    return (
      <TouchableOpacity
        style={[styles.container]}
        onPress={() => {
          this.props.navigation.goBack();
        }}
      >
        <Image
          style={{ width: 28, height: 15 }}
          source={require("../../../resources/img/back.png")}
        />
      </TouchableOpacity>
    );
  }
}

const styles = {
  container: {
    display: "flex",
    color: "#828282",
    padding: 10,
    borderRadius: 5,
    justifyContent: "flex-start",
    width: "100%",
    top: wp('2%'),
    zIndex:20,
    position:'relative'
  }
};

export default withNavigation(ComeBack);
