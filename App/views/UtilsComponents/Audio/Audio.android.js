import React, { Component } from "react";
import LinearGradient from "react-native-linear-gradient";
import { GlobalStyles } from "../../../resources/style/style";
import { ToolTipAudio } from "../tooltips/ToolTipAudio";
import PopoverTooltip from "react-native-popover-tooltip";
import TimerMixin from "react-timer-mixin";
import { withNavigation } from "react-navigation";
import Dialog from "react-native-dialog";
import PlayList from "../../../provider/playList";

import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ImageBackground
} from "react-native";
import Moment from "moment";
import PropTypes from "prop-types";
import { absoluteFill } from "react-native-extended-stylesheet";
class Audio extends Component {
  constructor(props) {
    super(props);

    this.state = {
      inputRef: null,
      value: "",
      dialogVisible: false,
      name: "",
      order: 1,
      title: ""
    };

    Moment.updateLocale("en", {
      ordinal: function(number, token) {
        var b = number % 10;
        var output =
          ~~((number % 100) / 10) === 1
            ? "th"
            : b === 1
            ? "st"
            : b === 2
            ? "nd"
            : b === 3
            ? "rd"
            : "th";
        return number + output;
      }
    });
  }

  componentWillReceiveProps(props) {
    this.setState({ title: props.sound.title });
  }

  showDialog = async () => {
    console.log("entro al show");
    console.log(this.state.dialogVisible);

    await this.setState({ dialogVisible: true });
  };

  handleCancel = () => {
    this.setState({ dialogVisible: false });
  };

  handleDelete = async () => {
    this.setState({ dialogVisible: false });
    if (this.state.dialogVisible == true) {
      console.log("id:", this.props.sound.date);
      let data = {
        date: this.props.sound.date,
        newTitle: this.state.name
      };

      await PlayList.setName(data).then(data => {
        this.setState({ title: data });
      });
    }
  };

  selectAudio = async () => {
    try {
      this.props.onPress(this.props.sound);
    } catch (e) {}
  };

  onLongPress = () => {
    try {
      if (this.props.sound) this.props.onLongPress(this.props.sound);
    } catch (e) {
      alert(e);
    }
  };
  test = () => {
    this.setState({ dialogVisible: this.state.dialogVisible });
  };

  items = [
    {
      label: () => {
        return <Text style={GlobalStyles.colorTootltipLabel}>Rename</Text>;
      },
      onPress: async ()=>{
        this.props.rename(this.props.sound);
      }
    },
    {
      label: () => {
        return <Text style={GlobalStyles.colorTootltipLabel}>Set Alarm</Text>;
      },
      onPress: () => {
        this.props.navigation.navigate("Alarms", {
          type: "AUDIO",
          id: this.props.sound.id
        });
      }
    },
    {
      label: () => {
        return <Text style={GlobalStyles.colorTootltipLabel}>Share</Text>;
      },
      onPress: () => {
        TimerMixin.setTimeout(() => {
          this.onLongPress();
        }, 500);
      }
    },
    {
      label: () => {
        return <Text style={GlobalStyles.colorTootltipLabel}>Delete</Text>;
      },
      onPress: () => {
        TimerMixin.setTimeout(() => {
          this.props.delete(this.props.sound.id);
        }, 500);
      }
    }
  ];


  updateName = value => {
    this.setState({ name: value });
  };
  componentWillReceiveProps(props) {
    this.setState({ title: props.sound.title });
  }
  componentDidMount() {
    this.setState({ title: this.props.sound.title });
  }

  render() {
    return (
      <View
        style={[
          styles.container,
          this.props.activate == this.props.sound.id ? styles.borderLeft : null
        ]}
      >
        {this.props.activate == this.props.sound.id ? (
          <LinearGradient
            start={{ x: -1.0, y: 0 }}
            end={{ x: 1.5, y: 0 }}
            colors={["#33a2f24a", "#33a2f200"]}
            style={[styles.linearGradient]}
          >
            <TouchableOpacity
              style={[styles.cardActiva, {}]}
              onPress={this.selectAudio}
              onLongPress={this.onLongPress}
            >
              <View
                style={{
                  width: "70%"
                }}
              >
                <Text
                  style={[
                    styles.fistline,
                    styles.activateColor,
                    GlobalStyles.regularText
                  ]}
                >
                  {this.state.title}
                </Text>
                <Text
                  style={[
                    styles.secoundLine,
                    styles.activateColor,
                    GlobalStyles.mediumText
                  ]}
                >
                  {Moment(this.props.sound.date).format(
                    "MMMM Do, YYYY [at] hh:mm a"
                  )}
                </Text>
              </View>
              <View
                style={[
                  styles.comtainerFirstLine,
                  {
                    width: "30%",
                    justifyContent: "center",
                    alignItems: "flex-start",
                    paddingBottom: 18,
                    paddingLeft: 17,
                    top: 0
                  }
                ]}
              >
                <Text
                  style={[
                    styles.fistline,
                    styles.activateColor,
                    GlobalStyles.regularText
                  ]}
                >
                  {this.props.sound.duration.substring(0, 5)}
                </Text>

                <Text
                  style={[
                    styles.secoundLine,
                    styles.activateColor,
                    GlobalStyles.mediumText
                  ]}
                >
                  {this.props.sound.size}
                </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity style={[styles.toolContainer, {}]}>
              <TouchableOpacity
                style={[styles.buttonComponent]}
                onPress={() => {
                  this.refs[this.props.sound.id].toggle(); // open popover tooltips one by one
                }}
              >
                <ImageBackground
                  style={[styles.tooltip, styles.ToolTipImage]}
                  resizeMode="center"
                  resizeMode="contain"
                  source={require("../../../resources/img/tab-buttons.png")}
                />
              </TouchableOpacity>

              <PopoverTooltip
                ref={this.props.sound.id}
                items={this.items}
                labelSeparatorColor="#cad0d4"
                labelStyle="#cad0d4"
                overlayStyle={{ backgroundColor: "#21252912" }}
                delayLongPress={0}
                triangleOffset={0}
                tooltipContainerStyle={{
                  padding: 10,
                  marginLeft: -25,
                  marginTop: -20
                }}
              />
            </TouchableOpacity>
          </LinearGradient>
        ) : (
          <View
            style={{
              width: "100%",
              height: 70,
              paddingLeft: 15
            }}
          >
            <TouchableOpacity
              style={[styles.card, {}]}
              onPress={this.selectAudio}
              onLongPress={this.onLongPress}
            >
              <View
                style={{
                  width: "75%"
                }}
              >
                <Text style={[styles.fistline, GlobalStyles.regularText]}>
                  {this.state.title}
                </Text>
                <Text style={[styles.secoundLine, GlobalStyles.mediumText]}>
                  {Moment(this.props.sound.date).format(
                    "MMMM Do, YYYY [at] hh:mm a"
                  )}
                </Text>
              </View>
              <View style={[styles.comtainerFirstLine]}>
                <Text style={[styles.fistline, GlobalStyles.regularText]}>
                  {this.props.sound.duration.substring(0, 5)}
                </Text>

                <Text
                  style={[
                    styles.secoundLine,
                    GlobalStyles.mediumText,
                    {
                      textAlign: "left"
                    }
                  ]}
                >
                  {this.props.sound.size}
                </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity style={[styles.toolContainer, {}]}>
              <TouchableOpacity
                style={[styles.buttonComponent]}
                onPress={() => {
                  this.refs[this.props.sound.id].toggle(); // open popover tooltips one by one
                }}
              >
                <ImageBackground
                  style={[styles.tooltip, styles.ToolTipImage]}
                  resizeMode="center"
                  resizeMode="contain"
                  source={require("../../../resources/img/tab-buttons.png")}
                />
              </TouchableOpacity>
              <PopoverTooltip
                ref={this.props.sound.id}
                items={this.items}
                labelSeparatorColor="#cad0d4"
                labelStyle="#cad0d4"
                overlayStyle={{ backgroundColor: "#21252912" }}
                delayLongPress={0}
                triangleOffset={0}
                tooltipContainerStyle={{
                  padding: 10,
                  marginLeft: -25,
                  marginTop: -20
                }}
              />
            </TouchableOpacity>
          </View>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    display: "flex",
    width: "100%",
    height: 70,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#f1f2f3"
  },
  buttonComponent: {
    width: 35.5,
    height: 65.2,
    zIndex: 100
  },
  ToolTipImage: {
    width: 8,
    height: 37,
    padding: 10,
    paddingLeft: 80,
    marginTop: 10
  },
  colorTootltipLabel: {
    color: "#cad0d4"
  },
  linearGradient: {
    width: "100%",
    height: 70,
    paddingLeft: 10
  },
  activateColor: {
    color: "#1676bb"
  },
  borderLeft: {
    borderLeftWidth: 5,
    borderLeftColor: "#33a2f2"
  },
  comtainerFirstLine: {
    width: "20%",
    justifyContent: "flex-start",
    alignItems: "flex-start",
    paddingBottom: 12
  },

  containerRow: {
    display: "flex",
    width: "100%",
    height: 53,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#f1f2f3"
  },

  fistline: {
    paddingBottom: 3,
    fontSize: 12
  },
  secoundLine: {
    color: "#616770",
    fontSize: 11
  },
  title: {
    color: "#fff",
    fontSize: 25,
    paddingTop: 30,
    paddingLeft: 30
  },
  subTitle: {
    color: "#fff",
    fontSize: 12,
    paddingTop: 5,
    paddingLeft: 27
  },
  card: {
    width: "95%",
    paddingTop: 10,
    height: 10,
    flex: 1,
    flexDirection: "row",
    borderBottomColor: "#cad0d4",
    borderBottomWidth: 0.5
  },
  cardActiva: {
    width: "95%",
    paddingTop: 10,
    height: 10,
    flex: 1,
    flexDirection: "row",
    borderBottomColor: "#cad0d4",
    borderBottomWidth: 0.5
  },
  tooltip: {
    height: 45.2,
    width: 35.5,
    padding: 10,
    top: 5,
    right: 22
  },
  toolContainer: {
    width: "10%",
    justifyContent: "flex-end",
    alignItems: "flex-end",
    padding: 0,
    position: "absolute",
    right: 17
  }
});

export default withNavigation(Audio);
