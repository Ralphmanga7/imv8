import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  View,
  FlatList,
  TouchableOpacity,
  Text,
  Image
} from "react-native";
import Accordion from 'react-native-collapsible/Accordion';
import LinearGradient from "react-native-linear-gradient";
import * as Animatable from "react-native-animatable";
import { GlobalStyles } from "../../resources/style/style";
import HeaderAlarm from "../UtilsComponents/Seccions/headerAlarm";
import i18n from "../../AppGlobalConfig/Localization/i18n";


const Questions = [
  {
    title: '¿First Question?',
    content: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book',
  },
  {
    title: '¿First Question?',
    content: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book',
  },
  {
    title: '¿First Question?',
    content: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book',
  },
  {
    title: '¿First Question?',
    content: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book',
  },
  {
    title: '¿First Question?',
    content: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book',
  },
  {
    title: '¿First Question?',
    content: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book',
  },
  {
    title: '¿First Question?',
    content: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book',
  },
];


export default class Help extends Component {
  constructor(props) {
    super(props);
    this.config = {};
    this.state = { 
      activeSections: [],
    };
  }
  componentWillReceiveProps(props) {
    const { navigation } = props;
  }
  componentWillMount() {
    const { navigation } = this.props;
    
  }

  _renderSectionTitle = section => {
    return (
      <View style={styles.header}>
      </View>
    );
  };

  _renderHeader = section => {
    return (
      <View style={styles.header}>
        <Text style={styles.headerText}>{section.title}</Text>
      </View>
    );
  };

  _renderContent = section => {
    return (
      <View style={styles.content}>
        <Text>{section.content}</Text>
      </View>
    );
  };

  _updateSections = activeSections => {
    this.setState({ activeSections });
  };

  load = () => {
    const { navigation } = this.props;
  }
  render() {
    return (
      <View style={styles.container}>
        <Animatable.View style={[styles.container]}>
          <LinearGradient
            start={{ x: 0.0, y: 0.25 }}
            end={{ x: 1.5, y: 0.5 }}
            colors={["#5ccbfa", "#33a2f2"]}
            style={styles.linearGradient}
          >
            <HeaderAlarm
              title={i18n.t("globals.help")}
              type={"listAlarms"}
              disableEdit={true}
            />
          </LinearGradient>
        </Animatable.View>
        <View style={[styles.accordion]}>
            <Accordion
              sections={Questions}
              activeSections={this.state.activeSections}
              renderSectionTitle={this._renderSectionTitle}
              renderHeader={this._renderHeader}
              renderContent={this._renderContent}
              onChange={this._updateSections}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,  
    display: "flex",
  },
  LinearGradient: {
    borderRadius: 50,
    width: 40,
    height: 40,
    alignItems: "center",
    justifyContent: "center"
  },
  accordion:{
    display: "flex",
    width: "100%",
    alignItems: "flex-start",
    justifyContent: "flex-start",
    backgroundColor: '#399F33',
  },
  title: {
    textAlign: 'left',
    fontSize: 22,
    fontWeight: '300',
    marginBottom: 20,
  },
  headerText: {
    textAlign: 'center',
    fontSize: 16,
    fontWeight: '500',
  },
  header: {
    backgroundColor: '#F5FCFF',
    padding: 10,
  },
  content: {
    top:0,
    padding: 20,
    backgroundColor: '#fff',
  },
  active: {
    backgroundColor: 'rgba(255,255,255,1)',
  },
  inactive: {
    backgroundColor: 'rgba(245,252,255,1)',
  },
  selectors: {
    marginBottom: 10,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  selector: {
    backgroundColor: '#F5FCFF',
    padding: 10,
  },
  activeSelector: {
    fontWeight: 'bold',
  },
  selectTitle: {
    fontSize: 14,
    fontWeight: '500',
    padding: 10,
  },
  multipleToggle: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginVertical: 30,
    alignItems: 'center',
  },
  multipleToggle__title: {
    fontSize: 16,
    marginRight: 8,
  },
});
//currentAudio={this.state.currentAudio}