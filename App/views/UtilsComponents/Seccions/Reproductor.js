import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  StatusBar,
  TextInput,
  ScrollView
} from "react-native";
const audioRecorderPlayer = new AudioRecorderPlayer();
import PropTypes from "prop-types";
import { Right } from "native-base";
import AudioRecorderPlayer from "react-native-audio-recorder-player";
export default class Reproductor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inputRef: null,
      value: "",
      isCorrect: 0,
      recordSecs: "",
      recordTime: ""
    };
  }

  componentDidMount() {
    this.requestPermisions();
    this.audioRecorderPlayer = new AudioRecorderPlayer();
  }
  componentDidUpdate(prev_props, prev_state) {
    let audio = this.props.link;
    if (audio.url) {
      let uri = audio.url.split("/")[audio.url.split("/").length - 1];
      this.onStartPlay(uri);
    }
  }

  requestPermisions = async () => {
    try {
      if (Platform.OS === "android") {
        const micPermission = await checkPermission("microphone");
        console.log("micPermission", micPermission);
        if (micPermission !== "authorized") {
          const micRequest = await requestPermission("microphone");
          console.log("micRequest", micRequest);
          if (micRequest !== "authorized") {
            return;
          }
        }

        const storagePermission = await checkPermission("storage");

        if (storagePermission !== "authorized") {
          const storageRequest = await requestPermission("storage");
          if (storageRequest !== "authorized") {
            return;
          }
        }
      }
    } catch (e) {}
  };

  onStartPlay = async url => {
    console.log("onStartPlay");
    const msg = await this.audioRecorderPlayer.startPlayer(url);
    console.log(msg);
    this.audioRecorderPlayer.addPlayBackListener(e => {
      if (e.current_position === e.duration) {
        console.log("finished");
        this.audioRecorderPlayer.stopPlayer();
      }
      this.setState({
        currentPositionSec: e.current_position,
        currentDurationSec: e.duration,
        playTime: this.audioRecorderPlayer.mmssss(
          Math.floor(e.current_position)
        ),
        duration: this.audioRecorderPlayer.mmssss(Math.floor(e.duration)),
        isPlaying: true,
        isPaused: false
      });
      return;
    });
  };
  onPausePlay = async () => {
    await this.audioRecorderPlayer.pausePlayer();
    this.setState({
      isPlaying: false,
      isPaused: true
    });
  };

  onStopPlay = async () => {
    console.log("onStopPlay");
    this.audioRecorderPlayer.stopPlayer();
    this.audioRecorderPlayer.removePlayBackListener();
  };
  saveSound = async () => {
    const result = await this.audioRecorderPlayer.stopPlayer();

    let audio = { url: this.state.audio, duration: this.state.duration };
    PlayList.add(audio);
    this.audioRecorderPlayer.removePlayBackListener();
    this.props.navigation.navigate("MyAudioView");
  };
  resetPlayable = () => {
    this.onStopPlay();
    this.audioRecorderPlayer.stopPlayer();
    this.audioRecorderPlayer.removePlayBackListener();

    this.setState({
      playableAudio: false,
      isRecording: false,
      recordTime: null,
      isPlaying: false,
      isPaused: false
    });
  };

  render() {
    return (
      <View style={styles.reproductorContainer}>
        <View style={styles.reproductorContainerRow}>
          <TouchableOpacity
            style={[
              styles.reproductorContainerImage,
              { alignItems: "flex-end" }
            ]}
          >
            <Image
              style={styles.reproductorReturnAudio}
              source={require("../../../resources/img/15left.png")}
            />
          </TouchableOpacity>
          <TouchableOpacity style={[styles.reproductorContainerImage]}>
            <Image
              style={styles.reproductorBtnNext}
              source={require("../../../resources/img/leftSong.png")}
            />
          </TouchableOpacity>
          <TouchableOpacity style={styles.reproductorContainerImage}>
            <Image
              style={styles.reproductorBtnPlay}
              source={require("../../../resources/img/paused.png")}
            />
          </TouchableOpacity>
          <TouchableOpacity style={styles.reproductorContainerImage}>
            <Image
              style={styles.reproductorBtnNext}
              source={require("../../../resources/img/rightSong.png")}
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={[
              styles.reproductorContainerImage,
              { alignItems: "flex-start" }
            ]}
          >
            <Image
              style={styles.reproductorReturnAudio}
              source={require("../../../resources/img/15right.png")}
            />
          </TouchableOpacity>
        </View>
        <View style={styles.reproductorContainerLine}>
          <View styles={styles.reproductorTimes}>
            <Text style={[styles.reproductorTimeText, { paddingRight: 10 }]}>
              00:39
            </Text>
          </View>

          <View styles={styles.reproductorLineTime}>
            <Image
              style={{ width: 208, height: 1 }}
              source={require("../../../resources/img/line.png")}
            />
          </View>
          <View styles={styles.reproductorTimes}>
            <Text style={[styles.reproductorTimeText, { paddingLeft: 10 }]}>
              04:01
            </Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  reproductorContainer: {
    display: "flex",
    width: "100%",
    height: 100,
    bottom: 0,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#e0e0e0",
    borderTopColor: "#cad0d4",
    borderTopWidth: 1
  },
  reproductorContainerRow: {
    flexDirection: "row",
    width: "90%",
    alignItems: "center",
    justifyContent: "center",
    paddingBottom: 10,
    height: 30
  },
  reproductorContainerLine: {
    flexDirection: "row",
    width: "90%",
    alignItems: "center",
    justifyContent: "center",
    paddingBottom: 10,
    height: 30
  },
  reproductorContainerImage: {
    width: "20%",
    height: 50,
    alignItems: "center",
    justifyContent: "center"
  },
  reproductorTimes: {
    width: "10%"
  },
  reproductorTimeText: {
    fontSize: 11,
    color: "#828282"
  },
  reproductorLineTime: {
    width: "80%"
  },

  reproductorBtnNext: {
    width: 19,
    height: 22
  },
  reproductorReturnAudio: {
    width: 20.92,
    height: 23.4
  },
  reproductorBtnPlay: {
    width: 62,
    height: 62,
    top: -30
  }
});
