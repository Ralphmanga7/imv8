import { Constants } from "../constants/constants.js";

exports.Login = async (data) =>{
	let uri  = (Constants.API_BASE + "api/v1/user/login");
	
    let response = await fetch(
    	uri,
          {
            method: "POST",
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json"
            },
            body:  JSON.stringify(data)
          }
    );

    return response;
}


exports.Signup = async (data) => {
	let uri  = (Constants.API_BASE + "api/v1/user");
	
    let response = await fetch(
    	uri,
          {
            method: "POST",
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json"
            },
            body:  JSON.stringify(data)
          }
    );

    return response;
}