import React, { Component } from "react";
import { StyleSheet, View, Image, ImageBackground, TouchableOpacity, Linking } from "react-native";
import { GlobalStyles } from "../../../resources/style/style";
import PropTypes from "prop-types";
export default class Promo extends Component {
  constructor() {
    super();
    this.state = {
      inputRef: null
    };
  }

  ads = ()=>{
     Linking.canOpenURL("https://www.mindbodywellnessstudio.com/").then(supported => {
      if (supported) {
        Linking.openURL("https://www.mindbodywellnessstudio.com/");
      } else {
        console.log("Don't know how to open URI: https://www.mindbodywellnessstudio.com/");
      }
    });      
  }

  render() {
    return (
      <View style={styles.containerRow}>
        <TouchableOpacity onPress={this.ads}>
          <Image
            style={styles.promo}
            source={require("../../../resources/img/promo.png")}
          />
          </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerRow: {
    display: "flex",
    width: "100%",
    height: 85,
    padding: 10
  },
  promo: {
    width: "100%",
    height: 60
  }
});
