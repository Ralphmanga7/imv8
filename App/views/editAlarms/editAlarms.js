import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  View,
  FlatList,
  TouchableOpacity,
  Text,
  Vibration,
  Image
} from "react-native";
import AudioRecorderPlayer from "react-native-audio-recorder-player";
import LinearGradient from "react-native-linear-gradient";
import * as Animatable from "react-native-animatable";
import { GlobalStyles } from "../../resources/style/style";
import HeaderAlarm from "../UtilsComponents/Seccions/headerAlarm";
import AlarmEdit from "../UtilsComponents/AlarmEdit/AlarmEdit";
import TabMenu from "../UtilsComponents/Seccions/TabMenu";
import ServiceAlarm from "../../services/AlarmService";
import Reproductor from "../UtilsComponents/Seccions/Reproductor";
import Promo from "../UtilsComponents/Seccions/promo";
import Search from "../UtilsComponents/Seccions/search";
import i18n from "../../AppGlobalConfig/Localization/i18n";
import TimerMixin from "react-timer-mixin";
import PlayList from "../../provider/playList";

export default class EditAlarms extends Component {
  constructor(props) {
    super(props);
    this.state = { alarmList: [] };
  }

  componentWillMount() {
    const { navigation } = this.props;
    const type = navigation.getParam("type");
    console.log(type);
    const id = navigation.getParam("id");
    let data = {
      type: type == "AUDIO" || type == "audios" ? "audios" : "defaultAudios",
      id: id
    };

    this.setState({
      typeData:
        data.type == "AUDIO" || data.type == "audios"
          ? "audios"
          : "defaultAudios",
      idAudio: data.id
    });
    console.log(data);
    this.loadAlarmList(data);
  }

  loadAlarmList = async data => {
    ServiceAlarm.getAlarms(data).then(data => {
      console.log("response", data.alarms);
      this.setState({
        alarmList: data.alarms
      });
    });
  };

  deleteAlert = idAlarm => {
    data = {
      type: this.state.typeData,
      id: this.state.idAudio,
      idAlarm: idAlarm
    };
    ServiceAlarm.deleteAlarm(data).then(data => {
      console.log("response", data.alarms);
      this.setState({
        alarmList: data.alarms
      });
    });
  };
  deleteAll = () => {
    data = {
      type: this.state.typeData,
      id: this.state.idAudio
    };
    ServiceAlarm.deleteAll(data).then(data => {
      console.log("response", data.alarms);
      this.setState({
        alarmList: data.alarms
      });
    });
  };
  render() {
    return (
      <View style={styles.container}>
        <Animatable.View style={[styles.container]}>
          <LinearGradient
            start={{ x: 0.0, y: 0.25 }}
            end={{ x: 1.5, y: 0.5 }}
            colors={["#5ccbfa", "#33a2f2"]}
            style={styles.linearGradient}
          >
            <HeaderAlarm
              title={i18n.t("globals.manage-alarms")}
              type={"EditAlarms"}
              delete={this.deleteAll}
            />
          </LinearGradient>

          <FlatList
            data={this.state.alarmList}
            extraData={this.state}
            onRefresh={() => {
              this.setState({ refreshing: true });
              TimerMixin.setTimeout(() => {
                this.setState({ refreshing: false });
              }, 2000);
            }}
            refreshing={this.state.refreshing ? true : false}
            renderItem={({ item }) => (
              <AlarmEdit
                alarm={item}
                activate={this.state.activate}
                typeView={this.state.typeData}
                idAudio={this.state.idAudio}
                onPress={this.deleteAlert}
                onLongPress={this.onShareAudio}
                keyExtractor={(item, index) => item.id}
              />
            )}
          />
        </Animatable.View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  reproductorContainer: {
    display: "flex",
    width: "100%",
    height: 100,
    bottom: 0,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#e0e0e0",
    borderTopColor: "#cad0d4",
    borderTopWidth: 1
  },
  reproductorContainerRow: {
    flexDirection: "row",
    width: "90%",
    alignItems: "center",
    justifyContent: "center",
    paddingBottom: 10,
    height: 30
  },
  reproductorContainerLine: {
    flexDirection: "row",
    width: "90%",
    alignItems: "center",
    justifyContent: "center",
    paddingBottom: 10,
    height: 30
  },
  reproductorContainerImage: {
    width: "20%",
    height: 50,
    alignItems: "center",
    justifyContent: "center"
  },
  reproductorTimes: {
    width: "10%"
  },
  reproductorTimeText: {
    fontSize: 12,
    color: "#828282"
  },
  reproductorLineTime: {
    width: "80%"
  },

  reproductorBtnNext: {
    width: 19,
    height: 22
  },
  reproductorReturnAudio: {
    width: 20.92,
    height: 23.4
  },
  reproductorBtnPlay: {
    width: 62,
    height: 62,
    top: -15
  }
});
