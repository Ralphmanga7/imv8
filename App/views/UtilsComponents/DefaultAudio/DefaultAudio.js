import React, { Component } from "react";
import LinearGradient from "react-native-linear-gradient";
import { GlobalStyles } from "../../../resources/style/style";
import { ToolTipAudio } from "../tooltips/ToolTipAudio";
import PopoverTooltip from "react-native-popover-tooltip";
import PlayList from "../../../provider/playList";
import AlertDialog from "../../UtilsComponents/Dialog/Dialog";
import TimerMixin from "react-timer-mixin";
import { withNavigation } from "react-navigation";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Button,
  TouchableOpacity,
  ImageBackground,
  TextInput,
  ScrollView,
  Image
} from "react-native";
import Moment from "moment";
import PropTypes from "prop-types";
class DefaultAudio extends Component {
  constructor(props) {
    super(props);

    this.state = {
      dialogVisible: false
    };

    Moment.updateLocale("en", {
      ordinal: function(number, token) {
        var b = number % 10;
        var output =
          ~~((number % 100) / 10) === 1
            ? "th"
            : b === 1
            ? "st"
            : b === 2
            ? "nd"
            : b === 3
            ? "rd"
            : "th";
        return number + output;
      }
    });
  }

  selectAudio = async () => {
    try {
      this.props.onPress(this.props.sound);
    } catch (e) {}
  };

  onLongPress = () => {
    try {
      if (this.props.sound) this.props.onLongPress(this.props.sound);
    } catch (e) {
      alert(e);
    }
  };

  items = [
    /* {
      label: () => {
        return <Text style={GlobalStyles.colorTootltipLabel}>Rename</Text>;
      },
      onPress: () => {
        TimerMixin.setTimeout(() => {
          this.setState({ dialogVisible: true });
        }, 500);
      }
    },*/
    {
      label: () => {
        return <Text style={GlobalStyles.colorTootltipLabel}>Set Alarm</Text>;
      },
      onPress: () => {
        //defaultAudios
        this.props.navigation.navigate("Alarms", {
          type: "DEFAULT",
          id: this.props.sound.id
        });
      }
    }
  ];

  closeDialog = () => {
    this.setState({ dialogVisible: false });
  };
  setNameAudio = async value => {
    console.log(value);
    let data = {
      id: this.props.sound.id,
      newTitle: value
    };

    await PlayList.setNameDefaultAffirmation(data).then(data => {
      this.setState({ title: data });
    });
  };
  componentDidMount() {
    console.log(" componentDidMount");
    this.setState({ title: this.props.sound.title });
  }
  componentWillMount() {
    console.log(" componentWillMount");

    this.setState({ title: this.props.sound.title });
  }
  componentWillReceiveProps(props) {
    this.setState({ title: props.sound.title });
  }

  render() {
    return (
      <View
        style={[
          styles.container,
          this.props.activate == this.props.sound.id ? styles.borderLeft : null
        ]}
      >
        <AlertDialog
          visible={this.state.dialogVisible}
          closeAlert={this.closeDialog}
          setValueName={this.setNameAudio}
        />
        {this.props.activate == this.props.sound.id ? (
          <LinearGradient
            start={{ x: -1.0, y: 0 }}
            end={{ x: 1.5, y: 0 }}
            colors={["#33a2f24a", "#33a2f200"]}
            style={[styles.linearGradient]}
          >
            <TouchableOpacity
              style={[styles.cardActiva, {}]}
              onPress={this.selectAudio}
              onLongPress={this.onLongPress}
            >
              <View
                style={{
                  width: "70%"
                }}
              >
                <Text
                  style={[
                    styles.name,
                    styles.activateColor,
                    GlobalStyles.regularText
                  ]}
                >
                  {this.state.title}
                </Text>
              </View>
              <View style={[styles.comtainerFirstLine, { paddingLeft: 17 }]}>
                <Text style={[styles.fistline, GlobalStyles.regularText]}>
                  {this.props.sound.duration.substring(0, 5)}
                </Text>

                <Text
                  style={[
                    styles.secoundLine,
                    GlobalStyles.mediumText,
                    {
                      textAlign: "left"
                    }
                  ]}
                >
                  {this.props.sound.size}
                </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity style={[styles.toolContainer, {}]}>
              <TouchableOpacity
                style={[styles.buttonComponent]}
                onPress={() => {
                  this.refs[this.props.sound.id].toggle(); // open popover tooltips one by one
                }}
              >
                <ImageBackground
                  style={[styles.tooltip, styles.ToolTipImage]}
                  resizeMode="center"
                  resizeMode="contain"
                  source={require("../../../resources/img/tab-buttons.png")}
                />
              </TouchableOpacity>
              <PopoverTooltip
                ref={this.props.sound.id}
                items={this.items}
                labelSeparatorColor="#cad0d4"
                labelStyle="#cad0d4"
                overlayStyle={{ backgroundColor: "#21252912" }}
                tooltipContainerStyle={{
                  padding: 10,
                  marginLeft: -20,
                  marginTop: -55
                }}
              />
            </TouchableOpacity>
          </LinearGradient>
        ) : (
          <View
            style={{
              width: "100%",
              height: 70,
              paddingLeft: 15
            }}
          >
            <TouchableOpacity
              style={[styles.card, {}]}
              onPress={this.selectAudio}
              onLongPress={this.onLongPress}
            >
              <View
                style={{
                  width: "75%"
                }}
              >
                <Text style={[styles.name, GlobalStyles.regularText]}>
                  {this.state.title}
                </Text>
              </View>
              <View style={[styles.comtainerFirstLine]}>
                <Text style={[styles.fistline, GlobalStyles.regularText]}>
                  {this.props.sound.duration.substring(0, 5)}
                </Text>

                <Text
                  style={[
                    styles.secoundLine,
                    GlobalStyles.mediumText,
                    {
                      textAlign: "left"
                    }
                  ]}
                >
                  {this.props.sound.size}
                </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity style={[styles.toolContainer, {}]}>
              <TouchableOpacity
                style={[styles.buttonComponent]}
                onPress={() => {
                  this.refs[this.props.sound.id].toggle(); // open popover tooltips one by one
                }}
              >
                <ImageBackground
                  style={[styles.tooltip, styles.ToolTipImage]}
                  resizeMode="center"
                  resizeMode="contain"
                  source={require("../../../resources/img/tab-buttons.png")}
                />
              </TouchableOpacity>

              <PopoverTooltip
                ref={this.props.sound.id}
                items={this.items}
                labelSeparatorColor="#cad0d4"
                labelStyle="#cad0d4"
                overlayStyle={{ backgroundColor: "#21252912" }}
                tooltipContainerStyle={{
                  padding: 10,
                  marginLeft: -20,
                  marginTop: -55
                }}
              />
            </TouchableOpacity>
          </View>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    display: "flex",
    width: "100%",
    height: 70,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#f1f2f3"
  },
  buttonComponent: {
    width: 35.5,
    height: 65.2,
    zIndex: 100
  },
  ToolTipImage: {
    width: 0.5,
    height: 30.2,
    marginLeft: 38,
    marginTop: 13
  },
  colorTootltipLabel: {
    color: "#cad0d4"
  },
  linearGradient: {
    width: "100%",
    height: 70,
    paddingLeft: 10
  },
  activateColor: {
    color: "#1676bb"
  },
  borderLeft: {
    borderLeftWidth: 5,
    borderLeftColor: "#33a2f2"
  },
  comtainerFirstLine: {
    width: "20%",
    justifyContent: "flex-start",
    alignItems: "flex-start",
    paddingBottom: 12
  },

  containerRow: {
    display: "flex",
    width: "100%",
    height: 53,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#f1f2f3"
  },

  fistline: {
    paddingBottom: 5,
    fontSize: 12
  },
  name: {
    paddingTop: 12
  },
  secoundLine: {
    color: "#616770",
    fontSize: 11
  },
  title: {
    color: "#fff",
    fontSize: 25,
    paddingTop: 30,
    paddingLeft: 30
  },
  subTitle: {
    color: "#fff",
    fontSize: 12,
    paddingTop: 5,
    paddingLeft: 27
  },
  card: {
    width: "95%",
    paddingTop: 10,
    height: 10,
    flex: 1,
    flexDirection: "row",
    borderBottomColor: "#cad0d4",
    borderBottomWidth: 0.5
  },
  cardActiva: {
    width: "95%",
    paddingTop: 10,
    height: 10,
    flex: 1,
    flexDirection: "row",
    borderBottomColor: "#cad0d4",
    borderBottomWidth: 0.5
  },
  tooltip: {
    height: 45.2,
    width: 35.5,
    padding: 10,
    top: 5,
    right: 22
  },
  toolContainer: {
    width: "10%",
    justifyContent: "flex-end",
    alignItems: "flex-end",
    padding: 0,
    position: "absolute",
    right: 17
  }
});
export default withNavigation(DefaultAudio);
