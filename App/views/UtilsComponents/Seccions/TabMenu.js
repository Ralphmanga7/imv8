import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Button,
  TouchableOpacity,
  StatusBar,
  TextInput,
  ScrollView,
  PixelRatio
} from "react-native";
import { GlobalStyles } from "../../../resources/style/style";
import { withNavigation } from "react-navigation";
import PropTypes from "prop-types";
import i18n from "../../../AppGlobalConfig/Localization/i18n";

class TabMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inputRef: null,
      isCorrect: 0
    };
    console.log(this.state, "props");
  }
  componentDidMount() {
    this.setState({ value: 1 });
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={[styles.containerRow]}>
          <TouchableOpacity
            style={[
              styles.TabActive,
              styles.containerTab,
              {
                alignItems: "flex-start",
                paddingLeft: 0
              }
            ]}
            onPress={() => {
              this.props.onLeave();
              this.props.navigation.navigate("MyAudioView");
            }}
          >
            {this.props.value == 1 ? (
              <View
                style={{
                  borderBottomColor: "#33a2f2",
                  borderBottomWidth: 2,
                  padding: 0
                }}
              >
                <Text
                  style={[
                    styles.title,
                    GlobalStyles.mediumText,
                    {
                      textAlign: "center",
                      height: 48,
                      paddingTop: 15,
                      color: "#1676bb"
                    }
                  ]}
                >
                  Listen
                </Text>
              </View>
            ) : (
              <Text style={[styles.title, GlobalStyles.mediumText]}>
                Listen
              </Text>
            )}
          </TouchableOpacity>
          <TouchableOpacity
            style={[
              styles.containerTab,
              styles.containerTab,
              {
                alignItems: "flex-start",
                paddingLeft: 0
              }
            ]}
            onPress={() => {
              this.props.onLeave();
              this.props.navigation.navigate("Record");
            }}
          >
            {this.props.value == 2 ? (
              <View
                style={{
                  borderBottomColor: "#33a2f2",
                  borderBottomWidth: 4,
                  padding: 0
                }}
              >
                <Text
                  style={[
                    styles.title,
                    GlobalStyles.mediumText,
                    {
                      textAlign: "center",
                      height: 50,
                      paddingTop: 17,
                      color: "#1676bb"
                    }
                  ]}
                >
                  Record
                </Text>
              </View>
            ) : (
              <Text style={[styles.title, GlobalStyles.mediumText]}>
                Record
              </Text>
            )}
          </TouchableOpacity>
          <TouchableOpacity
            style={[
              styles.containerTabBig,
              {
                alignItems: "flex-start",
                paddingLeft: 0
              }
            ]}
            onPress={() => {
              this.props.onLeave();
              this.props.navigation.navigate("DefaultAffirmation");
            }}
          >
            {this.props.value == 3 ? (
              <View
                style={{
                  borderBottomColor: "#33a2f2",
                  borderBottomWidth: 2,
                  padding: 0
                }}
              >
                <Text
                  style={[
                    styles.title,
                    GlobalStyles.mediumText,
                    {
                      textAlign: "center",
                      height: 50,
                      paddingTop: 17,
                      color: "#1676bb"
                    }
                  ]}
                >
                  Default Affirmations
                </Text>
              </View>
            ) : (
              <Text style={[styles.title, GlobalStyles.mediumText]}>
                Default Affirmations
              </Text>
            )}
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

TabMenu.defaultProps = {
  textColor: "#828282",
  onLeave: () => {}
};

const styles = StyleSheet.create({
  container: {
    display: "flex",
    width: "100%",
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#f1f2f3",
    borderBottomColor: "#cad0d4",
    borderBottomWidth: 0.5
  },
  containerTab: {
    width: "25%",
    height: 50,
    alignItems: "center",
    justifyContent: "center"
  },
  containerTabBig: {
    width: "50%",
    height: 50,
    alignItems: "center",
    justifyContent: "center"
  },
  containerRow: {
    flexDirection: "row",
    width: "90%",
    paddingTop: 10,
    paddingBottom: 10
  },

  title: {
    color: "#616770",
    textAlign: "center",
    fontSize: 11
  },
  titleActive: {
    paddingRight: 15,
    fontWeight: "bold"
  }
});
export default withNavigation(TabMenu);
