import { StyleSheet, PixelRatio, Platform, Dimensions } from "react-native";

let _pixelRatio = PixelRatio.get();

isIphoneXorAbove = (s, n)=> {
  const dimen = Dimensions.get('window');
  
  if((
    Platform.OS === 'ios' &&
    !Platform.isPad &&
    !Platform.isTVOS &&
    ((dimen.height === 812 || dimen.width === 812) || (dimen.height === 896 || dimen.width === 896))
  )){

    return s;

  }

  return n;
}

exports.GlobalStyles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center"
  },
  backgroundColor: {},
  containerRow: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    width: "100%",
    paddingTop: 7 * PixelRatio.get()
  },
  containerRow_Android: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    width: "100%",
    paddingTop: 2 * PixelRatio.get()
  },

  linearGradient: {
    minHeight: "100%",
    maxHeight: "100%",
    width: "100%",
    paddingLeft: 20,
    paddingRight: 20
  },

  containerForm: {
    minHeight: "100%",
    maxHeight: "100%",
    marginBottom: 20
  },
  generalText: {
    color: "#fff",
    padding: 5,
    fontSize: 14
  },

  generalText_android: {
    color: "#fff",
    padding: 4,
    fontSize: 1
  },

  titleText: {
    fontSize: 18 * _pixelRatio
  },

  titleText_android: {
    fontSize: isIphoneXorAbove((8.5 * _pixelRatio), (12 * _pixelRatio)) 
  },

  smallText: {
    fontSize: 4 * _pixelRatio
  },
  mediumText: {
    fontSize: isIphoneXorAbove((3.6 * _pixelRatio), (5 * _pixelRatio))
  },
  mediumUpText: {
    fontSize: isIphoneXorAbove((4.0 * _pixelRatio), (6 * _pixelRatio))
  },
  mediumText_android: {
    fontSize: 8 * _pixelRatio
  },
  font_8: {
    fontSize: 8 * _pixelRatio
  },
  regularText: {
    fontSize: isIphoneXorAbove((3.6 * _pixelRatio), (6 * _pixelRatio))
  },

  regularText_android: {
    fontSize: 6 * _pixelRatio
  },
  titleHeader: {
    fontSize: 10 * _pixelRatio
  },
  colorTootltipLabel: {
    color: "#616770"
  }
});
