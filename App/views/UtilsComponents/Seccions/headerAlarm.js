import React, { Component } from "react";
import { GlobalStyles } from "../../../resources/style/style";
import i18n from "../../../AppGlobalConfig/Localization/i18n";
import Search from "./search";
import PopoverTooltip from "react-native-popover-tooltip";

import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ImageBackground,
  TextInput,
  AsyncStorage,
  Image
} from "react-native";

import { withNavigation } from "react-navigation";

import PropTypes from "prop-types";
class HeaderAlarm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inputRef: null,
      value: "",
      isCorrect: 0,
      menu: 0
    };
  }

  render() {
    return [
      <View style={styles.container}>
        <View style={styles.containerMenu}>
          <View style={{ width: "50%" }}>
            {this.props.type == "listAlarms" ||
            this.props.type == "EditAlarms" ? (
              <TouchableOpacity
                style={[]}
                onPress={() => {
                  this.props.navigation.goBack();
                }}
              >
                <Image
                  style={{ width: 28, height: 15 }}
                  source={require("../../../resources/img/back.png")}
                />
              </TouchableOpacity>
            ) : null}

            {this.props.type == "addAlarm" ? (
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate("Alarms");
                }}
              >
                <Text
                  style={[
                    styles.subTitle,
                    GlobalStyles.mediumText,
                    { paddingLeft: 0 }
                  ]}
                >
                  Cancel
                </Text>
              </TouchableOpacity>
            ) : null}
          </View>
          <View
            style={{
              width: "50%",
              alignItems: "flex-end",
              justifyContent: "flex-end"
            }}
          >
            {this.props.type == "EditAlarms" ? (
              <TouchableOpacity
                onPress={() => {
                  this.props.delete();
                }}
              >
                <Text style={[styles.subTitle, GlobalStyles.mediumText]}>
                  DELETE ALL
                </Text>
              </TouchableOpacity>
            ) : null}
            {this.props.type == "addAlarm" ? (
              <TouchableOpacity
                onPress={async () => {
                  await this.props.save();
                  await this.props.navigation.navigate("Alarms", {
                    type: this.props.typeData,
                    id: this.props.idAudio
                  });
                }}
              >
                <Text style={[styles.subTitle, GlobalStyles.mediumText]}>
                  Save
                </Text>
              </TouchableOpacity>
            ) : null}
          </View>
        </View>
        <View />
        <View style={{ flex: 1, flexDirection: "row", marginTop: -20 }}>
          <View style={{ width: "70%" }}>
            <Text style={[styles.title, GlobalStyles.titleHeader]}>
              {this.props.title}
            </Text>
            <Text style={[styles.subTitle, GlobalStyles.mediumText]}>
              Vivamus sed vestibulum veilt
            </Text>
          </View>
          {this.props.disableEdit ? null : (
          <View style={styles.btnRigth}>
            {this.props.type == "listAlarms" ? (
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate("EditAlarms", {
                    type: this.props.typeData,
                    id: this.props.idAudio
                  });
                }}
              >
                <Text style={[styles.subTitle, GlobalStyles.mediumText]}>
                  EDIT
                </Text>
              </TouchableOpacity>
            ) : null}
            {this.props.type == "EditAlarms" ? (
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate("Alarms");
                }}
              >
                <Text style={[styles.subTitle, GlobalStyles.mediumText]}>
                  DONE
                </Text>
              </TouchableOpacity>
            ) : null}
          </View>)}
        </View>
      </View>
    ];
  }
}

export default withNavigation(HeaderAlarm);

HeaderAlarm.propTypes = {
  title: PropTypes.string
};

HeaderAlarm.defaultProps = {
  title: i18n.t("globals.default-title")
};

const styles = StyleSheet.create({
  container: {
    display: "flex",
    width: "100%",
    height: 135
  },
  containerMenu: {
    flex: 1,
    flexDirection: "row",
    width: "100%",
    alignItems: "flex-start",
    justifyContent: "flex-start",
    paddingTop: 20,
    paddingLeft: 20,
    paddingRight: 17,
    paddingBottom: 0
  },
  btnRigth: {
    alignItems: "flex-end",
    justifyContent: "flex-end",
    width: "30%",
    paddingBottom: 14,
    paddingRight: 15
  },
  title: {
    color: "#fff",
    fontSize: 26,
    paddingLeft: 20
  },
  subTitle: {
    color: "#fff",
    fontSize: 11,
    paddingTop: 0,
    paddingLeft: 20
  }
});
