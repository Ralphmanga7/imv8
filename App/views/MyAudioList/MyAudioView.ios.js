import React, { Component } from "react";

import {
  ProgressViewIOS,
  Platform,
  StyleSheet,
  View,
  FlatList,
  TouchableOpacity,
  Text,
  AsyncStorage,
  Vibration,
  Image,
  Alert
} from "react-native";
import Slider from "react-native-slider";
import AudioRecorderPlayer from "react-native-audio-recorder-player";
import LinearGradient from "react-native-linear-gradient";
import * as Animatable from "react-native-animatable";
import { GlobalStyles } from "../../resources/style/style";
import Header from "../UtilsComponents/Seccions/header";
import Audio from "../UtilsComponents/Audio/Audio";
import TabMenu from "../UtilsComponents/Seccions/TabMenu";
import PlayList from "../../provider/playList";
import Reproductor from "../UtilsComponents/Seccions/Reproductor";
import Promo from "../UtilsComponents/Seccions/promo";
import Search from "../UtilsComponents/Seccions/search";
import i18n from "../../AppGlobalConfig/Localization/i18n";
import TimerMixin from "react-timer-mixin";
import RNFetchBlob from "rn-fetch-blob";
import Share from "react-native-share";

export default class MyAudioView extends Component {
  constructor(props) {
    super(props);

    this.state = {
      ActiveTab: 1,
      playList: [],
      link: "",
      playableAudio: false,
      isRecording: false,
      bgColor: "#333",
      audio: "",
      onTransmission: true,
      url: "",
      index  : -1,
      progress  : 0,
      session : false
    };

    this._bootstrapAsync();
  }

    // Fetch the token from storage then navigate to our appropriate place
  _bootstrapAsync = async () => {
    const user = await AsyncStorage.getItem("userToken");
    this.setState({ session : JSON.parse(user) });
    this.loadAudioList();
  };

  componentDidMount() {
    this.requestPermisions();
    this.audioRecorderPlayer = new AudioRecorderPlayer();

    let progress = this.state.progress + 0.01;
    this.setState({ progress });
  }

  componentWillReceiveProps(props) {
    this.loadAudioList();
  }

  loadAudioList = async () => {
    console.warn("_id", this.state.session.user._id);
    //await AsyncStorage.removeItem("audios");
    await PlayList.get(this.state.session.user._id || this.state.session.id).then(data => {
      console.log("lista de reproducción", data);
      this.setState({
        playList: data
      });
    });
  };

  requestPermisions = async () => {
    try {
      if (Platform.OS === "android") {
        const micPermission = await checkPermission("microphone");
        console.log("micPermission", micPermission);
        if (micPermission !== "authorized") {
          const micRequest = await requestPermission("microphone");
          console.log("micRequest", micRequest);
          if (micRequest !== "authorized") {
            return;
          }
        }

        const storagePermission = await checkPermission("storage");

        if (storagePermission !== "authorized") {
          const storageRequest = await requestPermission("storage");
          if (storageRequest !== "authorized") {
            return;
          }
        }
      }
    } catch (e) {}
  };

  onPausePlay = async () => {
    await this.audioRecorderPlayer.pausePlayer();
    this.setState({
      onTransmission: true
    });
  };

  onStopRecord = async () => {
    const result = await this.audioRecorderPlayer.stopRecorder();
    this.audioRecorderPlayer.removeRecordBackListener();
    this.setState({
      recordSecs: 0,
      isRecording: false,
      playableAudio: true,
      bgColor: "#333",
      audio: result,
      onTransmission: true
    });
  };

  onStartPlayWithoutUrl = async () => {
    if(this.state.index <= -1){
        this.selectAudio(this.state.playList[0]);
      return;
    }

    const msg = await this.audioRecorderPlayer.startPlayer();
    
    this.audioRecorderPlayer.addPlayBackListener(e => {
      if (e.current_position === e.duration) {
        console.log("finished");
        this.audioRecorderPlayer.stopPlayer();
      }
      this.setState({
        currentPositionSec: e.current_position,
        currentDurationSec: e.duration,
        playTime: this.audioRecorderPlayer.mmssss(
          Math.floor(e.current_position)
        ),
        duration: this.audioRecorderPlayer.mmssss(Math.floor(e.duration))
      });
      return;
    });
  };

  onStartPlay = async url => {
    console.warn("play");
    var base64 = require("base-64");
    RNFetchBlob.fs.readFile(url, "base64").then(data => {
      console.log("======== data ==============");
      console.log(data);
      var decodedData = base64.decode(data);
      var bytes = decodedData.length;
      if (bytes < 1024) console.log(bytes + " Bytes");
      else if (bytes < 1048576)
        console.log("KB:" + (bytes / 1024).toFixed(3) + " KB");
      else if (bytes < 1073741824)
        console.log("MB:" + (bytes / 1048576).toFixed(2) + " MB");
      else console.log((bytes / 1073741824).toFixed(3) + " GB");
    });

    this.audioRecorderPlayer = new AudioRecorderPlayer();
    const msg = await this.audioRecorderPlayer.startPlayer(url);
    console.log(msg);

    this.audioRecorderPlayer.addPlayBackListener(e => {
      this.setState({
        currentPositionSec: e.current_position,
        currentDurationSec: e.duration,
        playTime: this.audioRecorderPlayer.mmssss(
          Math.floor(e.current_position)
        ),
        duration: this.audioRecorderPlayer.mmssss(Math.floor(e.duration)),
        duration_sec : e.duration,
        isPlaying: true,
        isPaused: false,
        progress :  Math.floor(e.current_position) / Math.floor(e.duration),
        onTransmission: false
      });
      if (e.current_position === e.duration) {
        console.log("finished");
        this.setState({
          onTransmission: true
        });
        this.onStopPlay();
      }
      return;
    });
  };

  onStopPlay = async () => {
    console.log("onStopPlay");
    this.state.playTime = "00:00:00";
    this.state.duration = "00:00:00";
    this.audioRecorderPlayer.stopPlayer();
    this.audioRecorderPlayer.removePlayBackListener();
  };

  selectAudio = currentAudio => {
    this.onStopPlay();
    this.setState({
      activate: currentAudio.id,
      index: this.state.playList.indexOf(currentAudio),
      selected_audio : currentAudio
    });
    if (currentAudio.url) {
      let uri = Platform.select({
        ios: currentAudio.url.split("/")[
          currentAudio.url.split("/").length - 1
        ],
        android: currentAudio.url
      });

      this.setState({
        url: uri
      });

      this.onStartPlay(uri);
    }
  };

  onShareAudio = async audio => {
    //console.warn(audio, "audio");

    // Vibration.vibrate([300]);

    let fileUrl = audio.url;
    console.warn(fileUrl, "FILE URL");

    let audioPath =
      RNFetchBlob.fs.dirs.CacheDir.replace("Caches", "tmp").replace(
        "/Library",
        ""
      ) +
      "/" +
      audio.url.split("/")[audio.url.split("/").length - 1];

    let options = {
      type: "audio/m4a",
      url: audioPath
    };

    await Share.open(options);

    //let know to the user that the audio was shared succesful
    alert(i18n.t("globals.audio-was-shared-ok"));
  };
  getTypeSearch = data => {
    if (data == "name") {
      const type = "audios";
      PlayList.getOrderName(type).then(data => {
        //alert(data.length)
        this.setState({
          playList: data
        });
      });
    }
    if (data.type == "filter") {
      let value = data.value;

      PlayList.getByCriteria(value).then(data => {
        this.setState({
          playList: data
        });
      });
    }
    if (data == "date") {
      PlayList.get().then(data => {
        this.setState({
          playList: data
        });
      });
    }
    if (data == "Lenght") {
      const type = "audios";
      PlayList.getOrderLenght(type).then(data => {
        this.setState({
          playList: data
        });
      });
    }
    if (data == "size") {
      const type = "audios";
      PlayList.getOrderSize(type).then(data => {
        this.setState({
          playList: data
        });
      });
    }
    if (data == "oldest") {
      PlayList.getOldestSize().then(data => {
        this.setState({
          playList: data
        });
      });
    }
    if (data == "newest") {
      PlayList.getNewestSize().then(data => {
        this.setState({
          playList: data
        });
      });
    }
  };

  rewind = async () => {
    if (this.state.isPlaying) {
      console.warn("back");

      let totalDuration = this.state.currentDurationSec / 1000;
      let current = this.state.currentPositionSec / 1000;
      let rewind = Math.floor(current - 15);

      console.warn(rewind);

      if (rewind > 0)
        await this.audioRecorderPlayer.seekToPlayer(rewind * 1000);
      else await this.audioRecorderPlayer.seekToPlayer(0);
    }
  };

  forward = async () => {
    if (this.state.isPlaying) {
      console.warn("forward");

      let totalDuration = this.state.currentDurationSec / 1000;
      let current = this.state.currentPositionSec / 1000;
      let forward = Math.floor(current + 15);

      console.warn(forward);

      if (forward <= totalDuration)
        await this.audioRecorderPlayer.seekToPlayer(forward * 1000);
      else alert(i18n.t("globals.duration_lower"));
    }
  };

  next = () => {
    try {
      return this.selectAudio(this.state.playList[this.state.index + 1]);
    } catch (e) {
      this.selectAudio(this.state.playList[0]);
    }
  };

  previous = () => {
    try {
      return this.selectAudio(this.state.playList[this.state.index - 1]);
    } catch (e) {
      this.selectAudio(this.state.playList[this.state.playList.length - 1]);
    }
  };

  deleteAudio = id => {
    // Works on both iOS and Android
    Alert.alert("Confirmation", "Are you sure to delete this Audio?", [
      {
        text: "Cancel",
        onPress: () => console.log("delete canceled"),
        style: "cancel"
      },
      {
        text: "Delete",
        onPress: () => {
          try {
            PlayList.delete(id).then(data => {
              this.setState({
                playList: data
              });

              alert("Audio removed succesful");
            });
          } catch (e) {
            alert("Audio can't be removed");
          }
        }
      }
    ]);
  };

  render() {
    return (
      <View style={styles.container}>
        <Animatable.View
          animation="fadeInLeft"
          delay={1200}
          duration={700}
          style={[styles.container]}
        >
          <LinearGradient
            start={{ x: 0.0, y: 0.25 }}
            end={{ x: 1.5, y: 0.5 }}
            colors={["#5ccbfa", "#33a2f2"]}
            style={styles.linearGradient}
          >
            <Header updateSearch={this.getTypeSearch} />
          </LinearGradient>
          <TabMenu
            update={this.changeTab}
            onPress={this.onStopPlay}
            value={1}
            onLeave={this.onStopPlay}
          />

          <Promo />

          <FlatList
            data={this.state.playList}
            extraData={this.state}
            onRefresh={() => {
              this.loadAudioList();
              this.setState({ refreshing: true });
              TimerMixin.setTimeout(() => {
                this.setState({ refreshing: false });
              }, 1000);
            }}
            refreshing={this.state.refreshing ? true : false}
            renderItem={({ item }) => (
              <Audio
                sound={item}
                delete={this.deleteAudio}
                activate={this.state.activate}
                onPress={this.selectAudio}
                onLongPress={this.onShareAudio}
                keyExtractor={(item, index) => item.id }
              />
            )}
          />

          <View style={styles.reproductorContainer}>
            <View style={styles.reproductorContainerRow}>
              <TouchableOpacity
                style={[
                  styles.reproductorContainerImage,
                  { alignItems: "flex-end" }
                ]}
                onPress={() => {
                  this.rewind();
                }}
              >
                <Image
                  style={styles.reproductorReturnAudio}
                  source={require("../../resources/img/15left.png")}
                />
              </TouchableOpacity>
              <TouchableOpacity
                style={[styles.reproductorContainerImage]}
                onPress={() => {
                  this.previous();
                }}
              >
                <Image
                  style={styles.reproductorBtnNext}
                  source={require("../../resources/img/leftSong.png")}
                />
              </TouchableOpacity>

              {this.state.onTransmission == true ? (
                <TouchableOpacity style={styles.reproductorBtnPlayContainer} onPress={this.onStartPlayWithoutUrl}>
                  <Image
                    style={styles.reproductorBtnPlay}
                    source={require("../../resources/img/play.png")}
                  />
                </TouchableOpacity>
              ) : (
                <TouchableOpacity
                  style={styles.reproductorBtnPlayContainer}
                  onPress={this.onPausePlay}
                >
                <Image
                  style={[styles.reproductorBtnPlay]}
                  source={require("../../resources/img/paused.png")}
                />
                </TouchableOpacity>
              )}
              <TouchableOpacity
                style={styles.reproductorContainerImage}
                onPress={() => {
                  this.next();
                }}
              >
                <Image
                  style={styles.reproductorBtnNext}
                  source={require("../../resources/img/rightSong.png")}
                />
              </TouchableOpacity>
              <TouchableOpacity
                style={[
                  styles.reproductorContainerImage,
                  { alignItems: "flex-start" }
                ]}
                onPress={() => {
                  this.forward();
                }}
              >
                <Image
                  style={styles.reproductorReturnAudio}
                  source={require("../../resources/img/15right.png")}
                />
              </TouchableOpacity>
            </View>
            <View style={styles.reproductorContainerLine}>
              <View styles={styles.reproductorTimes}>
                <Text
                  style={[styles.reproductorTimeText, { paddingRight: 10 }]}
                >
                  {this.state.playTime || "00:00:00"}
                </Text>
              </View>

              <View styles={styles.reproductorLineTime}>
                  <Slider
                    value={this.state.progress}
                    thumbStyle = {{width: 10, height: 10}}
                    thumbTintColor = {"#33a2f2"}
                    minimumTrackTintColor = {"#33a2f2"}
                    trackStyle = {{ backgroundColor: "#808080", height : 1 }}
                    style={{ width: 208, height: 1}}
                    onValueChange={value => this.setState({ value })}
                  />
              </View>
              <View styles={styles.reproductorTimes}>
                <Text style={[styles.reproductorTimeText, { paddingLeft: 10 }]}>
                  {this.state.duration || "00:00:00"}
                </Text>
              </View>
            </View>
          </View>
        </Animatable.View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  reproductorContainer: {
    display: "flex",
    width: "100%",
    height: 100,
    bottom: 0,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#e0e0e0",
    borderTopColor: "#cad0d4",
    borderTopWidth: 1
  },
  reproductorContainerRow: {
    flexDirection: "row",
    width: "90%",
    alignItems: "center",
    justifyContent: "center",
    paddingBottom: 10,
    height: 30
  },
  reproductorContainerLine: {
    flexDirection: "row",
    width: "90%",
    alignItems: "center",
    justifyContent: "center",
    paddingBottom: 10,
    height: 30
  },
  reproductorContainerImage: {
    width: "20%",
    height: 50,
    alignItems: "center",
    justifyContent: "center"
  },
  reproductorTimes: {
    width: "10%"
  },
  reproductorTimeText: {
    fontSize: 12,
    color: "#828282"
  },
  reproductorLineTime: {
    width: "80%"
  },

  reproductorBtnNext: {
    width: 19,
    height: 22
  },
  reproductorReturnAudio: {
    width: 20.92,
    height: 23.4
  },
  reproductorBtnPlay: {
    width: 62,
    height: 62,
    top: -15
  },
  progress: {
      height:3,
      backgroundColor:'#ccc',
      borderRadius:2,
      marginLeft: 10,
      marginRight: 10,
  }

});

/*



*/
