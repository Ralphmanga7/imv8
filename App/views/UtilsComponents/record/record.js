import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity
} from "react-native";
import AudioRecorderPlayer from "react-native-audio-recorder-player";
import i18n from "../../../AppGlobalConfig/Localization/i18n";
import { withNavigation } from "react-navigation";

class Recorder extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isRecording: false,
      bgColor: "#333"
    };
  }

  componentDidMount() {
    this.requestPermisions();
    this.audioRecorderPlayer = new AudioRecorderPlayer();
  }

  requestPermisions = async () => {
    try {
      if (Platform.OS === "android") {
        const micPermission = await checkPermission("microphone");
        console.log("micPermission", micPermission);
        if (micPermission !== "authorized") {
          const micRequest = await requestPermission("microphone");
          console.log("micRequest", micRequest);
          if (micRequest !== "authorized") {
            return;
          }
        }

        const storagePermission = await checkPermission("storage");

        if (storagePermission !== "authorized") {
          const storageRequest = await requestPermission("storage");
          if (storageRequest !== "authorized") {
            return;
          }
        }
      }
    } catch (e) {}
  };

  onStartRecord = async () => {
    console.log(this.audioRecorderPlayer.startRecorder, "audioRecorderPlayer");
    const result = await this.audioRecorderPlayer.startRecorder();
    this.audioRecorderPlayer.addRecordBackListener(e => {
      this.setState({
        recordSecs: e.current_position,
        recordTime: this.audioRecorderPlayer.mmssss(
          Math.floor(e.current_position)
        ),
        isRecording: true,
        bgColor: "red"
      });
      return;
    });
    console.log(result);
  };

  onStopRecord = async () => {
    const result = await this.audioRecorderPlayer.stopRecorder();
    this.audioRecorderPlayer.removeRecordBackListener();
    this.setState({
      recordSecs: 0,
      isRecording: false,
      playableAudio: true,
      bgColor: "#333"
    });
    console.log(result);
  };

  onStartPlay = async () => {
    console.log("onStartPlay");
    const msg = await this.audioRecorderPlayer.startPlayer();
    console.log(msg);
    this.audioRecorderPlayer.addPlayBackListener(e => {
      if (e.current_position === e.duration) {
        console.log("finished");
        this.audioRecorderPlayer.stopPlayer();
      }
      this.setState({
        currentPositionSec: e.current_position,
        currentDurationSec: e.duration,
        playTime: this.audioRecorderPlayer.mmssss(
          Math.floor(e.current_position)
        ),
        duration: this.audioRecorderPlayer.mmssss(Math.floor(e.duration)),
        isPlaying: true,
        isPaused: false
      });
      return;
    });
  };

  onPausePlay = async () => {
    await this.audioRecorderPlayer.pausePlayer();
    this.setState({
      isPlaying: false,
      isPaused: true
    });
  };

  onStopPlay = async () => {
    console.log("onStopPlay");
    this.audioRecorderPlayer.stopPlayer();
    this.audioRecorderPlayer.removePlayBackListener();
  };

  resetPlayable = () => {
    this.onStopPlay();
    this.audioRecorderPlayer.stopPlayer();
    this.audioRecorderPlayer.removePlayBackListener();

    this.setState({
      playableAudio: false,
      isRecording: false,
      recordTime: null,
      isPlaying: false,
      isPaused: false
    });
  };

  goToAudioList = () => {
    this.props.navigation.navigate("MyAudioView");
  };

  render() {
    return (
      <View style={[styles.container]}>
        <View>
          <View style={styles.containerRow}>
            {!this.state.playableAudio ? (
              <View style={styles.containerRow}>
                <Text style={{ fontSize: 15 }}>
                  {this.state.recordTime || "00:00"}
                </Text>
              </View>
            ) : null}
            {!this.state.playableAudio && !this.state.isRecording ? (
              <View style={styles.containerRow}>
                <TouchableOpacity
                  style={styles.containerImage}
                  onPress={this.onStartRecord}
                >
                  <Image
                    style={{ width: 56, height: 56 }}
                    source={require("../../../resources/img/record.png")}
                  />
                </TouchableOpacity>
              </View>
            ) : null}
          </View>
        </View>

        {!this.state.playableAudio && !this.state.isRecording ? (
          <View
            style={{
              flexDirection: "row",
              marginTop: 25,
              backgroundColor: "#0366d6",
              padding: 15,
              borderRadius: 5
            }}
          >
            <TouchableOpacity onPress={this.goToAudioList}>
              <Text style={[styles.btn]}>
                {i18n.t("globals.go-to-my-audios")}
              </Text>
            </TouchableOpacity>
          </View>
        ) : null}

        {this.state.isRecording ? (
          <View
            style={{
              flexDirection: "row",
              marginTop: 25,
              backgroundColor: "red",
              padding: 5,
              borderRadius: 5
            }}
          >
            <TouchableOpacity onPress={this.onStopRecord}>
              <Text style={styles.btn}>
                {i18n.t("recorder-component.stop-recording")}
              </Text>
            </TouchableOpacity>
          </View>
        ) : null}

        {this.state.playableAudio &&
        !this.state.isPlaying &&
        !this.state.isPaused ? (
          <View
            style={{
              flexDirection: "column",
              alignItems: "center",
              padding: 10
            }}
          >
            <Text
              style={{
                color: "white",
                textAlign: "center",
                fontWeight: "bold"
              }}
            >
              {i18n.t("recorder-component.audio-recorded-ok-title")}
            </Text>
            <Text style={{ color: "white", textAlign: "center", marginTop: 5 }}>
              {i18n.t("recorder-component.audio-recorded-ok-text")}
            </Text>
          </View>
        ) : null}

        {(this.state.playableAudio && this.state.isPlaying) ||
        (this.state.playableAudio && this.state.isPaused) ? (
          <View
            style={{
              flexDirection: "column",
              alignItems: "center",
              padding: 10
            }}
          >
            <Text
              style={{
                color: "white",
                textAlign: "center",
                fontWeight: "bold"
              }}
            >
              {this.state.playTime + " of " + this.state.duration}
            </Text>
          </View>
        ) : null}

        {this.state.playableAudio && !this.state.isPlaying ? (
          <View
            style={{
              flexDirection: "row",
              marginTop: 20,
              backgroundColor: "#0366d6",
              padding: 17,
              paddingTop: 5,
              paddingBottom: 5,
              borderRadius: 5
            }}
          >
            <TouchableOpacity onPress={this.onStartPlay}>
              <Text style={styles.btn}>
                {i18n.t("recorder-component.play-audio")}
              </Text>
            </TouchableOpacity>
          </View>
        ) : null}

        {this.state.playableAudio && this.state.isPlaying ? (
          <View
            style={{
              flexDirection: "row",
              marginTop: 20,
              backgroundColor: "#0366d6",
              padding: 17,
              paddingTop: 5,
              paddingBottom: 5,
              borderRadius: 5
            }}
          >
            <TouchableOpacity onPress={this.onPausePlay}>
              <Text style={styles.btn}>
                {i18n.t("recorder-component.pause-audio")}
              </Text>
            </TouchableOpacity>
          </View>
        ) : null}

        {this.state.playableAudio ? (
          <View
            style={{
              flexDirection: "row",
              marginTop: 20,
              backgroundColor: "red",
              padding: 25,
              paddingTop: 5,
              paddingBottom: 5,
              borderRadius: 5
            }}
          >
            <TouchableOpacity onPress={this.resetPlayable}>
              <Text style={styles.btn}>
                {i18n.t("recorder-component.go-back")}
              </Text>
            </TouchableOpacity>
          </View>
        ) : null}
      </View>
    );
  }
}

import { Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");
const calRatio = 16 * (width / height);

const screenWidth = width;
const screenHeight = height;
const ratio = (calRatio < 9 ? width / 9 : height / 18) / (360 / 9);

const styles = StyleSheet.create({
  container: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    width: "100%",
    height: "100%"
  },
  containerRow: {
    flexDirection: "row",
    width: "100%",
    alignItems: "center",
    justifyContent: "center"
  },
  timerItem: {
    marginTop: 12 * ratio,
    color: "white",
    fontSize: 70 * ratio,
    textAlignVertical: "center",
    fontFamily: "Helvetica Neue",
    letterSpacing: 3
  },
  btn: {
    color: "white",
    fontSize: 18
  }
});

export default withNavigation(Recorder);
