import React, { Component } from "react";

import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  DatePickerIOS
} from "react-native";

import PropTypes from "prop-types";
import EStyleSheet from "react-native-extended-stylesheet";

export default class Datepicker extends Component {
  constructor(props) {
    super(props);
    this.state = { chosenDate: new Date() };
    this.setDate = this.setDate.bind(this);
  }
  componentWillMount(props) {
    console.log(props);
  }

  setDate(newDate) {
    date = new Date(newDate);

    this.setState({ chosenDate: newDate });
    this.props.getDate(date);
  }

  render() {
    return (
      <DatePickerIOS
        date={this.props.hour}
        mode="time"
        onDateChange={this.setDate}
      />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: "center",
    alignItems: "center",
    flex: 1,
    backgroundColor: "#fff"
  },

  labelContainer: {
    width: "100%",
    height: 60,
    backgroundColor: "#716d6d"
  }
});
