import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  View,
  FlatList,
  TouchableOpacity,
  Text,
  Vibration,
  Image,
  DatePickerIOS
} from "react-native";
import AudioRecorderPlayer from "react-native-audio-recorder-player";
import LinearGradient from "react-native-linear-gradient";
import * as Animatable from "react-native-animatable";
import { GlobalStyles } from "../../resources/style/style";
import HeaderAlarm from "../UtilsComponents/Seccions/headerAlarm";
import AlarmEdit from "../UtilsComponents/AlarmEdit/AlarmEdit.ios";
import TabMenu from "../UtilsComponents/Seccions/TabMenu";
import ServiceAlarm from "../../services/AlarmService";
import Reproductor from "../UtilsComponents/Seccions/Reproductor";

import Search from "../UtilsComponents/Seccions/search";
import i18n from "../../AppGlobalConfig/Localization/i18n";
import TimerMixin from "react-timer-mixin";
import SwitchToggle from "react-native-switch-toggle";
import BtnDay from "../UtilsComponents/btnDay/btnDay";
import Promo from "../UtilsComponents/Seccions/promo";
import Datepicker from "../UtilsComponents/datepicker/datepicker";

export default class AddAlarm extends Component {
  constructor(props) {
    super(props);
    this.config = {};
    this.state = {
      hour: new Date(),
      alarm: [],
      active: true,
      sunday: false,
      monday: false,
      thursday: false,
      wednesday: false,
      tuesday: false,
      friday: false,
      saturday: false,
      snooze: false
    };
  }
  saveAlarm = async () => {
    let alarm = {
      hour: this.state.hour.toString(),
      active: true,
      sunday: this.state.sunday,
      monday: this.state.monday,
      tuesday: this.state.thursday,
      wednesday: this.state.wednesday,
      thursday: this.state.tuesday,
      friday: this.state.friday,
      saturday: this.state.saturday,
      textHour: this.state.textHour,
      snooze: this.state.snooze
    };
    console.log(alarm);

    if (this.config.typeFunction == "edit") {
      let configSearch = {
        id: this.config.id,
        idAlarm: this.config.idAlarm,
        type: this.config.type
      };
      data = { configSearch, alarm };
      let data = await ServiceAlarm.editAlarm(data);
    }
    if (this.config.typeFunction == "new") {
      let configSearch = {
        id: this.config.id,
        type: this.config.type
      };
      data = { configSearch, alarm };

      let data = await ServiceAlarm.addAlarm(data);
    }
  };
  componentDidMount() {
    const { navigation } = this.props;

    const type = navigation.getParam("type");
    const typeFunction = navigation.getParam("typeFunction");
    this.setState({
      typeData:
        type == "AUDIO" || type == "audios" ? "audios" : "defaultAudios",
      idAudio: navigation.getParam("id")
    });
    if (typeFunction == "edit") {
      let data = {
        id: navigation.getParam("id"),
        idAlarm: navigation.getParam("idAudio"),
        type: type
      };
      this.config = data;
      this.config.typeFunction = typeFunction;
      this.getAlarm(data);
    }

    if (typeFunction == "new") {
      let data = {
        id: navigation.getParam("id"),
        type: type
      };
      this.config = data;
      this.config.typeFunction = typeFunction;
      this.config.typeFunction = typeFunction;

      this.formatTextDate();
    }
  }
  formatTextDate() {
    let date = new Date();
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? "PM" : "AM";
    hours = hours % 12;
    hours = hours ? hours : 12;
    var strTime = hours + ":" + minutes + " " + ampm;

    this.setState({ textHour: strTime });
  }

  getAlarm = async info => {
    let data = await ServiceAlarm.getAlarm(info);
    console.log(data);
    this.setState({
      hour: new Date(data.hour),
      textHour: data.textHour,
      alarm: data,
      sunday: data.sunday,
      monday: data.monday,
      thursday: data.thursday,
      wednesday: data.wednesday,
      tuesday: data.tuesday,
      friday: data.friday,
      saturday: data.saturday,
      snooze: data.snooze
    });
  };

  getDateCurrent = data => {
    this.setState({ chosenDate: new Date(data).toString() });
    this.setState({ hour: data });
    date = new Date(data);
    var hours = date.getHours();
    var minutes = date.getMinutes();
    if (minutes < 10) {
      minutes = "0" + minutes;
    }
    var ampm = hours >= 12 ? "PM" : "AM";
    hours = hours % 12;
    hours = hours ? hours : 12;
    var strTime = hours + ":" + minutes + " " + ampm;

    this.setState({ textHour: strTime });
  };

  async chooseDate() {
    try {
      const { action, year, month, day } = await DatePickerAndroid.open({
        mode: "spinner",
        // Use `new Date()` for current date.
        // May 25 2020. Month 0 is January.
        date: new Date()
      });

      if (action !== DatePickerAndroid.dismissedAction) {
        this.setState({ hour: new Date(year, month, day) });
      }
    } catch ({ code, message }) {
      console.warn("Cannot open date picker", message);
    }
  }
  changeState = data => {
    if (data.day == "sunday") {
      this.setState({ sunday: data.active });
    }
    if (data.day == "monday") {
      this.setState({ monday: data.active });
    }
    if (data.day == "thursday") {
      this.setState({ thursday: data.active });
    }
    if (data.day == "wednesday") {
      this.setState({ wednesday: data.active });
    }
    if (data.day == "tuesday") {
      this.setState({ tuesday: data.active });
    }
    if (data.day == "friday") {
      this.setState({ friday: data.active });
    }
    if (data.day == "saturday") {
      this.setState({ saturday: data.active });
    }
  };
  render() {
    return (
      <View style={styles.container}>
        <Animatable.View style={[styles.container]}>
          <LinearGradient
            start={{ x: 0.0, y: 0.25 }}
            end={{ x: 1.5, y: 0.5 }}
            colors={["#5ccbfa", "#33a2f2"]}
            style={styles.linearGradient}
          >
            <HeaderAlarm
              title={i18n.t("globals.add-alarm")}
              type="addAlarm"
              save={this.saveAlarm}
              typeData={this.state.typeData}
              idAudio={this.state.idAudio}
            />
          </LinearGradient>
          <View style={styles.containerDatapicker}>
            <Datepicker
              getDate={this.getDateCurrent}
              hour={new Date(this.state.hour)}
            />
          </View>

          <View style={[styles.containerText, { borderTopWidth: 1 }]}>
            <Text style={GlobalStyles.regularText}>Repeat </Text>
          </View>

          <View style={styles.containerBtns}>
            <View style={styles.containerBtn}>
              <BtnDay
                day={"S"}
                active={this.state.sunday}
                updateState={this.changeState}
                dayCode={"sunday"}
              />
            </View>
            <View style={styles.containerBtn}>
              <BtnDay
                day={"M"}
                active={this.state.monday}
                updateState={this.changeState}
                dayCode={"monday"}
              />
            </View>
            <View style={styles.containerBtn}>
              <BtnDay
                day={"T"}
                active={this.state.thursday}
                updateState={this.changeState}
                dayCode={"thursday"}
              />
            </View>
            <View style={styles.containerBtn}>
              <BtnDay
                day={"W"}
                active={this.state.wednesday}
                updateState={this.changeState}
                dayCode={"wednesday"}
              />
            </View>
            <View style={styles.containerBtn}>
              <BtnDay
                day={"F"}
                active={this.state.friday}
                updateState={this.changeState}
                dayCode={"friday"}
              />
            </View>
            <View style={styles.containerBtn}>
              <BtnDay
                day={"S"}
                active={this.state.saturday}
                updateState={this.changeState}
                dayCode={"saturday"}
              />
            </View>
          </View>
          <View style={styles.containerText}>
            <View style={styles.containerDouble}>
              <Text style={GlobalStyles.regularText}>Sound </Text>
            </View>
            <View style={[styles.containerDouble, styles.right]}>
              <Text style={[GlobalStyles.regularText, { color: "#cad0d4" }]}>
                Radar{" "}
              </Text>
            </View>
          </View>
          <View style={styles.containerText}>
            <View style={styles.containerDouble}>
              <Text style={GlobalStyles.regularText}>Snooze </Text>
            </View>
            <View style={[styles.containerDouble, styles.right]}>
              <SwitchToggle
                containerStyle={{
                  width: 36,
                  height: 21,
                  borderRadius: 25,
                  padding: 1
                }}
                circleStyle={{
                  width: 19,
                  height: 17,
                  borderRadius: 30,
                  backgroundColor: "#33a2f2" // rgb(102,134,205)
                }}
                switchOn={this.state.snooze}
                onPress={() => {
                  this.setState({
                    snooze: this.state.snooze == true ? false : true
                  });
                  // console.log(this.state.snooze);
                }}
                circleColorOff="#fff"
                circleColorOn="#fff"
                backgroundColorOff="gray"
                backgroundColorOn="#33a2f2"
                duration={500}
              />
            </View>
          </View>
        </Animatable.View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff"
  },
  containerDatapicker: {
    width: "100%",
    paddingTop: 10
  },
  right: {
    alignItems: "flex-end",
    justifyContent: "flex-end",
    paddingRight: 25
  },
  containerText: {
    borderWidth: 1,
    borderLeftWidth: 0,
    borderRightWidth: 0,
    borderTopWidth: 0,
    paddingTop: 9,
    paddingBottom: 9,
    paddingLeft: 27,
    borderColor: "#cad0d4",
    flexDirection: "row"
  },
  containerBtns: {
    borderWidth: 1,
    borderLeftWidth: 0,
    borderRightWidth: 0,
    borderTopWidth: 0,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 27,
    borderColor: "#cad0d4",
    flexDirection: "row"
  },
  LinearGradient: {
    borderRadius: 50,
    width: 40,
    height: 40,
    alignItems: "center",
    justifyContent: "center"
  },
  containerBtn: {
    width: "16.6%",
    borderColor: "#cad0d4",
    borderWidth: 0,
    paddingLeft: 2,
    paddingRight: 2
  },
  containerDouble: {
    width: "50%"
  },
  btn: {
    backgroundColor: "#e0e0e0",
    borderRadius: 50,
    width: 40,
    height: 40,
    alignItems: "center",
    justifyContent: "center"
  },
  color: {
    color: "#828282"
  }
});
