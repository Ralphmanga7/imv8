import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Button,
  TouchableOpacity,
  StatusBar,
  TextInput,
  ScrollView,
} from "react-native";
import PropTypes from "prop-types";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

export default class InputText extends Component {
  constructor() {
    super();
    this.state = {
      inputRef: null,
      value: "",
      isCorrect: false,
      required: true,
      error : ""
    };
  }

  checkIfIsCorrect = () => {
    if(this.state.value.length == 0){
      return;
    }

    let pattern = /^[a-zA-Z\s]*$/;

    if (this.state.value !== "" && pattern.test(this.state.value) ) {

        this.state.isCorrect = true;
        delete this.state.error;
    
    }else{
    
      if(this.state.value == ""){
          this.state.error = i18n.t("register.email_field_required") ;
          this.state.isCorrect = false;
      }else{
        this.state.error = i18n.t("register.email_error");
        this.state.isCorrect = false;
      }
    
    }

    this.setState(this.state);
    this.props.update(this.state.value);
  };

  clearInput = () => {
    this.state.inputRef._root.setNativeProps({ text: "" }); // eslint-disable-line
    this.setState({ isCorrect: 0, value: "" });
  };
  updateText = value => {
    this.state.value = value;
  };
  render() {
    return (
      <TextInput
        style={[styles.input,this.props.customStyle]}
        blurOnSubmit={false}
        returnKeyType={this.props.returnKeyType}
        ref={ref => {
          this.state.inputRef = ref;
        }}
        autoCapitalize="none"
        keyboardType={this.props.keyboardType}
        placeholder={this.props.placeholder}
        onSubmitEditing={this.props.changeFocus}
        onChangeText={this.updateText}
        onEndEditing={this.checkIfIsCorrect}
        placeholderTextColor={this.props.placeholderTextColor}
      />
    );
  }
}

const styles = StyleSheet.create({
  input: {
    color: "white",
    height: hp('8%'), // 70% of height device screen
    width: wp('95%'), 
    borderRadius: wp('6%'),
    borderWidth: 2,
    borderColor: "#fff",
    paddingLeft: 20,
    fontSize:16,
  }
});

InputText.propTypes = {
  update: PropTypes.func.isRequired,
  changeFocus: PropTypes.func.isRequired,
  special: PropTypes.bool,
  placeholder: PropTypes.string
};
InputText.defaultProps = {
  special: false,
  placeholder: "",
  customStyle: {},
  placeholderTextColor: 'gray',
  keyboardType: 'default',
  returnKeyType: 'next',
};
