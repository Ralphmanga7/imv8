import { AsyncStorage } from "react-native";
import {PushNotificationIOS} from 'react-native';
import PushNotification from 'react-native-push-notification';

const configure = {
  // (required) Called when a remote or local notification is opened or received
  onNotification: function(notification) {
    console.log( 'NOTIFICATION:', notification );
    
    alert(notification.message)
    // process the notification

    // required on iOS only (see fetchCompletionHandler docs: https://facebook.github.io/react-native/docs/pushnotificationios.html)
    notification.finish(PushNotificationIOS.FetchResult.NoData);
  //  notification.finish('backgroundFetchResultNewData');
  },

  // ANDROID ONLY: GCM or FCM Sender ID (product_number) (optional - not required for local notifications, but is need to receive remote push notifications)
  senderID: "YOUR GCM (OR FCM) SENDER ID",

  // IOS ONLY (optional): default: all - Permissions to register.
  permissions: {
    alert: true,
    badge: true,
    sound: true
  },

  // Should the initial notification be popped automatically
  // default: true
  popInitialNotification: true,

  /**
   * (optional) default: true
   * - Specified if permissions (ios) and token (android and ios) will requested or not,
   * - if not, you must call PushNotificationsHandler.requestPermissions() later
   */
  requestPermissions: true,
};



exports.getAlarms = async function(data) {
  PushNotification.configure(configure);
  try {
    let list = await AsyncStorage.getItem(data.type);
    let storedAudios = JSON.parse(list);
    let result = storedAudios.find(function(audio) {
      return audio.id == data.id;
    });

    return result;
  } catch {}
};
exports.getAlarm = async function(data) {

  try {
    let list = await AsyncStorage.getItem(data.type);
    let storedAudios = JSON.parse(list);
    let audio = storedAudios.find(function(audio) {
      return audio.id == data.id;
    });
    let alarm = audio.alarms.find(function(alarm) {
      return alarm.id == data.idAlarm;
    });
    return alarm;
  } catch {}
};

exports.changeStateAlarm = async function(data) {

  try {
    let storedAudios = await AsyncStorage.getItem(data.type);
    storedAudios = JSON.parse(storedAudios);
    if (storedAudios) {
      for (let i = 0; storedAudios.length > i; i++) {
        if (storedAudios[i].id == data.id) {
          for (let j = 0; storedAudios[i].alarms.length > j; j++) {
            if (storedAudios[i].alarms[j].id == data.idAlarm) {
              storedAudios[i].alarms[j].active = data.active;
              await AsyncStorage.setItem(
                data.type,
                JSON.stringify(storedAudios)
              );
              return storedAudios[i].alarms[j].active;
            }
          }
        }
      }
    } else {
      return !data.active;
    }

    return data.newTitle;
  } catch (e) {
    alert(i18n.t("globals.error") + ":" + e);
    return false;
  }
};
exports.deleteAlarm = async function(data) {
  try {
    let storedAudios = await AsyncStorage.getItem(data.type);
    storedAudios = JSON.parse(storedAudios);
    if (storedAudios) {
      for (let i = 0; storedAudios.length > i; i++) {
        if (storedAudios[i].id == data.id) {
          for (let j = 0; storedAudios[i].alarms.length > j; j++) {
            if (storedAudios[i].alarms[j].id == data.idAlarm) {
              storedAudios[i].alarms.splice(j, 1);
              await AsyncStorage.setItem(
                data.type,
                JSON.stringify(storedAudios)
              );
            }
          }
        }
      }
      let result = storedAudios.find(function(audio) {
        return audio.id == data.id;
      });

      return result;
    } else {
      return !data.active;
    }

    return data.newTitle;
  } catch (e) {
    alert(i18n.t("globals.error") + ":" + e);
    return false;
  }
};

exports.deleteAll = async function(data) {
  try {
    let list = await AsyncStorage.getItem(data.type);
    let storedAudios = JSON.parse(list);
    if (storedAudios) {
      for (let i = 0; storedAudios.length > i; i++) {
        if (storedAudios[i].id == data.id) {
          storedAudios[i].alarms = [];
          await AsyncStorage.setItem(data.type, JSON.stringify(storedAudios));
        }
      }
    }
    let result = storedAudios.find(function(audio) {
      return audio.id == data.id;
    });
    return result;
  } catch {
    alert(i18n.t("globals.error") + ":" + e);
    return false;
  }
};

exports.editAlarm = async function(data) {
  try {
    configSearch = data.configSearch;
    alarm = data.alarm;

    let list = await AsyncStorage.getItem(configSearch.type);

    let storedAudios = JSON.parse(list);

    if (storedAudios) {
      for (let i = 0; storedAudios.length > i; i++) {
        if (storedAudios[i].id == configSearch.id) {
          for (let j = 0; storedAudios[i].alarms.length > j; j++) {
            if (storedAudios[i].alarms[j].id == configSearch.idAlarm) {
              alarm.id = storedAudios[i].alarms[j].id;
              storedAudios[i].alarms[j] = alarm;

              await AsyncStorage.setItem(
                configSearch.type,
                JSON.stringify(storedAudios)
              );
            }
          }
        }
      }
    }
  } catch {
    alert(i18n.t("globals.error") + ":" + e);
    return false;
  }
};

function createAlarm(data, audio) {

  PushNotification.localNotificationSchedule({
    userInfo: { id: data.id },
    repeatType: 'day',
    message: "Reproduce tu audio "+audio["title"], // (required)
    date: new Date(data.hour) // in 60 secs
  });

  console.warn(data);
  console.warn(audio);
  
  let days = ["monday","tuesday","wednesday","thursday","friday","saturday","sunday"];

  days.forEach((day, index)=>{
    if(data[day])
      createAlarmForDay(index)
  });
}

function deleteAlarm(id){

}

function createAlarmForDay(dayIndex){
  console.warn(dayIndex)
}

exports.addAlarm = async function(data) {
  try {
    console.log(data);
    configSearch = data.configSearch;
    alarm = data.alarm;
    console.log("servicio", data);
    let list = await AsyncStorage.getItem(configSearch.type);
    let storedAudios = JSON.parse(list);
    console.log("mi lista de audios", storedAudios);
    if (storedAudios) {
      for (let i = 0; storedAudios.length > i; i++) {
        console.log("recorro mi lista de audios", storedAudios[i].id);
        if (storedAudios[i].id == configSearch.id) {
          alarm.id = generateId(storedAudios[i].alarms);
          storedAudios[i].alarms.push(alarm);
          await AsyncStorage.setItem(
            configSearch.type,
            JSON.stringify(storedAudios)
          );
          createAlarm(alarm, storedAudios[i]);
        }
      }
    }
  } catch(e) {
    alert(e);
    return false;
  }
};
function generateId(list) {
  let flag = 0;
  while (flag == 0) {
    let id = Math.random()
      .toString(36)
      .substr(2, 9);
    let response = list.find(function(object) {
      return object.id == id;
    });
    if (response == undefined) {
      flag = 1;
      return id;
    }
  }
}
