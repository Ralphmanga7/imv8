import React, { Component } from "react";
import {
  Platform,
  Dimensions,
  StyleSheet,
  Text,
  View,
  Button,
  TouchableOpacity,
  StatusBar,
  TextInput,
  ScrollView,
  PixelRatio

} from "react-native";
import EStyleSheet from "react-native-extended-stylesheet";
import PropTypes from "prop-types";
import i18n from "../../../AppGlobalConfig/Localization/i18n";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";


export default class Name extends Component {
  constructor() {
    super();
    this.state = {
      inputRef: null,
      value: "",
      isCorrect: false,
      required: true,
      error: ""
    };


      let _pixelRatio = PixelRatio.get();
  }

  errorTemplate = ()=> {
      if(this.state.error){
        return (<Text style={{ textAlign: 'center', color:'red', fontSize:10  * this._pixelRatio }}>{this.state.error}</Text>);
      }else{
        return null;
      }
   }


  checkIfIsCorrect = () => {
    let pattern = /^[a-zA-Z\s]*$/;

    if (this.state.value !== "" && pattern.test(this.state.value)) {
      this.state.isCorrect = true;
      delete this.state.error;
    } else {
      if (this.state.value == "") {
        this.state.error = i18n.t("register.invalid_name_field");
        this.state.isCorrect = false;
      } else {
        this.state.error = i18n.t("register.invalid_name_field");
        this.state.isCorrect = false;
      }
    }

    this.setState(this.state);
    this.props.update(this.state.value);
  };

  clearInput = () => {
    this.state.inputRef._root.setNativeProps({ text: "" }); // eslint-disable-line
    this.setState({ isCorrect: 0, value: "" });
  };
  updateText = value => {
    this.state.value = value;
  };
  render() {
    return (
      <View style={{ width: '100%', padding:4}}>
      <TextInput
        style={[styles.input, StyleSheet.create(this.props.customStyle) ]}
        blurOnSubmit={false}
        returnKeyType={this.props.returnKeyType}
        ref={ref => {
          this.state.inputRef = ref;
        }}
        autoCapitalize="words"
        keyboardType={this.props.keyboardType}
        placeholder={this.props.placeholder}
        onSubmitEditing={this.props.changeFocus}
        onChangeText={this.updateText}
        onEndEditing={this.checkIfIsCorrect}
        placeholderTextColor={this.props.placeholderTextColor}
      />
      {(this.state.error != "" ) ? this.errorTemplate() : null}
      </View>
    );
  }
}

isIphoneXorAbove = (s, n)=> {
  const dimen = Dimensions.get('window');
  
  if((
    Platform.OS === 'ios' &&
    !Platform.isPad &&
    !Platform.isTVOS &&
    ((dimen.height === 812 || dimen.width === 812) || (dimen.height === 896 || dimen.width === 896))
  )){

    return s;

  }

  return n;
}

const styles = StyleSheet.create({
  input: {
    color: "white",
    height: isIphoneXorAbove(hp('6%'), hp('8%')), // 70% of height device screen
    width: wp('95%'), 
    borderRadius: wp('6%'),
    borderWidth: 2,
    borderColor: "#fff",
    paddingLeft: 20,
    fontSize: 16
  }
});

Name.propTypes = {
  update: PropTypes.func.isRequired,
  changeFocus: PropTypes.func.isRequired,
  special: PropTypes.bool,
  placeholder: PropTypes.string
};
Name.defaultProps = {
  special: false,
  placeholder: "",
  customStyle: {},
  placeholderTextColor: "gray",
  keyboardType: "default",
  returnKeyType: "next"
};
