import PlayListDefaultJson from "../api/audiosDefault.json";
import { AsyncStorage } from "react-native";
import i18n from "../AppGlobalConfig/Localization/i18n";
import RNFetchBlob from "rn-fetch-blob";

exports.getPLayList = function() {
  let orderlist = PlayList.sort(function(a, b) {
    return a.date - b.date;
  });

  return orderlist;
};

exports.add = function(data) {
  (data.title = "This is audio test"), (data.size = "10 am");
  data.date = "January 2th 2018 at 11:00 a.m";
  PlayList.push(data);
};

async function createId(list) {
  try {
    let flag = 0;
    while (flag == 0) {
      let id = Math.random()
        .toString(36)
        .substr(2, 9);
      let response = list.find(function(object) {
        return object.id == id;
      });
      if (response == undefined) {
        flag = 1;
        return id;
      }
    }
  } catch (e) {
    console.warn(i18n.t("globals.error") + ":" + e);
    return false;
  }
}

exports.save = async function(data) {
  try {
    //await AsyncStorage.removeItem("audios");

    let storedAudios = await AsyncStorage.getItem("audios");
    storedAudios = JSON.parse(storedAudios);
    data.id = await createId(storedAudios);
    data.alarms = [];
    if (storedAudios) {
      storedAudios.push(data);
      await AsyncStorage.setItem("audios", JSON.stringify(storedAudios));
    } else {
      let audios = [];
      audios.push(data);

      await AsyncStorage.setItem("audios", JSON.stringify(audios));
    }

    return data;
  } catch (e) {
    console.warn(i18n.t("globals.error") + ":" + e);
    return false;
  }
};

exports.setNameDefaultAffirmation = async function(data) {
  try {
    let storedAudios = await AsyncStorage.getItem("defaultAudios");
    storedAudios = JSON.parse(storedAudios);

    if (storedAudios) {
      console.log(storedAudios);
      for (let i = 0; storedAudios.length > i; i++) {
        console.log(storedAudios[i].id + "= " + data.id);
        if (storedAudios[i].id == data.id) {
          storedAudios[i].title = data.newTitle;
        }
      }
      await AsyncStorage.setItem("defaultAudios", JSON.stringify(storedAudios));
    } else {
      let audios = [];
      audios.push(data);
      console.log(data);
      await AsyncStorage.setItem("defaultAudios", JSON.stringify(audios));
    }

    return data.newTitle;
  } catch (e) {
    console.warn(i18n.t("globals.error") + ":" + e);
    return false;
  }
};
exports.setName = async function(data) {
  try {
    // await AsyncStorage.removeItem("audios");
    data.id = await createId();
    console.warn(data);
    let storedAudios = await AsyncStorage.getItem("audios");
    storedAudios = JSON.parse(storedAudios);

    if (storedAudios) {
      console.log(storedAudios);
      for (let i = 0; storedAudios.length > i; i++) {
        console.log(storedAudios[i].date + "= " + data.date);
        if (storedAudios[i].date == data.date) {
          storedAudios[i].title = data.newTitle;
        }
      }
      await AsyncStorage.setItem("audios", JSON.stringify(storedAudios));
    } else {
      let audios = [];
      audios.push(data);
      console.log(data);
      await AsyncStorage.setItem("audios", JSON.stringify(audios));
    }

    return data.newTitle;
  } catch (e) {
    console.warn(i18n.t("globals.error") + ":" + e);
    return false;
  }
};

exports.get = async function(userId) {
  try {
    let list = await AsyncStorage.getItem("audios");

    console.warn("list", list);
    
    let playList = JSON.parse(list).filter((a)=>{
        if(a.owner){
          return (a.owner == userId);
        }
        return false;
    });

    return playList.reverse();
  } catch (e) {
    return [];
  }
};

exports.getDefault = async function() {
  try {
    //await AsyncStorage.removeItem("defaultAudios");
    let storedAudios = await AsyncStorage.getItem("defaultAudios");
    storedAudios = JSON.parse(storedAudios);

    if (!storedAudios || storedAudios == null) {
      await AsyncStorage.setItem(
        "defaultAudios",
        JSON.stringify(PlayListDefaultJson.defaultAudio)
      );
      let storedAudios = await AsyncStorage.getItem("defaultAudios");
      return JSON.parse(storedAudios);
    } else {
      return storedAudios;
    }
  } catch (e) {
    return [];
  }
};
exports.getOrderName = async function(typeSearch) {
  try {
    let list = await AsyncStorage.getItem(typeSearch);
    let playList = JSON.parse(list);

    orderlist = playList.sort(function(a, b) {
      if (a.title > b.title) {
        return 1;
      }
      if (a.title < b.title) {
        return -1;
      }
      // a must be equal to b
      return 0;
    });
    console.log(orderlist);
    return orderlist;
  } catch {}
};
exports.getdOrderDate = async function() {
  try {
    let list = await AsyncStorage.getItem("audios");
    let playList = JSON.parse(list);
    let orderlist = playList.sort(function(a, b) {
      return a.date - b.date;
    });
  } catch {}
};
exports.getOrderLenght = async function(typeSearch) {
  try {
    let list = await AsyncStorage.getItem(typeSearch);
    let playList = JSON.parse(list);
    let orderlist = playList.sort(function(a, b) {
      time1 = a.duration.split(":");
      time1 =
        time1[2] == undefined
          ? time1[0].trim() + time1[1].trim()
          : time1[0].trim() + time1[1].trim() + time1[2].trim();
      time1 = parseInt(time1);

      time2 = b.duration.split(":");
      time2 =
        time2[2] == undefined
          ? time2[0].trim() + time2[1].trim()
          : time2[0].trim() + time2[1].trim() + time2[2].trim();
      time2 = parseInt(time2);

      return time1 - time2;
    });

    return orderlist.reverse();
  } catch {}
};
exports.getOrderSize = async function(typeSearch) {
  try {
    let list = await AsyncStorage.getItem(typeSearch);
    let playList = JSON.parse(list);
    let orderlist = playList.sort(function(a, b) {
      time1 = a.size.split("MB");
      time1 = time1[0].split(".");
      time1 = time1[0] + time1[1];
      time1 = parseInt(time1);
      time2 = b.size.split("MB");
      time2 = time2[0].split(".");
      time2 = time2[0] + time2[1];
      time2 = parseInt(time2);

      return time1 - time2;
    });
    return orderlist.reverse();
  } catch {}
};

exports.getOldestSize = async function() {
  try {
    let list = await AsyncStorage.getItem("audios");
    let playList = JSON.parse(list);
    let orderlist = playList.sort(function(a, b) {
      return a.date - b.date;
    });
    return orderlist;
  } catch {}
};

exports.getNewestSize = async function() {
  try {
    let list = await AsyncStorage.getItem("audios");
    let playList = JSON.parse(list);
    let orderlist = playList.sort(function(a, b) {
      return a.date - b.date;
    });
    return orderlist.reverse();
  } catch {}
};

exports.getByCriteria = async function(value) {
  let list = await AsyncStorage.getItem("audios");
  let playList = JSON.parse(list);

  playList = playList.filter(audio => {
    return audio.title.toLowerCase().indexOf(value.toLowerCase()) !== -1;
  });

  return playList;
};

exports.getByCriteriaDefault = async function(value, typeSearch) {
  let playList = await AsyncStorage.getItem(typeSearch);

  try {
    await AsyncStorage.removeItem(typeSearch);
    let storedAudios = await AsyncStorage.getItem(typeSearch);
    storedAudios = JSON.parse(storedAudios);

    if (!storedAudios || storedAudios == null) {
      await AsyncStorage.setItem(
        typeSearch,
        JSON.stringify(PlayListDefaultJson.defaultAudio)
      );
      let storedAudios = await AsyncStorage.getItem(typeSearch);
      return JSON.parse(storedAudios).filter(audio => {
        return audio.title.toLowerCase().indexOf(value.toLowerCase()) !== -1;
      });
    } else {
      return storedAudios.filter(audio => {
        return audio.title.toLowerCase().indexOf(value.toLowerCase()) !== -1;
      });
    }
  } catch (e) {
    return [];
  }
};

exports.getAlarms = async function(data) {
  try {
    let list = await AsyncStorage.getItem("defaultAudios");
    let storedAudios = JSON.parse(list);
    let result = storedAudios.find(function(audio) {
      return audio.id == data.id;
    });

    return result;
  } catch {}
};
exports.delete = async function(id) {
  try {
    let storedAudios = await AsyncStorage.getItem("audios");
    storedAudios = JSON.parse(storedAudios);

    if (storedAudios) {
      for (let i = 0; storedAudios.length > i; i++) {
        if (storedAudios[i].id == id) {
          storedAudios.splice(i, 1);
          await AsyncStorage.setItem("audios", JSON.stringify(storedAudios));
        }
      }
    }
    let resultList = await AsyncStorage.getItem("audios");
    return JSON.parse(resultList);
  } catch (e) {
    console.warn(i18n.t("globals.error") + ":" + e);
    return false;
  }
};
