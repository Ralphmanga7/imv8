import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  View,
  FlatList,
  TouchableOpacity,
  Text,
  Image
} from "react-native";

import LinearGradient from "react-native-linear-gradient";
import * as Animatable from "react-native-animatable";
import { GlobalStyles } from "../../resources/style/style";
import HeaderAlarm from "../UtilsComponents/Seccions/headerAlarm";
import Alarm from "../UtilsComponents/alarm/alarm";

import ServiceAlarm from "../../services/AlarmService";

import i18n from "../../AppGlobalConfig/Localization/i18n";
import TimerMixin from "react-timer-mixin";
import PlayList from "../../provider/playList";

export default class Alarms extends Component {
  constructor(props) {
    super(props);
    this.config = {};
    this.state = { alarmList: [] };
  }
  componentWillReceiveProps(props) {
    const { navigation } = props;

    this.config.type = navigation.getParam("type");
    this.config.id = navigation.getParam("id");

    let data = {
      type:
        this.config.type == "AUDIO" || this.config.type == "audios"
          ? "audios"
          : "defaultAudios",
      id: this.config.id
    };

    this.setState({
      typeData:
        this.config.type == "AUDIO" || this.config.type == "audios"
          ? "audios"
          : "defaultAudios",
      idAudio: this.config.id
    });

    this.loadAlarmList(data);
  }
  componentWillMount() {
    const { navigation } = this.props;
    this.config.type = navigation.getParam("type");
    this.config.id = navigation.getParam("id");
    this.props.navigation.addListener('willFocus', this.load)

    let data = {
      type:
        this.config.type == "AUDIO" || this.config.type == "audios"
          ? "audios"
          : "defaultAudios",
      id: this.config.id
    };

    this.setState({
      typeData:
        this.config.type == "AUDIO" || this.config.type == "audios"
          ? "audios"
          : "defaultAudios",
      idAudio: this.config.id
    });

    this.loadAlarmList(data);
    
  }

  load = () => {
    const { navigation } = this.props;
    this.config.type = navigation.getParam("type");
    this.config.id = navigation.getParam("id");
    let data = {
      type:
        this.config.type == "AUDIO" || this.config.type == "audios"
          ? "audios"
          : "defaultAudios",
      id: this.config.id
    };

    this.loadAlarmList(data)
  }

  loadAlarmList = async data => {
    let response = ServiceAlarm.getAlarms(data).then(data => {
      console.log("resultado", data);
      this.setState({
        alarmList: data.alarms
      });
    });
  };
  render() {
    return (
      <View style={styles.container}>
        <Animatable.View style={[styles.container]}>
          <LinearGradient
            start={{ x: 0.0, y: 0.25 }}
            end={{ x: 1.5, y: 0.5 }}
            colors={["#5ccbfa", "#33a2f2"]}
            style={styles.linearGradient}
          >
            <HeaderAlarm
              title={i18n.t("globals.manage-alarms")}
              type={"listAlarms"}
              typeData={this.state.typeData}
              idAudio={this.state.idAudio}
            />
          </LinearGradient>
          {this.state.alarmList.length == 0 ? (
            <View style={styles.empty}>
              <Image
                style={{ width: 52, height: 52 }}
                source={require("../../resources/img/clock.png")}
              />
              <Text style={{ color: "#bdbdbd" }}>No alarms</Text>
              <Text style={{ color: "#bdbdbd", paddingTop: 30 }}>
                The alarms you add will appear here
              </Text>
            </View>
          ) : null}
          <FlatList
            data={this.state.alarmList}
            extraData={this.state}
            onRefresh={() => {
              this.setState({ refreshing: true });
              TimerMixin.setTimeout(() => {
                this.setState({ refreshing: false });
              }, 2000);
            }}
            refreshing={this.state.refreshing ? true : false}
            renderItem={({ item }) => (
              <Alarm
                alarm={item}
                activate={this.state.activate}
                typeView={this.state.typeData}
                idAudio={this.state.idAudio}
                onPress={this.selectAudio}
                onLongPress={this.onShareAudio}
                keyExtractor={(item, index) => item.id}
              />
            )}
          />
          <View style={styles.ContainerBtn}>
            <TouchableOpacity
              style={[]}
              onPress={() => {
                this.props.navigation.navigate("AddAlarm", {
                  type: this.state.typeData,
                  id: this.state.idAudio,
                  typeFunction: "new"
                });
              }}
            >
              <Image
                style={{ width: 52, height: 52}}
                source={require("../../resources/img/more.png")}
              />
            </TouchableOpacity>
          </View>
        </Animatable.View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  ContainerBtn: {
    display: "flex",
    width: "100%",
    height: 100,
    bottom: 0,
    alignItems: "flex-end",
    justifyContent: "flex-end",
    paddingBottom: 40,
    paddingRight: 20
  },
  empty: {
    display: "flex",
    width: "100%",
    alignItems: "center",
    justifyContent: "center",
    paddingTop: 150
  }
});
//currentAudio={this.state.currentAudio}
