import React, { Component } from "react";
import { getStatusBarHeight } from "react-native-status-bar-height";
import {
  Platform,
  StatusBar,
  StyleSheet,
  View,
  Text,
  SafeAreaView,
  ActivityIndicator,
  AsyncStorage,
  ImageBackground
} from "react-native";
import { createStackNavigator, createSwitchNavigator } from "react-navigation";
//import RNLanguages from 'react-native-languages';
import i18n from "./AppGlobalConfig/Localization/i18n";
import LinearGradient from "react-native-linear-gradient";
// pages
import RecorderView from "./views/Recorder/RecorderView";
import MyAudioView from "./views/MyAudioList/MyAudioView";
import LoginSigninView from "./views/LoginSignin/LoginSigninView";
import Register from "./views/register/RegisterView";
import Record from "./views/record/recordView";
import Alarms from "./views/Alarms/Alarms";
import editAlarms from "./views/editAlarms/editAlarms";
import AddAlarm from "./views/addAlarm/addAlarm";
import DefaultAffirmation from "./views/DefaultAfirmations/DefaultAfirmations";
import Feedback from "./views/FeedBack/feedBack";
import Help from "./views/Help/help";
import TransitionConfig from "./views/UtilsComponents/transitions/react-navigation-transistioner";
import GlobalStyles from "./resources/style/style";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import * as Animatable from "react-native-animatable";

import OfflineNotice from "./views/UtilsComponents/network/OfflineNotice";

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";

class AuthLoadingScreen extends React.Component {
  constructor(props) {
    super(props);
    this._bootstrapAsync();
  }

  // Fetch the token from storage then navigate to our appropriate place
  _bootstrapAsync = async () => {
    //await AsyncStorage.removeItem("userToken");

    const userToken = await AsyncStorage.getItem("userToken");

    // This will switch to the App screen or Auth screen and this loading
    // screen will be unmounted and thrown away.
    this.props.navigation.navigate(userToken ? "App" : "Auth");
  };

  // Render any loading content that you like here
  render() {
    return (
      <ImageBackground
        style={[GlobalStyles.containerRow, GlobalStyles.backgroundColor]}
        source={require("./resources/img//background.png")}
      >
        <KeyboardAwareScrollView
          style={GlobalStyles.containerForm}
          scrollEnabled={false}
        >
          <Animatable.View
            animation="fadeInUp"
            delay={1200}
            duration={700}
            style={[GlobalStyles.container]}
          >
          <View style={styles.container_wrap}>
            <ActivityIndicator size={"small"} color="#FFFFFF" style={LoaderView.loading}/>
          </View>
        </Animatable.View>
        </KeyboardAwareScrollView>
      </ImageBackground>
    );
  }
}

const AppStack = createStackNavigator(
  {
    Home: {
      screen: RecorderView
    },
    MyAudioView: {
      screen: MyAudioView
      //screen: AddAlarm
    },
    Record: {
      screen: Record
    },
    DefaultAffirmation: {
      screen: DefaultAffirmation
    },
    Help: {
      screen: Help
    },
    Feedback: {
      screen: Feedback
    },
    Alarms: {
      screen: Alarms
    },
    EditAlarms: {
      screen: editAlarms
    },
    AddAlarm: {
      screen: AddAlarm
    }
  },
  {
    initialRouteName: "MyAudioView",
    headerMode: "none",
    initialRouteParams: { transition: 'fade' },
    transitionConfig: TransitionConfig,
    tabBarComponent: () => null
  }
);

const AuthStack = createStackNavigator(
  { 
    Auth: LoginSigninView,
    Register: Register,
  },
  {
    initialRouteName: "Auth",
    headerMode: "none",
    initialRouteParams: { transition: 'fade' },
    transitionConfig: TransitionConfig,
    tabBarComponent: () => null
  }
);

const StackNavigation = createSwitchNavigator(
  {
    AuthLoading: AuthLoadingScreen,
    App: AppStack,
    Auth: AuthStack
  },
  {
    initialRouteName: "AuthLoading",
    initialRouteParams: { transition: 'fade' },
    transitionConfig: TransitionConfig
  }
);

export default class App extends React.Component {
  constructor() {
    super();
  }

  /*
  componentWillMount() {
    RNLanguages.addEventListener('change', this._onLanguagesChange);
  }

  componentWillUnmount() {
    RNLanguages.removeEventListener('change', this._onLanguagesChange);
  }

  _onLanguagesChange = ({ language }) => {
    i18n.locale = language;
  };
  */

  componentDidMount() {
    if (Platform.OS === "android") {
      StatusBar.setTranslucent(true);
      StatusBar.setBackgroundColor("transparent");
    }
  }

  render() {
    return (
      <SafeAreaView style={styles.container} forceInset={{'top': 'never'}}>
        <OfflineNotice />
        <StatusBar backgroundColor="#33a2f2" barStyle="light-content" />
        <StackNavigation />
      </SafeAreaView>
    );
  }
}

const styles = {
  container: {
    flex: 1,
    width:"100%",
    backgroundColor:"#33a2f2",
    height:"100%",
    justifyContent: "center",
    paddingTop: getStatusBarHeight(false), // false to get height of android too.
    flexDirection: "column",
    backgroundColor: "black"
  },
  container_wrap: {
    flex: 1,
    width:"100%",
    height:"100%",
    justifyContent: "center",
    paddingTop: getStatusBarHeight(false), // false to get height of android too.
    flexDirection: "column",
    backgroundColor: "transparent"
  }
};

const LoaderView = StyleSheet.create({
        loading: {
            flex: 1,
            width:wp("100%"),
            height:hp("100%"),
            justifyContent: "center",
            flexDirection: "column",
            backgroundColor: "transparent"
        }
})
