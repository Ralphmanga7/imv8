import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  AsyncStorage
} from "react-native";

import UUIDGenerator from "react-native-uuid-generator";
import RNFetchBlob from "rn-fetch-blob";
import AudioRecorderPlayer from "react-native-audio-recorder-player";
import i18n from "../../../AppGlobalConfig/Localization/i18n";
import { withNavigation } from "react-navigation";
import PlayList from "../../../provider/playList";
import KeepAwake from 'react-native-keep-awake';

class RecorderTemp extends React.Component {
  constructor(props) {
    super(props);

    this.ios_ext = ".m4a";
    this.android_ext = ".mp4";
    this.path = Platform.select({
      ios: Date.now() + ".m4a",
      android: "sdcard/" + Date.now() + ".mp4" // should give extra dir name in android. Won't grant permission to the first level of dir.
    });

    this.state = {
      isRecording: false,
      bgColor: "#333",
      audio: "",
      session : false
    };

  }

  _bootstrapAsync = async () => {
    const user = await AsyncStorage.getItem("userToken");
    this.setState({ session : JSON.parse(user) });
  };

  componentDidMount() {
    this._bootstrapAsync();
    this.requestPermisions();
    this.audioRecorderPlayer = new AudioRecorderPlayer();

    UUIDGenerator.getRandomUUID().then(uuid => {
      this.state.path = {
        ios: uuid + this.ios_ext,
        android: "sdcard/" + uuid + this.android_ext // should give extra dir name in android. Won't grant permission to the first level of dir.
      };
    });
  }

  requestPermisions = async () => {
    try {
      if (Platform.OS === "android") {
        const micPermission = await checkPermission("microphone");
        console.log("micPermission", micPermission);
        if (micPermission !== "authorized") {
          const micRequest = await requestPermission("microphone");
          console.log("micRequest", micRequest);
          if (micRequest !== "authorized") {
            return;
          }
        }

        const storagePermission = await checkPermission("storage");

        if (storagePermission !== "authorized") {
          const storageRequest = await requestPermission("storage");
          if (storageRequest !== "authorized") {
            return;
          }
        }
      }
    } catch (e) {}
  };

  onStartRecord = async () => {
    KeepAwake.activate();

    console.log(this.audioRecorderPlayer.startRecorder, "audioRecorderPlayer");

    this.audioRecorderPlayer = new AudioRecorderPlayer();

    const result = await this.audioRecorderPlayer.startRecorder(this.path);

    this.audioRecorderPlayer.addRecordBackListener(e => {
      this.setState({
        recordSecs: e.current_position,
        recordTime: this.audioRecorderPlayer.mmssss(
          Math.floor(e.current_position)
        ),
        isRecording: true,
        bgColor: "red"
      });
      return;
    });
  };

  onStopRecord = async () => {
    const result = await this.audioRecorderPlayer.stopRecorder();
    this.audioRecorderPlayer.removeRecordBackListener();

    this.setState({
      recordSecs: 0,
      isRecording: false,
      playableAudio: true,
      bgColor: "#333",
      audio: result
    });

    KeepAwake.deactivate();
  };

  onStartPlay = async () => {
    console.log("onStartPlay");
    data = {};
    data.state = "onStartPlay";
    data.nameAudio = this.props.name || "no_name_" + this.path.split(".")[0];
    this.props.onPress(data);
    const msg = await this.audioRecorderPlayer.startPlayer(this.path);
    console.log(msg);
    this.audioRecorderPlayer.addPlayBackListener(e => {
      if (e.current_position === e.duration) {
        console.log("finished");
        this.audioRecorderPlayer.stopPlayer();
      }
      this.setState({
        currentPositionSec: e.current_position,
        currentDurationSec: e.duration,
        playTime: this.audioRecorderPlayer.mmssss(
          Math.floor(e.current_position)
        ),
        duration: this.audioRecorderPlayer.mmssss(Math.floor(e.duration)),
        isPlaying: true,
        isPaused: false
      });
      return;
    });
  };

  onPausePlay = async () => {
    await this.audioRecorderPlayer.pausePlayer();
    this.setState({
      isPlaying: false,
      isPaused: true
    });
  };

  onStopPlay = async () => {
    console.log("onStopPlay");
    this.audioRecorderPlayer.stopPlayer();
    this.audioRecorderPlayer.removePlayBackListener();
  };

  saveSound = async () => {
    const result = await this.onStopPlay();
    console.log("======== url audio ==============");

    console.log(this.state.audio);

    let audio = {};

    try {
      let stats = await RNFetchBlob.fs
        .stat(
          RNFetchBlob.fs.dirs.CacheDir.replace("Caches", "tmp").replace(
            "/Library",
            ""
          ) +
            "/" +
            this.path
        )
        .catch(e => {
          console.log(e);
        });

      audio = {
        owner : this.state.session.user._id || this.session.id,
        title: this.props.name || "no_name_" + this.path.split(".")[0],
        url: this.state.audio,
        duration: this.state.duration,
        date: new Date().getTime(),
        size: Math.round((stats.size / 1000000) * 100) / 100 + "MB", //converting filesize to MB
        sizeBytes: stats.size //preserving filesize in Bytes
      };
    } catch (e) {
      console.warn(e, "error with the filesize");

      audio = {
        owner : this.state.session.user._id || this.session.id,
        title: this.props.name || "no_name_" + this.path.split(".")[0],
        url: this.state.audio,
        duration: this.state.duration,
        date: new Date().getTime(),
        size: Math.round((76262662 / 1000000) * 100) / 100 + "MB", //converting filesize to MB
        sizeBytes: 76262662 //preserving filesize in Bytes
      };
    }

    console.log(audio);

    let new_recorded = await PlayList.save(audio);

    this.audioRecorderPlayer.removePlayBackListener();
    this.goToAudioList();
  };

  resetPlayable = () => {
    this.onStopPlay();
    this.audioRecorderPlayer.stopPlayer();
    this.audioRecorderPlayer.removePlayBackListener();

    this.setState({
      playableAudio: false,
      isRecording: false,
      recordTime: null,
      isPlaying: false,
      isPaused: false
    });
  };

  goToAudioList = () => {
    this.props.navigation.navigate("MyAudioView", { props: true });
  };

  render() {
    return (
      <View style={styles.container}>
        {!this.state.playableAudio ? (
          <View style={styles.containerRow}>
            <Text style={styles.chronometer}>
              {this.state.recordTime || "00:00"}
            </Text>
          </View>
        ) : null}
        {!this.state.playableAudio && !this.state.isRecording ? (
          <View style={styles.containerRow}>
            <TouchableOpacity
              style={styles.containerImage}
              onPress={this.onStartRecord}
            >
              <Image
                style={{ width: 56, height: 56 }}
                source={require("../../../resources/img/record.png")}
              />
            </TouchableOpacity>
          </View>
        ) : null}
        {this.state.isRecording ? (
          <View style={styles.containerRow}>
            <TouchableOpacity onPress={this.onStopRecord}>
              <Image
                style={{ width: 56, height: 56 }}
                source={require("../../../resources/img/paused.png")}
              />
            </TouchableOpacity>
          </View>
        ) : null}
        <View style={styles.containerRow}>
          {this.state.playableAudio ? (
            <View
              style={[
                styles.containerPart,
                !this.state.isPlaying ? styles.cancel : styles.containerRight
              ]}
            >
              <TouchableOpacity onPress={this.resetPlayable}>
                <Image
                  style={{ width: 23, height: 23 }}
                  source={require("../../../resources/img/cancel.png")}
                />
              </TouchableOpacity>
            </View>
          ) : null}
          {this.state.playableAudio && this.state.isPlaying ? (
            <View style={[styles.containerPart, styles.containerCenter]}>
              <TouchableOpacity onPress={this.saveSound}>
                <Image
                  style={{ width: 56, height: 56 }}
                  source={require("../../../resources/img/check.png")}
                />
              </TouchableOpacity>
            </View>
          ) : null}

          {this.state.playableAudio && !this.state.isPlaying ? (
            <View
              style={[
                !this.state.isPlaying
                  ? styles.containerMiddle
                  : styles.containerPart,
                !this.state.isPlaying
                  ? styles.positionLeft
                  : styles.containerCenter
              ]}
            >
              <TouchableOpacity onPress={this.onStartPlay}>
                <Image
                  style={{ width: 56, height: 56 }}
                  source={require("../../../resources/img/play.png")}
                />
              </TouchableOpacity>
            </View>
          ) : null}
          {this.state.playableAudio && this.state.isPlaying ? (
            <View style={[styles.containerPart, styles.pausedSimple]}>
              <TouchableOpacity onPress={this.onPausePlay}>
                <Image
                  style={{ width: 20, height: 23 }}
                  source={require("../../../resources/img/paused-simple.png")}
                />
              </TouchableOpacity>
            </View>
          ) : null}
        </View>
        <Image
          style={{ height: 2, width: 80 }}
          source={require("../../../resources/img/line.png")}
        />
      </View>
    );
  }
}

import { Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");
const calRatio = 16 * (width / height);

const screenWidth = width;
const screenHeight = height;
const ratio = (calRatio < 9 ? width / 9 : height / 18) / (360 / 9);

const styles = StyleSheet.create({
  container: {
    display: "flex",
    width: "100%",
    height: 130,
    bottom: 0,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#e0e0e0",
    borderTopColor: "#cad0d4",
    borderTopWidth: 1
  },
  containerMiddle: {
    width: "50%"
  },
  positionLeft: {
    alignItems: "flex-start",
    justifyContent: "flex-start"
  },
  containerPart: {
    width: "33%"
  },
  containerRight: {
    alignItems: "flex-end",
    justifyContent: "flex-end"
  },
  containerCenter: {
    alignItems: "center",
    justifyContent: "center"
  },
  pausedSimple: {
    alignItems: "flex-start",
    justifyContent: "flex-start"
  },
  cancel: {
    alignItems: "flex-end",
    justifyContent: "flex-end",
    paddingRight: 20
  },
  chronometer: {
    fontSize: 30,
    color: "#33a2f2"
  },
  containerRow: {
    flexDirection: "row",
    width: "100%",
    alignItems: "center",
    justifyContent: "center",
    paddingBottom: 5
  },

  containerImage: {
    width: "20%",
    height: 50,
    alignItems: "center",
    justifyContent: "center"
  }
});

export default withNavigation(RecorderTemp);
