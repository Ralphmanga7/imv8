import React, { Component } from "react";
import { StyleSheet, View, Image, TouchableOpacity, Text } from "react-native";
import { GlobalStyles } from "../../../resources/style/style";
import PropTypes from "prop-types";
import LinearGradient from "react-native-linear-gradient";

export default class BtnDay extends Component {
  constructor() {
    super();
  }

  changeState = () => {
    this.props.updateState({
      active: !this.props.active,
      day: this.props.dayCode
    });
  };

  render() {
    return (
      <View>
        {!this.props.active ? (
          <TouchableOpacity style={styles.btn} onPress={this.changeState}>
            <Text style={[GlobalStyles.regularText, styles.color]}>
              {this.props.day}
            </Text>
          </TouchableOpacity>
        ) : (
          <LinearGradient
            start={{ x: 0.0, y: 0.25 }}
            end={{ x: 1.5, y: 0.5 }}
            colors={["#5ccbfa", "#33a2f2"]}
            style={styles.LinearGradient}
          >
            <TouchableOpacity onPress={this.changeState}>
              <Text style={[GlobalStyles.regularText, styles.colorActive]}>
                {this.props.day}
              </Text>
            </TouchableOpacity>
          </LinearGradient>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  color: {
    color: "#828282"
  },
  colorActive: {
    color: "white"
  },
  btn: {
    backgroundColor: "#e0e0e0",
    borderRadius: 50,
    width: 40,
    height: 40,
    alignItems: "center",
    justifyContent: "center"
  },
  LinearGradient: {
    borderRadius: 50,
    width: 40,
    height: 40,
    alignItems: "center",
    justifyContent: "center"
  }
});
