import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  View,
  FlatList,
  TouchableHighlight,
  TouchableOpacity,
  Text,
  Image,
  Dimensions,
} from "react-native";
import LinearGradient from "react-native-linear-gradient";
import * as Animatable from "react-native-animatable";
import { GlobalStyles } from "../../resources/style/style";
import HeaderAlarm from "../UtilsComponents/Seccions/headerAlarm";
import i18n from "../../AppGlobalConfig/Localization/i18n";
import Btn from "../UtilsComponents/Buttons/Button";
import t from 'tcomb-form-native'; // 0.6.9
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

const Form = t.form.Form;

const User = t.struct({
  name: t.String,
  email: t.String,
  message: t.String
});

export default class Feedback extends Component {
  constructor(props) {
    super(props);
    this.config = {};
    this.state = { 
    };
  }
  componentWillReceiveProps(props) {
    const { navigation } = props;
  }
  componentWillMount() {
    const { navigation } = this.props;
  }

  isIphoneXorAbove = (s, n)=> {
    const dimen = Dimensions.get('window');
    
    if((
      Platform.OS === 'ios' &&
      !Platform.isPad &&
      !Platform.isTVOS &&
      ((dimen.height === 812 || dimen.width === 812) || (dimen.height === 896 || dimen.width === 896))
    )){

      return s;

    }

    return n;
  }

  load = () => {
    const { navigation } = this.props;
  }
  render() {
    return (
      <View style={styles.container}>
        <Animatable.View style={[styles.container]}>
          <LinearGradient
            start={{ x: 0.0, y: 0.25 }}
            end={{ x: 1.5, y: 0.5 }}
            colors={["#5ccbfa", "#33a2f2"]}
            style={styles.linearGradient}
          >
            <HeaderAlarm
              title={i18n.t("globals.feedback")}
              type={"listAlarms"}
              disableEdit={true}
            />
          </LinearGradient>
        </Animatable.View>
        <View style={GlobalStyles.containerRow}>
        <Form type={User} />
                  <Btn
                    onPress={this.Login}
                    text={i18n.t("feedback.send")}
                    customStyle={{
                      borderRadius: 30,
                      paddingTop: this.isIphoneXorAbove(hp('1%'), hp('2%')),
                      flexDirection: "row",
                      width: "80%",
                      marginTop: 20,
                      width: "100%",
                      color: "#828282",
                      backgroundColor: "#fff"
                    }}
                  />
          </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
});
//currentAudio={this.state.currentAudio}