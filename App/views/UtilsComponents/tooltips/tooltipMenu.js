import React, { Component } from "react";
import { GlobalStyles } from "../../../resources/style/style";
import i18n from "../../../AppGlobalConfig/Localization/i18n";
import PopoverTooltip from "react-native-popover-tooltip";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  ImageBackground,
  TextInput,
  AsyncStorage
} from "react-native";

import { withNavigation } from "react-navigation";

import {
  Menu,
  MenuOptions,
  MenuOption,
  MenuTrigger
} from "react-native-popup-menu";

import PropTypes from "prop-types";
class ToolTipMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      order: 1
    };
  }

  logout = async () => {
    await AsyncStorage.removeItem("userToken");
    this.props.navigation.navigate("AuthLoading");
  };

  render() {
    return (
      <PopoverTooltip
        ref="tooltip1"
        buttonComponent={
          <View>
            <Image
              style={{
                padding: 2,
                justifyContent: "center",
                alignItems: "center"
              }}
              resizeMode="center"
              resizeMethod="auto"
              source={require("../../../resources/img/tabmenu.png")}
            />
          </View>
        }
        items={[
          {
            label: "Item 1",
            onPress: () => {}
          },
          {
            label: "Item 2",
            onPress: () => {}
          }
        ]}
      />
    );
  }
}

export default withNavigation(ToolTipMenu);
