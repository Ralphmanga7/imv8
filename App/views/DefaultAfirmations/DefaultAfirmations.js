import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  View,
  FlatList,
  TouchableOpacity,
  Text,
  Vibration,
  Image,
  ProgressBarAndroid,
  ProgressViewIOS
} from "react-native";
import Slider from "react-native-slider";
import AudioRecorderPlayer from "react-native-audio-recorder-player";
import LinearGradient from "react-native-linear-gradient";
import * as Animatable from "react-native-animatable";
import { GlobalStyles } from "../../resources/style/style";
import Header from "../UtilsComponents/Seccions/header";
import DefaultAudio from "../UtilsComponents/DefaultAudio/DefaultAudio";
import TabMenu from "../UtilsComponents/Seccions/TabMenu";
import PlayList from "../../provider/playList";
import Reproductor from "../UtilsComponents/Seccions/Reproductor";
import Promo from "../UtilsComponents/Seccions/promo";
import Search from "../UtilsComponents/Seccions/search";
import i18n from "../../AppGlobalConfig/Localization/i18n";
import TimerMixin from "react-timer-mixin";
import RNFetchBlob from "rn-fetch-blob";
import Share from "react-native-share";
import { MAIN_BUNDLE } from "react-native-sound";

var Sound = require("react-native-sound");

export default class DefaultAffirmation extends Component {
  constructor(props) {
    super(props);

    this.state = {
      ActiveTab: 1,
      playList: [],
      link: "",
      playableAudio: false,
      isRecording: false,
      bgColor: "#333",
      audio: "",
      onTransmission: true,
      url: "",
      index: -1
    };
  }

  componentDidMount() {
    this.audioRecorderPlayer = new AudioRecorderPlayer();
    this.loadAudioList();
    this.song;
    this.timer = null;
  }

  componentWillUnmount() {
  }

  componentWillReceiveProps(props) {
    this.loadAudioList();
  }

  loadAudioList = async () => {
    //await AsyncStorage.removeItem("audios");
    await PlayList.getDefault().then(data => {
      //alert(data.length)
      console.log(data);
      this.setState({
        playList: data
      });
    });
  };

  onPausePlay = async () => {
    this.song.pause();
    this.setState({
      onTransmission: true
    });
  };

  onStopRecord = async () => {
    const result = await this.audioRecorderPlayer.stopRecorder();
    this.audioRecorderPlayer.removeRecordBackListener();
    this.setState({
      recordSecs: 0,
      isRecording: false,
      playableAudio: true,
      bgColor: "#333",
      audio: result,
      onTransmission: true
    });
    console.log(result);
  };

  onStartPlayWithoutUrl = async () => {
    console.warn("onStartPlayWithoutUrl");

    if(this.state.index <= -1){
        this.selectAudio(this.state.playList[0]);
      return;
    }
    
    this.song.play();
    this.setState({
      isPlaying: true,
      isPaused: false,
      onTransmission: false
      //currentPositionSec: e.current_position,
      //currentDurationSec: e.duration,
      //playTime: this.audioRecorderPlayer.mmssss(Math.floor(e.current_position)),
      //duration: this.audioRecorderPlayer.mmssss(Math.floor(e.duration))
    });
  };

  onStopPlay = async () => {
    console.log("onStopPlay");
    this.state.playTime = "00:00";
    this.state.duration = "00:00";
    this.audioRecorderPlayer.stopPlayer();
    this.song.stop();
    this.song.release();
    TimerMixin.clearInterval(this.timer);

    this.setState({
      isPlaying: false,
      isPaused: false,
      onTransmission: true
    });
  };

  selectAudio = currentAudio => {
    if (this.state.isPlaying) {
      console.warn("playing");
      this.onStopPlay();
    }

    this.setState({
      activate: currentAudio.id,
      index: this.state.playList.indexOf(currentAudio),
      selected_audio : currentAudio
    });

    if (currentAudio.url) {
      this.onStartPlay(currentAudio.url);
    }
  };

  onStartPlay =  url => {
    if (this.timer) TimerMixin.clearInterval(this.timer);
    console.warn(url);
    this.song = new Sound(url, Sound.MAIN_BUNDLE, error => {
      if (error) {
        console.warn("failed to load the sound", error);
        return;
      }
      this.song.setCategory("Playback");
      //activate audio in silence mode ios

      this.timer = TimerMixin.setInterval(() => {
        this.song.getCurrentTime(current => {
          let minutes = Math.floor(current / 60);
          let seconds = ~~current % 60;

          this.setState({
            currentPositionSec: Math.floor(current),
            current : current,
            progress :  current / this.song.getDuration(),
            playTime: `${minutes < 10 ? "0" + minutes : minutes}:${
              seconds < 10 ? "0" + seconds : seconds
            }`
          });

          if(this.state.current == this.song.getDuration()){
            if(this.state.isPlaying){
                clearInterval(this.timer)
                this.onStopPlay();
                return;       
            }
          }

        });
      }, 1000);
      console.warn(this.song);
      let duration = this.song.getDuration();
      let minutes = Math.floor(duration / 60);
      let seconds = ~~duration % 60;

      this.setState({
        currentDurationSec: duration,
        duration: `${minutes < 10 ? "0" + minutes : minutes}:${seconds}`
      });

      this.song.play(success => {
        if (!success) {
          //ToastAndroid.show("Error when play Sound :(((", ToastAndroid.SHORT);
        }

        //this.song.reset();
      });
    });

    this.setState({
      isPlaying: true,
      isPaused: false,
      onTransmission: false
    });
  };

  next = () => {
    if(this.state.isPaused){
      this.state.playTime = "00:00:00";
      this.state.duration = "00:00:00";
      this.audioRecorderPlayer.stopPlayer();
      this.audioRecorderPlayer.removePlayBackListener();
      delete this.state.progress;
    }

    try {
      return this.selectAudio(this.state.playList[this.state.index + 1]);
    } catch (e) {
      this.selectAudio(this.state.playList[0]);
    }
  };

  previous = () => {
    if(this.state.isPaused){
      this.state.playTime = "00:00:00";
      this.state.duration = "00:00:00";
      delete this.state.progress;
      this.audioRecorderPlayer.stopPlayer();
      this.audioRecorderPlayer.removePlayBackListener();      
    }

    try {
      return this.selectAudio(this.state.playList[this.state.index - 1]);
    } catch (e) {
      this.selectAudio(this.state.playList[this.state.playList.length - 1]);
    }
  };

  rewind = async () => {
    if (this.state.isPlaying) {

      let totalDuration = this.state.currentDurationSec;
      let current = this.state.currentPositionSec;
      let rewind = Math.floor(current - 15);

      console.warn(rewind);

      if (rewind > 0) await this.song.setCurrentTime(rewind);
      else await this.song.setCurrentTime(0);
    }
  };

  forward = async () => {
    if (this.state.isPlaying) {
      
      let totalDuration = this.state.currentDurationSec;
      let current = this.state.currentPositionSec;
      let forward = Math.floor(current + 15);

      if (forward <= totalDuration) await this.song.setCurrentTime(forward);
      else alert(i18n.t("globals.duration_lower"));

      if (forward <= totalDuration) await this.song.setCurrentTime(forward);
      else alert("Sorry current audio duration lower or equal to 15 seconds");
    }
  };

  onShareAudio = async audio => {
    //console.warn(audio, "audio");

    // Vibration.vibrate([300]);

    let fileUrl = audio.url;
    console.warn(fileUrl, "FILE URL");

    let audioPath =
      RNFetchBlob.fs.dirs.CacheDir.replace("Caches", "tmp").replace(
        "/Library",
        ""
      ) +
      "/" +
      audio.url.split("/")[audio.url.split("/").length - 1];

    let options = {
      type: "audio/mp3",
      url: audioPath
    };

    await Share.open(options);

    //let know to the user that the audio was shared succesful
    alert(i18n.t("globals.audio_was_shared_ok"));
  };

  getTypeSearch = data => {
      const type = "defaultAudios";

    if (data == "name") {
        PlayList.getOrderName(type).then(data => {
          //alert(data.length)
          this.setState({
            playList: data
          });
        });
    }

    if (data.type == "filter") {
      let value = data.value;
      const type = "defaultAudios";

      PlayList.getByCriteriaDefault(value, type).then(data => {
        this.setState({
          playList: data
        });
      });
    }

    if (data == "Lenght") {
      const type = "defaultAudios";
      PlayList.getOrderLenght(type).then(data => {
        this.setState({
          playList: data
        });
      });
    }
    if (data == "size") {
      const type = "defaultAudios";
      PlayList.getOrderSize(type).then(data => {
        this.setState({
          playList: data
        });
      });
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <Animatable.View style={[styles.container]}>
          <LinearGradient
            start={{ x: 0.0, y: 0.25 }}
            end={{ x: 1.5, y: 0.5 }}
            colors={["#5ccbfa", "#33a2f2"]}
            style={styles.linearGradient}
          >
            <Header
              updateSearch={this.getTypeSearch}
              typeConsult={"defaultAudios"}
            />
          </LinearGradient>
          <TabMenu
            update={this.changeTab}
            onPress={this.onStopPlay}
            value={3}
            onLeave={this.onStopPlay}
          />
          <Promo />
          <FlatList
            data={this.state.playList}
            extraData={this.state}
            onRefresh={() => {
              this.setState({ refreshing: true });
              TimerMixin.setTimeout(() => {
                this.setState({ refreshing: false });
              }, 2000);
            }}
            refreshing={this.state.refreshing ? true : false}
            renderItem={({ item }) => (
              <DefaultAudio
                sound={item}
                activate={this.state.activate}
                onPress={this.selectAudio}
                onLongPress={this.onShareAudio}
                keyExtractor={(item, index) => item.id}
              />
            )}
          />

          <View style={styles.reproductorContainer}>
            <View style={styles.reproductorContainerRow}>
              <TouchableOpacity
                style={[
                  styles.reproductorContainerImage,
                  { alignItems: "flex-end" }
                ]}
                onPress={this.rewind}
              >
                <Image
                  style={styles.reproductorReturnAudio}
                  source={require("../../resources/img/15left.png")}
                />
              </TouchableOpacity>
              <TouchableOpacity
                style={[styles.reproductorContainerImage]}
                onPress={() => {
                  this.previous();
                }}
              >
                <Image
                  style={styles.reproductorBtnNext}
                  source={require("../../resources/img/leftSong.png")}
                />
              </TouchableOpacity>

              {this.state.onTransmission == true ? (
                <TouchableOpacity onPress={this.onStartPlayWithoutUrl}>
                  <Image
                    style={styles.reproductorBtnPlay}
                    source={require("../../resources/img/play.png")}
                  />
                </TouchableOpacity>
              ) : (
                <TouchableOpacity onPress={this.onPausePlay}>
                  <Image
                    style={[styles.reproductorBtnPlay, {}]}
                    source={require("../../resources/img/paused.png")}
                  />
                </TouchableOpacity>
              )}
              <TouchableOpacity
                style={styles.reproductorContainerImage}
                onPress={() => {
                  this.next();
                }}
              >
                <Image
                  style={styles.reproductorBtnNext}
                  source={require("../../resources/img/rightSong.png")}
                />
              </TouchableOpacity>
              <TouchableOpacity
                style={[
                  styles.reproductorContainerImage,
                  { alignItems: "flex-start" }
                ]}
                onPress={this.forward}
              >
                <Image
                  style={styles.reproductorReturnAudio}
                  source={require("../../resources/img/15right.png")}
                />
              </TouchableOpacity>
            </View>
            <View style={[styles.reproductorContainerLine]}>
              <View styles={[styles.reproductorTimes]}>
                <Text
                  style={[styles.reproductorTimeText, { paddingRight: 10 }]}
                >
                  {this.state.playTime || "00:00"}
                </Text>
              </View>

              <View styles={styles.reproductorLineTime}>
              { (
                  <Slider
                    value={this.state.progress || 0}
                    thumbStyle = {{width: 10, height: 10}}
                    thumbTintColor = {"#33a2f2"}
                    minimumTrackTintColor = {"#33a2f2"}
                    trackStyle = {{ backgroundColor: "#808080", height : 1}}
                    style={{ width: 208, height: 1}}
                    onValueChange={value => this.setState({ value })}
                  />
                )
              }
              </View>
              <View styles={styles.reproductorTimes}>
                <Text style={[styles.reproductorTimeText, { paddingLeft: 10 }]}>
                  {this.state.duration || "00:00"}
                </Text>
              </View>
            </View>
          </View>
        </Animatable.View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  reproductorContainer: {
    display: "flex",
    width: "100%",
    height: 100,
    bottom: 0,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#e0e0e0",
    borderTopColor: "#cad0d4",
    borderTopWidth: 1
  },
  reproductorContainerRow: {
    flexDirection: "row",
    width: "90%",
    alignItems: "center",
    justifyContent: "center",
    paddingBottom: 10,
    height: 30
  },
  reproductorContainerLine: {
    flexDirection: "row",
    width: "90%",
    alignItems: "center",
    justifyContent: "center",
    paddingBottom: 10,
    height: 30
  },
  reproductorContainerImage: {
    width: "20%",
    height: 50,
    alignItems: "center",
    justifyContent: "center"
  },
  reproductorTimes: {
    width: "10%"
  },
  reproductorTimeText: {
    fontSize: 12,
    color: "#828282"
  },
  reproductorLineTime: {
    width: "80%"
  },

  reproductorBtnNext: {
    width: 19,
    height: 22
  },
  reproductorReturnAudio: {
    width: 20.92,
    height: 23.4
  },
  reproductorBtnPlay: {
    width: 62,
    height: 62,
    top: -15
  },
  progress: {
      height:3,
      backgroundColor:'#ccc',
      borderRadius:2,
      marginLeft: 10,
      marginRight: 10,
  }

});


//currentAudio={this.state.currentAudio}
