import React, { Component } from "react";
import {
  StyleSheet,
  View,
  TextInput,
  Keyboard,
  Text,
  TouchableOpacity,
  Image
} from "react-native";
import { GlobalStyles } from "../../../resources/style/style";
import i18n from "../../../AppGlobalConfig/Localization/i18n";
import PropTypes from "prop-types";
import PopoverTooltip from "react-native-popover-tooltip";

export default class Search extends Component {
  constructor() {
    super();
    this.state = {
      inputRef: null,
      items: [],
      focus: false
    };
  }
  items = [];
  componentDidMount() {
    if (this.props.typeConsult != "defaultAudios") {
      this.createItemsAudios();
    } else {
      this.createItemsDefault();
    }
  }
  componentWillMount() {
    if (this.props.typeConsult != "defaultAudios") {
      this.createItemsAudios();
    } else {
      this.createItemsDefault();
    }
  }
  createItemsDefault() {
    let items = [
      {
        label: () => {
          return <Text style={GlobalStyles.colorTootltipLabel}>Name</Text>;
        },
        onPress: () => {
          this.props.update("name");
        }
      },

      {
        label: () => {
          return <Text style={GlobalStyles.colorTootltipLabel}>Lenght</Text>;
        },
        onPress: () => {
          this.props.update("Lenght");
        }
      },
      {
        label: () => {
          return <Text style={GlobalStyles.colorTootltipLabel}>File Size</Text>;
        },
        onPress: () => {
          this.props.update("size");
        }
      }
    ];
    this.setState({ items: items });
    console.log(this.state.items);
  }
  createItemsAudios() {
    console.log("Creando audios");
    let items = [
      {
        label: () => {
          return <Text style={GlobalStyles.colorTootltipLabel}>Name</Text>;
        },
        onPress: () => {
          this.props.update("name");
        }
      },
      {
        label: () => {
          return <Text style={GlobalStyles.colorTootltipLabel}>Date</Text>;
        },
        onPress: () => {
          this.props.update("date");
        }
      },
      {
        label: () => {
          return <Text style={GlobalStyles.colorTootltipLabel}>Lenght</Text>;
        },
        onPress: () => {
          this.props.update("Lenght");
        }
      },
      {
        label: () => {
          return <Text style={GlobalStyles.colorTootltipLabel}>File Size</Text>;
        },
        onPress: () => {
          this.props.update("size");
        }
      },
      {
        label: () => {
          return (
            <Text style={GlobalStyles.colorTootltipLabel}>Oldest First</Text>
          );
        },
        onPress: () => {
          this.props.update("oldest");
        }
      },
      {
        label: () => {
          return (
            <Text style={GlobalStyles.colorTootltipLabel}>Newest First</Text>
          );
        },
        onPress: () => {
          this.props.update("newest");
        }
      }
    ];
    this.setState({ items: items });
    console.log(this.state.items);
  }
  updateText(value) {
    this.props.update({ type: "filter", value: value });
  }

  render() {
    return (
      <View style={styles.containerRow}>
        <TouchableOpacity
          onPress={() => {
            this.refs["tooltip1"].toggle(); // open popover tooltips one by one
          }}
        >
          <Image
            style={styles.filter}
            source={require("../../../resources/img/filter.png")}
          />
        </TouchableOpacity>
        {this.props.typeConsult != "defaultAudios" ? (
          <PopoverTooltip
            ref="tooltip1"
            items={this.state.items}
            labelSeparatorColor="#cad0d4"
            labelStyle="#cad0d4"
            overlayStyle={{ backgroundColor: "#21252912" }}
            triangleOffset={-25}
            setBelow={true}
            tooltipContainerStyle={[
              {
                padding: 10,
                marginLeft: 18,
                top: 16,
                marginTop: -30
              }
            ]}
            // animationType='timing'
            // using the default timing animation
          />
        ) : (
          <PopoverTooltip
            ref="tooltip1"
            items={this.state.items}
            labelSeparatorColor="#cad0d4"
            labelStyle="#cad0d4"
            overlayStyle={{ backgroundColor: "#21252912" }}
            triangleOffset={-25}
            setBelow={true}
            tooltipContainerStyle={[
              {
                padding: 10,
                marginLeft: 18,
                top: 16,
                marginTop: -45
              }
            ]}
            // animationType='timing'
            // using the default timing animation
          />
        )}

        <TouchableOpacity
          style={styles.containerSearch}
          onPress={() => {
            this.state.inputRef.focus();
          }}
        >
          <Image
            style={styles.filter}
            source={require("../../../resources/img/search.png")}
          />
        </TouchableOpacity>

        <View style={[styles.containerInput, { left: -15 }]}>
          <TextInput
            ref={ref => {
              this.state.inputRef = ref;
            }}
            style={{
              fontSize: GlobalStyles.regularText.fontSize,
              color: "white",
              height: 40,
              bottom: 11
            }}
            blurOnSubmit={this.state.focus}
            placeholderTextColor="#fff"
            placeholder="Search"
            returnKeyType={this.props.returnKeyType}
            autoCapitalize="none"
            keyboardType={this.props.keyboardType}
            onSubmitEditing={() => {
              Keyboard.dismiss();
            }}
            onChangeText={v => {
              this.updateText(v);
            }}
            onEndEditing={this.checkIfIsCorrect}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerRow: {
    display: "flex",
    width: "100%",
    height: 35,
    padding: 5,
    paddingLeft: 20,
    flexDirection: "row"
  },
  containerFilter: {
    width: "10%"
  },
  containerSearch: {
    width: "20%",
    paddingLeft: "10%"
  },
  containerInput: {
    width: "70%",
    paddingLeft: 0,
    color: "#fff"
  },
  filter: {
    width: 17.7,
    height: 17.3
  }
});
