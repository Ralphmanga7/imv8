import React, { Component } from "react";
import LinearGradient from "react-native-linear-gradient";
import { GlobalStyles } from "../../../resources/style/style";
import { ToolTipAudio } from "../tooltips/ToolTipAudio";
import PopoverTooltip from "react-native-popover-tooltip";
import TimerMixin from "react-timer-mixin";
import { withNavigation } from "react-navigation";
import ServiceAlarm from "../../../services/AlarmService";
import SwitchToggle from "react-native-switch-toggle";

import {
  Platform,
  StyleSheet,
  Text,
  View,
  Button,
  TouchableOpacity,
  ImageBackground,
  TextInput,
  ScrollView
} from "react-native";
import Moment from "moment";
import PropTypes from "prop-types";
class Alarm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      inputRef: null,
      value: ""
    };
  }

  loadAudioList = async () => {
    //await AsyncStorage.removeItem("audios");
    await PlayList.get().then(data => {
      this.setState({
        alarmList: data
      });
    });
  };
  componentDidMount() {
    this.setState({
      active: this.props.alarm.active
    });
  }
  changeStateSwitch() {
    console.warn(this.state);
  }
  render() {
    return (
      <View style={[styles.container]}>
        <View
          style={{
            width: "100%",
            height: 70,
            paddingLeft: 15
          }}
        >
          <View style={[styles.card, {}]}>
            <View
              style={{
                width: "75%"
              }}
            >
              <Text style={[styles.fistline, GlobalStyles.titleHeader]}>
                {this.props.alarm.textHour}
                {this.props.alarm.active}
              </Text>
              <Text style={{ color: "#616770" }}>
                {this.props.alarm.description}
              </Text>
            </View>
            <View style={[styles.comtainerFirstLine]}>
              <SwitchToggle
                containerStyle={{
                  width: 55,
                  height: 30,
                  borderRadius: 25,
                  padding: 1
                }}
                circleStyle={{
                  width: 28,
                  height: 26,
                  borderRadius: 30,
                  backgroundColor: "#33a2f2" // rgb(102,134,205)
                }}
                switchOn={this.state.active}
                onPress={() => {
                  data = {
                    type: this.props.typeView,
                    id: this.props.idAudio,
                    idAlarm: this.props.alarm.id,
                    active: this.state.active == true ? false : true
                  };
                  ServiceAlarm.changeStateAlarm(data).then(response => {
                    this.setState({
                      active: response
                    });
                  });
                }}
                circleColorOff="#fff"
                circleColorOn="#fff"
                backgroundColorOff="gray"
                backgroundColorOn="#33a2f2"
                duration={500}
              />
            </View>
          </View>
          <TouchableOpacity style={[styles.toolContainer, {}]} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    display: "flex",
    width: "100%",
    height: 70,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#f1f2f3"
  },
  buttonComponent: {
    width: 35.5,
    height: 65.2,
    zIndex: 100
  },
  ToolTipImage: {
    width: 8,
    height: 37,
    padding: 10,
    paddingLeft: 80,
    marginTop: 10
  },
  colorTootltipLabel: {
    color: "#cad0d4"
  },
  linearGradient: {
    width: "100%",
    height: 70,
    paddingLeft: 10
  },
  activateColor: {
    color: "#1676bb"
  },
  borderLeft: {
    borderLeftWidth: 5,
    borderLeftColor: "#33a2f2"
  },
  comtainerFirstLine: {
    width: "25%",
    justifyContent: "flex-end",
    alignItems: "flex-end",
    paddingBottom: 20
  },

  containerRow: {
    display: "flex",
    width: "100%",
    height: 53,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#f1f2f3"
  },

  fistline: {
    paddingBottom: 1,
    fontSize: 16,
    color: "#828282"
  },
  secoundLine: {
    color: "#616770",
    fontSize: 11
  },
  title: {
    color: "#fff",
    fontSize: 25,
    paddingTop: 30,
    paddingLeft: 30
  },
  subTitle: {
    color: "#fff",
    fontSize: 12,
    paddingTop: 5,
    paddingLeft: 27
  },
  card: {
    width: "95%",
    paddingTop: 10,
    height: 10,
    flex: 1,
    flexDirection: "row",
    borderBottomColor: "#cad0d4",
    borderBottomWidth: 0.5
  },
  cardActiva: {
    width: "95%",
    paddingTop: 10,
    height: 10,
    flex: 1,
    flexDirection: "row",
    borderBottomColor: "#cad0d4",
    borderBottomWidth: 0.5
  },
  tooltip: {
    height: 45.2,
    width: 35.5,
    padding: 10,
    top: 5,
    right: 22
  },
  toolContainer: {
    width: "10%",
    justifyContent: "flex-end",
    alignItems: "flex-end",
    padding: 0,
    position: "absolute",
    right: 17
  }
});

export default withNavigation(Alarm);
