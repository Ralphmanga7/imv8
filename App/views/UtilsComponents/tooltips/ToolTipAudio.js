import React, { Component } from "react";
import { GlobalStyles } from "../../../resources/style/style";
import i18n from "../../../AppGlobalConfig/Localization/i18n";
import PopoverTooltip from "react-native-popover-tooltip";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  ImageBackground,
  TextInput,
  AsyncStorage
} from "react-native";

import { withNavigation } from "react-navigation";

import {
  Menu,
  MenuOptions,
  MenuOption,
  MenuTrigger
} from "react-native-popup-menu";

import PropTypes from "prop-types";
class ToolTipAudio extends Component {
  constructor(props) {
    super(props);
    this.state = {
      order: 1
    };
  }

  logout = async () => {
    await AsyncStorage.removeItem("userToken");
    this.props.navigation.navigate("AuthLoading");
  };

  render() {
    return (
      <View
        style={{
          width: "10%",
          justifyContent: "flex-end",
          alignItems: "flex-end",
          paddingBottom: 12
        }}
      >
        <PopoverTooltip
          ref="tooltip1"
          buttonComponent={
            <View>
              <ImageBackground
                style={{
                  height: 41.2,
                  width: 8.5
                }}
                resizeMode="center"
                resizeMethod="auto"
                source={require("../../../resources/img/tabmenu.png")}
              />
            </View>
          }
          items={[
            {
              label: "Item 1",
              onPress: () => {}
            },
            {
              label: "Item 2",
              onPress: () => {}
            }
          ]}
        />
      </View>
    );
  }
}

export default withNavigation(ToolTipAudio);
