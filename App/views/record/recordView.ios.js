import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Image,
  ImageBackground,
  View,
  Text,
  ScrollView,
  TextInput,
  Keyboard
} from "react-native";

import { GlobalStyles } from "../../resources/style/style";
import LinearGradient from "react-native-linear-gradient";
import Header from "../UtilsComponents/Seccions/header";
import Audio from "../UtilsComponents/Audio/Audio";
import TabMenu from "../UtilsComponents/Seccions/TabMenu";
import PlayList from "../../provider/playList";
import Reproductor from "../UtilsComponents/Seccions/Reproductor";
import Promo from "../UtilsComponents/Seccions/promo";
import RecorderTemp from "../UtilsComponents/record/recordTemp";
import Recorder from "../UtilsComponents/record/record";
import i18n from "../../AppGlobalConfig/Localization/i18n";
import UUIDGenerator from "react-native-uuid-generator";

export default class Record extends Component {
  constructor(props) {
    super(props);

    this.state = {
      ActiveTab: 1,
      playList: [],
      stateRecover: "",
      title: "",
      date: "",
      duration: "",
      size: "",
      link: "",
      value: "",
      keyboard: false
    };

    this._keyboardDidShow = this._keyboardDidShow.bind(this);
    this._keyboardDidHide = this._keyboardDidHide.bind(this);
  }

  componentWillMount() {
    this.keyboardDidShowListener = Keyboard.addListener(
      "keyboardDidShow",
      this._keyboardDidShow
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      "keyboardDidHide",
      this._keyboardDidHide
    );
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow() {
    this.setState({ keyboard: true });
    console.warn("hidden keyboard", true);
  }

  _keyboardDidHide() {
    this.setState({ keyboard: false });
    console.warn("hidden keyboard", false);
  }

  goToRecord() {
    console.warn(input, "changeState");
  }
  goToAudioList = () => {
    this.props.navigation.navigate("MyAudioView");
  };

  getStateRecord = data => {
    console.log(data);
    this.setState({
      stateRecover: data.state,
      value: data.nameAudio
    });
  };
  updateText = value => {
    this.setState({
      value: value
    });

    console.log(this.state.value);
  };
  componentDidMount() {}

  showCmdRecord() {
    if (!this.state.keyboard) {
      return (
        <RecorderTemp onPress={this.getStateRecord} name={this.state.value} />
      );
    }

    return null;
  }

  render() {
    return [
      <View style={GlobalStyles.container}>
        <LinearGradient
          start={{ x: 0.0, y: 0.25 }}
          end={{ x: 1.5, y: 0.5 }}
          colors={["#5ccbfa", "#33a2f2"]}
        >
          <Header title={i18n.t("recorder-component.title")} />
        </LinearGradient>
        <TabMenu value={2} />
        <ScrollView
          scrollEnabled={false}
          style={{ backgroundColor: "#f1f2f3", width: "100%", height: "100%" }}
        >
          <View style={[GlobalStyles.container]}>
            {this.state.stateRecover == "onStartPlay" ? (
              <View
                style={[styles.container, { flex: 1, flexDirection: "column" }]}
              >
                <View style={{ width: "100%" }}>
                  <Text style={styles.recordText}>{this.state.value}.wav</Text>
                </View>
                <View
                  style={{ width: "100%", height: 230, borderColor: "#f1f2f3" }}
                >
                  <ImageBackground
                    source={require("../../resources/img/backgroundAudio.png")}
                    resizeMode="contain"
                    resizeMethod="auto"
                    style={{
                      flexDirection: "row",
                      flex: 1,
                      width: "100%",
                      height: "85%"
                    }}
                  />
                </View>
              </View>
            ) : (
              <View style={styles.container}>
                <TextInput
                  style={styles.recordInput}
                  placeholderTextColor="#000000"
                  placeholder=" My Recording #00"
                  blurOnSubmit={false}
                  returnKeyType={this.props.returnKeyType}
                  ref={ref => {
                    this.state.inputRef = ref;
                  }}
                  autoCapitalize="none"
                  keyboardType={this.props.keyboardType}
                  onSubmitEditing={Keyboard.dismiss}
                  onChangeText={this.updateText}
                  onEndEditing={this.checkIfIsCorrect}
                />
                <Text style={styles.recordExtention}>.wav</Text>
                <Text style={styles.recordCaption}>Ready to Start</Text>
              </View>
            )}
          </View>
          {this.state.stateRecover !== "onStartPlay" ? (
            <View style={[styles.container, { height: 300 }]}>
              <Text style={styles.recordQuote}>8h left on storage</Text>
            </View>
          ) : null}
        </ScrollView>
        {this.showCmdRecord()}
      </View>
    ];
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
    top: "25%"
  },

  recordInput: {
    width: "70%",
    textAlign: "center",
    fontSize: GlobalStyles.titleHeader.fontSize - 1,
    height: 45,
    borderBottomWidth: 1,
    borderBottomColor: "#333333"
  },
  recordText: {
    textAlign: "center",
    fontSize: GlobalStyles.titleHeader.fontSize - 1,
    height: 40
  },

  recordExtention: {
    fontSize: 22
  },

  recordCaption: {
    position: "absolute",
    top: "100%",
    textAlign: "center",
    color: "rgba(140, 138, 138, 0.8)",
    fontSize: GlobalStyles.regularText.fontSize,
    paddingTop: 5
  },

  recordQuote: {
    textAlign: "center",
    color: "rgba(140, 138, 138, 0.8)",
    backgroundColor: "#f1f2f3",
    fontSize: 12,
    width: "100%"
  }
});
