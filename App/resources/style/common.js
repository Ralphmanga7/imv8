import { Dimensions } from 'react-native';
const { width, height } = Dimensions.get('window');

export const SmallPhone = () => {
  if(width<=320) // Here you could use "react-native-device-info" to get device information.
     return true
  else 
     return false
}