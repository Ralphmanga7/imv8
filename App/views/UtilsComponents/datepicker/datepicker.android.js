import React, { Component } from "react";

import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  TimePickerAndroid
} from "react-native";

import PropTypes from "prop-types";
import EStyleSheet from "react-native-extended-stylesheet";
import Moment from "moment";
import i18n from "../../../AppGlobalConfig/Localization/i18n";
import { GlobalStyles } from "../../../resources/style/style";

export default class Datepicker extends Component {
  constructor(props) {
    super(props);
    this.state = { chosenDate: new Date() };
  }

  setDate(newDate) {
    this.props.getDate(newDate);
    this.setState({ chosenDate: newDate });
  }

  async chooseDate() {
    try {
      const {action, hour, minute} = await TimePickerAndroid.open({
        hour: 12,
        minute: 0,
        is24Hour: false, // Will display '2 PM'
      });

      if (action !== TimePickerAndroid.dismissedAction) {
        this.props.getDate(new Date(1999, 12, 3, hour, minute, 0, 0));
        this.setState({ chosenDate : new Date(1999, 12, 3, hour, minute, 0, 0) });
        // Selected hour (0-23), minute (0-59)
      }
    } catch ({ code, message }) {
      console.warn("Cannot open date picker", message);
    }
  }

  render() {
    return (
      <View style={styles.containerText}>
        <View style={styles.container30}>
          <Text style={GlobalStyles.regularText}>{i18n.t("globals.label_select_time")}</Text>
        </View>
        <View style={[styles.container70, styles.right]}>
          <TouchableOpacity
            onPress={() => {
              this.chooseDate();
            }}
          >
            <Text style={[GlobalStyles.regularText, { color: "#cad0d4" }]}>
              {Moment(this.state.chosenDate).format("HH:mm A")}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff"
  },
  containerDatapicker: {
    width: "100%",
    paddingTop: 10
  },
  right: {
    alignItems: "flex-end",
    justifyContent: "flex-end",
    paddingRight: 25
  },
  containerText: {
    borderLeftWidth: 0,
    borderRightWidth: 0,
    borderTopWidth: 0,
    paddingTop: 9,
    paddingBottom: 9,
    paddingLeft: 27,
    borderColor: "#cad0d4",
    flexDirection: "row"
  },
  containerBtns: {
    borderWidth: 1,
    borderLeftWidth: 0,
    borderRightWidth: 0,
    borderTopWidth: 0,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 27,
    borderColor: "#cad0d4",
    flexDirection: "row"
  },
  LinearGradient: {
    borderRadius: 50,
    width: 40,
    height: 40,
    alignItems: "center",
    justifyContent: "center"
  },
  containerBtn: {
    width: "13.5%",
    borderColor: "#cad0d4",
    borderWidth: 0,
    paddingLeft: 2,
    paddingRight: 2
  },
  container30: {
    width: "50%"
  },
  container70: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    width: "70%"
  },
  btn: {
    backgroundColor: "#e0e0e0",
    borderRadius: 50,
    width: 40,
    height: 40,
    alignItems: "center",
    justifyContent: "center"
  },
  color: {
    color: "#828282"
  }
});
