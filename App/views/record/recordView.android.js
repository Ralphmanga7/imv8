import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Image,
  ImageBackground,
  View,
  Text,
  ScrollView,
  TextInput,
  Keyboard,
  TouchableOpacity
} from "react-native";

import { GlobalStyles } from "../../resources/style/style";
import LinearGradient from "react-native-linear-gradient";
import Header from "../UtilsComponents/Seccions/header";
import Audio from "../UtilsComponents/Audio/Audio";
import TabMenu from "../UtilsComponents/Seccions/TabMenu";
import PlayList from "../../provider/playList";
import Reproductor from "../UtilsComponents/Seccions/Reproductor";
import Promo from "../UtilsComponents/Seccions/promo";
import RecorderTemp from "../UtilsComponents/record/recordTemp";
import Recorder from "../UtilsComponents/record/record";
import i18n from "../../AppGlobalConfig/Localization/i18n";
import UUIDGenerator from "react-native-uuid-generator";
import Dialog from "react-native-dialog";

export default class Record extends Component {
  constructor(props) {
    super(props);

    this.state = {
      ActiveTab: 1,
      playList: [],
      stateRecover: "",
      title: "",
      date: "",
      duration: "",
      size: "",
      link: "",
      value: "",
      keyboard: false,
      dialogVisible: false
    };
  }

  componentWillMount() {}

  componentWillUnmount() {}

  goToRecord() {
    console.warn(input, "changeState");
  }
  goToAudioList = () => {
    this.props.navigation.navigate("MyAudioView");
  };

  getStateRecord = data => {
    console.log(data);
    this.setState({
      stateRecover: data.state,
      value: data.nameAudio
    });
  };

  updateText = value => {
    this.setState({
      value: value
    });
  };

  componentDidMount() {}

  showDialog = async () => {
    let md = await this.setState({ dialogVisible: true });

    if (md) {
      console.warn("opened");
    }
  };

  rename = () => {
    this.setState({ dialogVisible: false });
  };

  cancel = () => {
    this.setState({ dialogVisible: false });
  };

  showCmdRecord() {
    if (!this.state.keyboard) {
      return (
        <RecorderTemp onPress={this.getStateRecord} name={this.state.value} />
      );
    }
  }

  render() {
    return [
      <View style={GlobalStyles.container}>
        <LinearGradient
          start={{ x: 0.0, y: 0.25 }}
          end={{ x: 1.5, y: 0.5 }}
          colors={["#5ccbfa", "#33a2f2"]}
        >
          <Header title={i18n.t("recorder-component.title")} />
        </LinearGradient>
        <TabMenu value={2} />
        <Dialog.Container visible={this.state.dialogVisible} contentStyle={{}}>
          <Dialog.Title>Audio Name</Dialog.Title>
          <Dialog.Description>{i18n.t("recorder-component.type_audio_name")}</Dialog.Description>
          <Dialog.Input
            ref={ref => {
              this.state.inputModal = ref;
            }}
            onChangeText={e => {
              this.updateText(e);
            }}
            placeholder={i18n.t("recorder-component.audio_name")}
            wrapperStyle={{ borderWidth: 1, padding: 4 }}
          />
          <Dialog.Button label="Cancel" onPress={this.cancel} />
          <Dialog.Button label="Rename" onPress={this.rename} />
        </Dialog.Container>
        <ScrollView
          scrollEnabled={false}
          style={{ backgroundColor: "#f1f2f3", width: "100%", height: "100%" }}
        >
          <View style={[GlobalStyles.container]}>
            {this.state.stateRecover == "onStartPlay" ? (
              <View
                style={[
                  styles.container,
                  {
                    flex: 1,
                    flexDirection: "column",

                    top: "15%"
                  }
                ]}
              >
                <View style={{ width: "100%" }}>
                  <Text style={styles.recordText}>{this.state.value}.wav</Text>
                </View>
                <View
                  style={{ width: "100%", height: 230, borderColor: "#f1f2f3" }}
                >
                  <ImageBackground
                    source={require("../../resources/img/backgroundAudio.png")}
                    resizeMode="contain"
                    resizeMethod="auto"
                    style={{
                      flexDirection: "row",
                      flex: 1,
                      width: "100%",
                      height: "85%"
                    }}
                  />
                </View>
              </View>
            ) : (
              <View style={styles.container}>
                <TextInput
                  style={styles.recordInput}
                  placeholderTextColor="#000000"
                  placeholder=" My Recording #00"
                  ref={ref => {
                    this.state.inputRef = ref;
                  }}
                  value={this.state.value}
                  keyboardType={"none"}
                  onTouchStart={() => {
                    this.showDialog();
                  }}
                />
                <Text style={styles.recordExtention}>.wav</Text>
                <Text style={styles.recordCaption}>Ready to Start</Text>
              </View>
            )}
          </View>
          {this.state.stateRecover !== "onStartPlay" ? (
            <View style={[styles.container, { height: 300}]}>
              <Text style={styles.recordQuote}>8h left on storage</Text>
            </View>
          ) : null}
        </ScrollView>
        {this.showCmdRecord()}
      </View>
    ];
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
    top: "25%"
  },

  recordInput: {
    width: "70%",
    textAlign: "center",
    fontSize: GlobalStyles.titleHeader.fontSize - 1,
    height: 45,
    borderBottomWidth: 1,
    borderBottomColor: "#333333"
  },
  recordText: {
    textAlign: "center",
    fontSize: GlobalStyles.titleHeader.fontSize - 1,
    height: 40
  },

  recordExtention: {
    fontSize: 22
  },

  recordCaption: {
    position: "absolute",
    top: "100%",
    textAlign: "center",
    color: "rgba(140, 138, 138, 0.8)",
    fontSize: GlobalStyles.regularText.fontSize,
    paddingTop: 5
  },

  recordQuote: {
    textAlign: "center",
    color: "rgba(140, 138, 138, 0.8)",
    backgroundColor: "#f1f2f3",
    fontSize: 12,
    width: "100%"
  }
});
