import React, { Component } from "react";
import PropTypes from "prop-types";
import { TouchableOpacity, Text, View, Dimensions, Platform } from "react-native";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { GlobalStyles } from "../../../resources/style/style";

export default class Btn extends Component {
  constructor() {
    super();
  }

  render() {
    return (
      <TouchableOpacity
        style={[
          styles.btn,
          {
            backgroundColor: this.props.backgroundColor,
            color: this.props.color,
            padding:isIphoneXorAbove(6, 4),
          },
          this.props.customStyle
        ]}
        onPress={this.props.onPress}
      >
        <Text style={{ color: this.props.textColor, fontSize:GlobalStyles.titleText_android.fontSize }}>{this.props.text}</Text>
      </TouchableOpacity>
    );
  }
}

isIphoneXorAbove = (s, n)=> {
  const dimen = Dimensions.get('window');
  
  if((
    Platform.OS === 'ios' &&
    !Platform.isPad &&
    !Platform.isTVOS &&
    ((dimen.height === 812 || dimen.width === 812) || (dimen.height === 896 || dimen.width === 896))
  )){

    return s;

  }

  return n;
}

Btn.propTypes = {
  text: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired,
  color: PropTypes.string,
  textColor: PropTypes.string,
  customStyle: PropTypes.object
};

Btn.defaultProps = {
  textColor: "#828282",
  customStyle: {}
};

const styles = {
  btn: {
    display: "flex",
    color: "#828282",
    height: isIphoneXorAbove(hp('6%'), hp('8%')),
    width: wp('100%'), 
    justifyContent : "center"
  }
};
