import React, { Component } from "react";
import { GlobalStyles } from "../../resources/style/style";
import { LoginManager, LoginButton, AccessToken, GraphRequestManager, GraphRequest } from 'react-native-fbsdk';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Button,
  TouchableOpacity,
  ImageBackground,
  TextInput,
  ScrollView,
  AsyncStorage,
  Keyboard,
  PixelRatio,
  Dimensions,
  ActivityIndicator,
  Alert,
  Image
} from "react-native";

import * as Animatable from "react-native-animatable";
import i18n from "../../AppGlobalConfig/Localization/i18n";
import { Login } from "../../services/UserService.js";
import { Form } from "native-base";
import { GoogleSignin, GoogleSigninButton } from 'react-native-google-signin';
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import Email from "../UtilsComponents/Inputs/Email";
import Password from "../UtilsComponents/Inputs/Password";
import Btn from "../UtilsComponents/Buttons/Button";
import Dialog from "react-native-dialog";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

export default class LoginSiginView extends Component {
  constructor(props) {
    super(props);

    this.state = {
      inputs: []
    };

    this.state.login_enabled = false;
    this.state.formError = "";
    GoogleSignin.configure({
      scopes: ['https://www.googleapis.com/auth/drive.readonly'], // what API you want to access on behalf of the user, default is email and profile
      webClientId: '49935950970-20md1gt0fgu3msj5fbkg51tqq7u7iiec.apps.googleusercontent.com', // client ID of type WEB for your server (needed to verify user ID and offline access)
      accountName: '', // [Android] specifies an account name on the device that should be used
      iosClientId: '49935950970-qig3b38ejml497s9f3k0nklbk66mqkmi.apps.googleusercontent.com', // [iOS] optional, if you want to specify the client ID of type iOS (otherwise, it is taken from GoogleService-Info.plist)
    });
  }

  changeInputFocus = index => () => {
    if (index === this.state.inputs.length - 1) {
      this.Login();
      return;
    }

    if (index === 0) {
      // console.warn(this.state.inputs[index + 1]);
      this.state.inputs[index + 1].state.inputRef.focus(); // eslint-disable-line
    } else this.state.inputs[index + 1].state.inputRef.focus();
  };


  isIphoneXorAbove = (s, n)=> {
    const dimen = Dimensions.get('window');
    
    if((
      Platform.OS === 'ios' &&
      !Platform.isPad &&
      !Platform.isTVOS &&
      ((dimen.height === 812 || dimen.width === 812) || (dimen.height === 896 || dimen.width === 896))
    )){

      return s;

    }

    return n;
  }

  updateCanRegisterState = input => {
    console.warn("update", input);
  };

  updateCanRegisterStateEmail = input => {
    console.warn("email", this.state.inputs[0].state.isCorrect);
    console.warn("message", this.state.inputs[0].state.error);
  };

  errorTemplate = () => {
    if (this.state.formError != "") {
      return (
        <View style={[GlobalStyles.containerRow]}>
          <Text
            style={[
              { color: "red", paddingBottom: 25 },
              GlobalStyles.mediumText
            ]}
          >
            {this.state.formError}
          </Text>
        </View>
      );
    } else {
      return null;
    }
  };

  noAvaiblable = () => {
    alert(i18n.t("globals.no-available"));
  };

  show_loading = () => {
    if (this.state.loading) {
      return (
        <View style={[Activity.container, Activity.horizontal]}>
          <ActivityIndicator size="large" color="#FFFFFF" />
        </View>
      );
    }
  };

  google_login  = async () =>{
    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      console.warn('User Info --> ', userInfo);
      this.setState({ userInfo });
    } catch (error) {
      alert("Error")
    }
  }


  facebook_login  = () =>{
    LoginManager.logOut();
    LoginManager.setLoginBehavior('browser');
    LoginManager.logInWithReadPermissions().then(
      (result)=> {
        if (result.isCancelled) {
          console.warn("Login cancelled", result);
        } else {
          AccessToken.getCurrentAccessToken().then(
            (data) => {
              const infoRequest = new GraphRequest(
                '/me?fields=name,picture,id',
                null,
                this._responseInfoCallback
              );
              // Start the graph request.
              new GraphRequestManager().addRequest(infoRequest).start();
            }
          )
        }
      },
      (error)=>{
        console.warn("Login fail with error: " + error);
      }
    );
  }

  _responseInfoCallback = async (error, result) => {
    if (error) {
      console.warn('Error fetching data: ' + error.toString());
    } else {        
        result["_id"] = result.id;

        result = { user : result, token: result.id};
        console.warn(result, "login ok")
        

        await AsyncStorage.setItem("userToken", JSON.stringify(result));
        this.goToHome();
        console.warn('Result Name: ' + result.name + " Result Id : " + result.id);
    }
  }

  goToHome = () => {
    this.props.navigation.navigate("AuthLoading");
  };
  goToRegister = () => {
    this.props.navigation.navigate("Register");
  };
  forgot = () => {
    this.setState({ dialogVisible: true });
  };

  cancel = () => {
    this.setState({ dialogVisible: false });
  };

  ok = () => {
    this.setState({ dialogVisible: false });
  };
  updateText = e => {
    console.log(e);
  };

  Login = async () => {
    Keyboard.dismiss();

    if (!this.state.inputs[0].state.isCorrect) {
      Alert.alert(
        i18n.t("login.email_error_title"),
        i18n.t("login.email_error"),
        [
          {
            text: "OK",
            onPress: () => this.state.inputs[0].state.inputRef.focus()
          }
        ],
        { cancelable: false }
      );
      return;
    }

    if (
      !this.state.inputs[1].state.isCorrect ||
      this.state.inputs[1].state.value.length < 8
    ) {
      Alert.alert(
        i18n.t("login.password_error_title"),
        i18n.t("login.password_error"),
        [
          {
            text: "OK",
            onPress: () => this.state.inputs[1].state.inputRef.focus()
          }
        ],
        { cancelable: false }
      );
      return;
    }

    if (
      !this.state.inputs[0].state.isCorrect ||
      !this.state.inputs[1].state.isCorrect
    ) {
      Alert.alert(
        i18n.t("login.fill_form_title"),
        i18n.t("login.fill_form"),
        [
          {
            text: "OK",
            onPress: () => this.state.inputs[0].state.inputRef.focus()
          }
        ],
        { cancelable: false }
      );
    } else {
      this.setState({ loading: true });

      try {
        const data = {
          username: this.state.inputs[0].state.value,
          password: this.state.inputs[1].state.value
        };

        let response = await Login(data);
        console.warn("status", response.status);

        switch (response.status) {
          case 500:
            Alert.alert(
              i18n.t("errors.sorry"),
              i18n.t("errors.500"),
              [
                {
                  text: "OK",
                  onPress: () => this.state.inputs[0].state.inputRef.focus()
                }
              ],
              { cancelable: false }
            );
            break;
          case 404:
            console.warn("No found");
            break;
          case 401:
            //this.setState({formError: i18n.t("login.invalid_username_password")});
            Alert.alert(
              i18n.t("login.invalid_username_password_title"),
              i18n.t("login.invalid_username_password"),
              [
                {
                  text: "OK",
                  onPress: () => this.state.inputs[0].state.inputRef.focus()
                }
              ],
              { cancelable: false }
            );
            this.setState({ loading: false });
            break;
          case 200:
            let session = await response.json();
            console.warn("session", session);
            await AsyncStorage.setItem("userToken", JSON.stringify(session));
            this.setState({ loading: false });
            this.goToHome();
            break;
        }
      } catch (e) {
        console.warn("error fetch", e);
        this.setState({ loading: false });
      }
    }
  };

  render() {
    return (
      <ImageBackground
        style={[GlobalStyles.containerRow, GlobalStyles.backgroundColor]}
        source={require("../../resources/img//background.png")}
      >
        <KeyboardAwareScrollView
          style={GlobalStyles.containerForm}
          scrollEnabled={false}
        >
          <Animatable.View
            animation="fadeInUp"
            delay={1200}
            duration={700}
            style={[styles.container]}
          >
            <Dialog.Container
              visible={this.state.dialogVisible}
              contentStyle={{}}
            >
              <Dialog.Title>{i18n.t("login.forgot-password")}</Dialog.Title>
              <Dialog.Description>
                {i18n.t("login.forgot-password-message")}
              </Dialog.Description>
              <Dialog.Input
                ref={ref => {
                  this.state.inputModal = ref;
                }}
                onChangeText={e => {
                  this.updateText(e);
                }}
                placeholder={"Email"}
                wrapperStyle={{ borderWidth: 1, padding: 4 }}
              />
              <Dialog.Button label="Cancel" onPress={this.cancel} />
              <Dialog.Button label="Send" onPress={this.ok} />
            </Dialog.Container>
            <View
              style={[GlobalStyles.containerRow, { padding: 7 * this.pxr }]}
            >
              <View
                style={{
                  width: 60,
                  height: 68,
                  marginBottom:20,
                  marginTop:20,
                  alignItems: "center",
                  justifyContent: "center"
                }}
              >
                <Image
                  style={{
                    height: "155%",
                    width: "155%"
                  }}
                  source={require("../../resources/img/logoblanco.png")}
                />

              </View>
            </View>

            {this.state.formError != "" ? this.errorTemplate() : null}
            <Form>
              <View style={GlobalStyles.containerRow}>
                {/*  
                <Text style={GlobalStyles.generalText}>
                  {i18n.t("login.user-email")}
                </Text>
                */}
                <Email
                  changeFocus={this.changeInputFocus(0)}
                  update={this.updateCanRegisterStateEmail}
                  special
                  ref={ref => {
                    this.state.inputs[0] = ref;
                  }}
                  placeholderTextColor="white"
                />
              </View>
              <View
                style={[
                  GlobalStyles.containerRow,
                  { marginTop: 3.5 * this.pxr }
                ]}
              >
                {/*
                <Text style={GlobalStyles.generalText}>
                  {i18n.t("login.password")}
                </Text>
                 */}
                <Password
                  changeFocus={this.changeInputFocus(1)}
                  update={this.updateCanRegisterState}
                  returnKeyType="go"
                  special
                  ref={ref => {
                    this.state.inputs[1] = ref;
                  }}
                  placeholderTextColor="white"
                />
              </View>
              <View style={GlobalStyles.containerRow}>
                {this.state.loading ? (
                  this.show_loading()
                ) : (
                  <Btn
                    onPress={this.Login}
                    text={i18n.t("login.login-button")}
                    customStyle={{
                      borderRadius: 30,
                      paddingTop: this.isIphoneXorAbove(hp('1%'), hp('2%')),
                      flexDirection: "row",
                      width: "80%",
                      marginTop: 20,
                      width: "100%",
                      color: "#828282",
                      backgroundColor: "#fff"
                    }}
                  />

                )}
              </View>
            </Form>
            <View
              style={[
                GlobalStyles.containerRow,
                { paddingBottom: 30, marginTop: 5 }
              ]}
            >
              <TouchableOpacity onPress={this.forgot}>
                <Text
                  style={[
                    {
                      borderRadius: 0,
                      flexDirection: "row",
                      justifyContent: "flex-start",
                      alignSelf: "flex-start",
                      alignSelf: "center",
                      textDecorationLine: "underline",
                      marginTop: 10,
                      fontSize: 15
                    },
                    styles.forgot
                  ]}
                >
                  {i18n.t("login.forgot-password")}
                </Text>
              </TouchableOpacity>
            </View>
            <View style={{ flex: 1, flexDirection: "row" }}>
              <TouchableOpacity
                onPress={this.google_login}
                style={[styles.socialMedia, styles.socialMediaGoogle]}
              >
                <View
                  style={{
                    width: "20%",
                    paddingTop: 7,
                    paddingLeft: 5,
                    marginLeft : 22,
                    left : -20,
                    right : 0
                  }}
                >
                  <Image
                    style={[
                      GlobalStyles.containerRow,
                      GlobalStyles.backgroundColor
                    ]}
                    source={require("../../resources/img/google.png")}
                    style={{ width: 12, height: 12  }}
                  />
                </View>
                <View style={{ width: "90%" }}>
                  <Text
                    style={[
                      styles.textSocial,
                      {
                        textAlign: "left",
                        alignSelf: "flex-start",
                        paddingLeft: 10,
                        marginLeft : -10,
                        paddingTop: 3,
                        left : -30
                      }
                    ]}
                  >
                    {i18n.t("login.login_google")}
                  </Text>
                </View>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={this.facebook_login}
                style={[styles.socialMedia, styles.socialMediaFacebook]}
              >
                <View
                  style={{
                    width: "85%"
                  }}
                >
                  <Text
                    style={[
                      styles.textSocial,
                      { textAlign: "right", paddingTop: 3, right : -30}
                    ]}
                  >
                    {i18n.t("login.login_facebook")}
                  </Text>
                </View>
                <View
                  style={{
                    width: "15%",
                    padding: 5,
                    paddingTop: 7
                  }}
                >
                  <Image
                    style={[
                      GlobalStyles.containerRow,
                      GlobalStyles.backgroundColor,
                      { paddingTop: 7 * this.pxr }
                    ]}
                    source={require("../../resources/img/facebook.png")}
                    style={{ width: 5, height: 14, right : -6 }}
                  />
                </View>
              </TouchableOpacity>
            </View>
            <View
              style={[
                GlobalStyles.containerRow,
                { paddingTop: 40, paddingBottom: 10 }
              ]}
            >
              <Text style={{ color: "#fff", fontSize: 14 }}>
                {i18n.t("login.any-account")}
              </Text>
            </View>
            <View style={{ flex: 1, flexDirection: "row" }}>
              <View style={{ width: "8%" }} />
              <View
                style={{
                  width: "20%",
                  marginleft: 80,
                  borderTop: "solid",
                  borderColor: "#fff",
                  borderTopWidth: 1,
                  marginTop: 17
                }}
              />
              <View style={{ width: "40%" }}>
                <TouchableOpacity
                  style={styles.singIn}
                  onPress={this.goToRegister}
                >
                  <Text style={styles.textSignIn}>
                    {i18n.t("login.sign-in")}
                  </Text>
                </TouchableOpacity>
              </View>
              <View
                style={{
                  width: "20%",
                  marginleft: 80,
                  borderTop: "solid",
                  borderColor: "#fff",
                  borderTopWidth: 1,
                  marginTop: 17
                }}
              />
            </View>
            <View style={{ width: "8%" }} />
          </Animatable.View>
        </KeyboardAwareScrollView>
      </ImageBackground>
    );
  }
}
const styles = StyleSheet.create({
  socialMedia: {
    width: "45%",
    height: 30,
    borderColor: "#fff",
    borderWidth: 1,
    flex: 1,
    flexDirection: "row"
  },
  socialMediaFacebook: {
    borderLeftColor: "transparent",
    borderLeftWidth: 0,
    borderTopRightRadius: 5,
    borderBottomRightRadius: 5
  },
  socialMediaGoogle: {
    borderTopLeftRadius: 5,
    borderBottomLeftRadius: 5
  },
  forgot: {
    color: "#fff",
    fontSize: 18,
    alignSelf: "center",
    marginTop: 4
  },
  textSocial: {
    color: "#fff",
    fontSize: GlobalStyles.mediumText.fontSize,
    alignSelf: "center",
    marginTop: 4
  },
  textSignIn: {
    color: "#fff",
    alignSelf: "center",
    fontSize: 18
  },
  singIn: {
    borderRadius: 20,
    borderColor: "#fff",
    borderWidth: 1,
    padding: 5,
    paddingBottom: 7,
    width: "100%",
    alignItems: "center",
    justifyContent: "center"
  },
  imagenSocial: { width: 10, height: 10, flexDirection: "row" }
});

const Activity = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  horizontal: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10
  }
});

