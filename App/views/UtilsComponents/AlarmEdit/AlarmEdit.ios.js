import React, { Component } from "react";
import LinearGradient from "react-native-linear-gradient";
import { GlobalStyles } from "../../../resources/style/style";
import { withNavigation } from "react-navigation";
import { StyleSheet, View, TouchableOpacity, Text, Image } from "react-native";
import ToggleSwitch from "toggle-switch-react-native";

class AlarmEdit extends Component {
  constructor(props) {
    super(props);

    this.state = {
      inputRef: null,
      value: ""
    };
  }

  loadAudioList = async () => {
    //await AsyncStorage.removeItem("audios");
    await PlayList.get().then(data => {
      this.setState({
        alarmList: data
      });
    });
  };
  componentDidMount() {
    this.setState({
      active: this.props.alarm.active
    });
    console.log(this.props.alarm);
  }
  delete = async () => {
    try {
      this.props.onPress(this.props.alarm.id);
    } catch (e) {}
  };

  render() {
    return (
      <View style={[styles.container]}>
        <View style={{ width: "100%", height: 70, paddingLeft: 15 }}>
          <View style={[styles.card]}>
            <View style={{ width: "10%" }}>
              <TouchableOpacity
                style={{ paddingTop: 12 }}
                onPress={this.delete}
              >
                <Image
                  style={{ width: 23, height: 23 }}
                  source={require("../../../resources/img/delete.png")}
                />
              </TouchableOpacity>
            </View>
            <View style={{ width: "45%", paddingTop: 6, paddingLeft: 5 }}>
              <Text style={[styles.fistline, GlobalStyles.titleHeader]}>
                {this.props.alarm.textHour}
              </Text>
            </View>
            <View
              style={{
                width: "12%",
                paddingBottom: 22,
                paddingRight: 5,
                justifyContent: "flex-end",
                alignItems: "flex-end"
              }}
            />

            <View style={styles.day}>
              <Text
                style={[
                  this.props.alarm.sunday
                    ? styles.dayColorActive
                    : styles.dayColorInactive
                ]}
              >
                S
              </Text>
            </View>
            <View style={styles.day}>
              <Text
                style={[
                  this.props.alarm.monday
                    ? styles.dayColorActive
                    : styles.dayColorInactive
                ]}
              >
                M
              </Text>
            </View>
            <View style={styles.day}>
              <Text
                style={[
                  this.props.alarm.tuesday
                    ? styles.dayColorActive
                    : styles.dayColorInactive
                ]}
              >
                T
              </Text>
            </View>
            <View style={styles.day}>
              <Text
                style={[
                  this.props.alarm.wednesday
                    ? styles.dayColorActive
                    : styles.dayColorInactive
                ]}
              >
                W
              </Text>
            </View>
            <View style={styles.day}>
              <Text
                style={[
                  this.props.alarm.thursday
                    ? styles.dayColorActive
                    : styles.dayColorInactive
                ]}
              >
                T
              </Text>
            </View>
            <View style={styles.day}>
              <Text
                style={[
                  this.props.alarm.friday
                    ? styles.dayColorActive
                    : styles.dayColorInactive
                ]}
              >
                F
              </Text>
            </View>
            <View style={styles.day}>
              <Text
                style={[
                  this.props.alarm.saturday
                    ? styles.dayColorActive
                    : styles.dayColorInactive
                ]}
              >
                S
              </Text>
            </View>
            <View style={{ width: "5%" }}>
              <TouchableOpacity
                style={{ paddingTop: 15 }}
                onPress={() => {
                  this.props.navigation.navigate("AddAlarm", {
                    type: this.props.typeView,
                    id: this.props.idAudio,
                    typeFunction: "edit",
                    idAudio: this.props.alarm.id
                  });
                }}
              >
                <Image
                  style={{ width: 10.6, height: 21 }}
                  source={require("../../../resources/img/nextPage.png")}
                />
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    display: "flex",
    width: "100%",
    height: 70,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#f1f2f3"
  },
  buttonComponent: {
    width: 35.5,
    height: 65.2,
    zIndex: 100
  },
  ToolTipImage: {
    width: 8,
    height: 37,
    padding: 10,
    paddingLeft: 80,
    marginTop: 10
  },
  colorTootltipLabel: {
    color: "#cad0d4"
  },
  linearGradient: {
    width: "100%",
    height: 70,
    paddingLeft: 12
  },
  dayColorActive: {
    color: "#33a2f2",
    fontSize: 17
  },

  day: {
    width: "4%",
    paddingBottom: 24,
    paddingRight: 5,
    justifyContent: "flex-end",
    alignItems: "flex-end"
  },
  dayColorInactive: {
    color: "#cad0d4",
    fontSize: 17
  },

  borderLeft: {
    borderLeftWidth: 5,
    borderLeftColor: "#33a2f2"
  },
  comtainerFirstLine: {
    width: "25%",
    justifyContent: "flex-end",
    alignItems: "flex-end",
    paddingBottom: 12
  },

  containerRow: {
    display: "flex",
    width: "100%",
    height: 53,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#f1f2f3"
  },

  fistline: {
    paddingBottom: 1,
    fontSize: 16,
    color: "#828282"
  },
  secoundLine: {
    color: "#616770",
    fontSize: 11
  },
  title: {
    color: "#fff",
    fontSize: 25,
    paddingTop: 30,
    paddingLeft: 30
  },
  subTitle: {
    color: "#fff",
    fontSize: 12,
    paddingTop: 5,
    paddingLeft: 27
  },
  card: {
    width: "95%",
    paddingTop: 10,
    height: 10,
    flex: 1,
    flexDirection: "row",
    borderBottomColor: "#cad0d4",
    borderBottomWidth: 0.5
  },
  cardActiva: {
    width: "95%",
    paddingTop: 10,
    height: 10,
    flex: 1,
    flexDirection: "row",
    borderBottomColor: "#cad0d4",
    borderBottomWidth: 0.5
  }
});

export default withNavigation(AlarmEdit);
