import React, { Component } from "react";
import {
  Platform,
  Dimensions,
  StyleSheet,
  Text,
  View,
  Button,
  TouchableOpacity,
  StatusBar,
  TextInput,
  ScrollView,
  PixelRatio
} from "react-native";
import PropTypes from "prop-types";
import EStyleSheet from "react-native-extended-stylesheet";
import i18n from "../../../AppGlobalConfig/Localization/i18n";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import { TextInputMask } from "react-native-masked-text";

export default class Phone extends Component {
  constructor() {
    super();
    this.state = {
      inputRef: null,
      value: "",
      isCorrect: false,
      required: true,
      error: ""
    };

    let _pixelRatio = PixelRatio.get();
  }

  errorTemplate = ()=> {
      if(this.state.error){
        return (<Text style={{ textAlign: 'center', color:'red', fontSize:10  * this._pixelRatio}}>{this.state.error}</Text>);
      }else{
        return null;
      }
   }


  checkIfIsCorrect = () => {
    if (this.state.value !== "") {
      this.state.isCorrect = true;
      delete this.state.error;
    } else {
      if (this.state.value == "") {
        this.state.error = i18n.t("register.invalid_phone_field");
        this.state.isCorrect = false;
      } else {
        this.state.error = i18n.t("register.invalid_phone_field");
        this.state.isCorrect = false;
      }
    }

    this.setState(this.state);
    this.props.update(this.state.value);
  };

  clearInput = () => {
    this.state.inputRef._root.setNativeProps({ text: "" }); // eslint-disable-line
    this.setState({ isCorrect: 0, value: "" });
  };

  updateText = value => {
    this.state.value =  this.state.inputRef.getRawValue();
    this.state.isCorrect = true;
  };

  render() {
    return (
      <View style={{ width: "100%", padding: 4 }}>
        <TextInputMask
          style={[styles.input, EStyleSheet.create(this.props.customStyle)]}
          ref={ref => {
            this.state.inputRef = ref;
          }}
          type={'custom'}
          options={{
            mask: '(999) 999 - 9999'
          }}
          placeholder={this.props.placeholder}
          onChangeText={this.updateText}
          //onEndEditing={this.checkIfIsCorrect}
          placeholderTextColor={this.props.placeholderTextColor}
        />
        {this.state.error != "" ? this.errorTemplate() : null}
      </View>
    );
  }
}

isIphoneXorAbove = (s, n)=> {
  const dimen = Dimensions.get('window');
  
  if((
    Platform.OS === 'ios' &&
    !Platform.isPad &&
    !Platform.isTVOS &&
    ((dimen.height === 812 || dimen.width === 812) || (dimen.height === 896 || dimen.width === 896))
  )){

    return s;

  }

  return n;
}

const styles = StyleSheet.create({
  input: {
    color: "white",
    height: isIphoneXorAbove(hp('6%'), hp('8%')), // 70% of height device screen
    width: wp('95%'), 
    borderRadius: wp('6%'),
    borderWidth: 2,
    borderColor: "#fff",
    paddingLeft: 20,
    fontSize:16
  }
});

Phone.propTypes = {
  update: PropTypes.func.isRequired,
  changeFocus: PropTypes.func.isRequired,
  special: PropTypes.bool,
  placeholder: PropTypes.string
};
Phone.defaultProps = {
  special: false,
  placeholder: "Email",
  placeholderTextColor: "gray",
  customStyle: {}
};
