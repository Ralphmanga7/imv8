import React, { Component } from "react";
import { GlobalStyles } from "../../../resources/style/style";
import i18n from "../../../AppGlobalConfig/Localization/i18n";
import Search from "./search.ios";
import PopoverTooltip from "react-native-popover-tooltip";

import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ImageBackground,
  TextInput,
  AsyncStorage,
  Image
} from "react-native";

import { withNavigation } from "react-navigation";

import PropTypes from "prop-types";
class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inputRef: null,
      order: 1,
      isCorrect: 0,
      menu: 0,
      showPropover: true
    };
  }

  logout = async () => {
    await AsyncStorage.removeItem("userToken");
    this.props.navigation.navigate("AuthLoading");
  };

  help = () => {
    console.warn("help")
  };

  feedBack = () => {
    console.warn("feedback")
  };

  items = [
    {
      label: () => {
        return <Text style={GlobalStyles.colorTootltipLabel}>My account</Text>;
      },
      onPress: () => {}
    },
    {
      label: () => {
        return <Text style={GlobalStyles.colorTootltipLabel}>Help</Text>;
      },
      onPress: () => {
        this.help;
      }
    },
    {
      label: () => {
        return <Text style={GlobalStyles.colorTootltipLabel}>Feedback</Text>;
      },
      onPress: this.feedBack
    },
    {
      label: () => {
        return <Text style={GlobalStyles.colorTootltipLabel}>Logout</Text>;
      },
      onPress: async () => {
        await AsyncStorage.removeItem("userToken");
        this.props.navigation.navigate("AuthLoading");
      }
    }
  ];
  getTypeSearch = input => {
    this.props.updateSearch(input);
  };

  render() {
    return [
      <View style={styles.container}>
        <View style={styles.containerMenu}>
          <TouchableOpacity
            style={{ paddingRight: 20, paddingTop: 20, paddingLeft: 20,paddingBottom: 20 }}
            onPress={() => {
              this.refs["tooltip"].toggle(); // open popover tooltips one by one
            }}
          >
            <Image
              style={[{ width: 20, height: 17, top: 24 }]}
              source={require("../../../resources/img/menu.png")}
            />
          </TouchableOpacity>

          <PopoverTooltip
            ref="tooltip"
            items={this.items}
            labelSeparatorColor="#cad0d4"
            labelStyle="#cad0d4"
            /*overlayStyle={{ backgroundColor: "#21252912" }}
            delayLongPress={0}
            triangleOffset={0}*/
            tooltipContainerStyle={{
              padding: 10,
              marginLeft: -25,
              marginTop: -20
            }}
            animationType="timing"
            // using the default timing animation
          />
        </View>
        <View />

        <Text style={[styles.title, GlobalStyles.titleHeader]}>
          {this.props.title}
        </Text>
        <Text style={[styles.subTitle, GlobalStyles.mediumText]}>
          Vivamus sed vestibulum veilt
        </Text>
      </View>,
      <Search
        update={this.getTypeSearch}
        typeConsult={this.props.typeConsult}
      />
    ];
  }
}

export default withNavigation(Header);

Header.propTypes = {
  title: PropTypes.string
};

Header.defaultProps = {
  title: i18n.t("globals.default-title")
};

const styles = StyleSheet.create({
  container: {
    display: "flex",
    width: "100%",
    height: 135
  },
  containerMenu: {
    flexDirection: "row",
    width: "100%",
    alignItems: "flex-end",
    justifyContent: "flex-end",
    paddingTop: 10,
    paddingBottom: 10,
    height: 35
  },
  title: {
    color: "#fff",
    fontSize: 26,
    paddingTop: 18,
    paddingLeft: 20
  },
  subTitle: {
    color: "#fff",
    fontSize: 11,
    paddingTop: 0,
    paddingLeft: 20
  }
});
