import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Button,
  TouchableOpacity,
  StatusBar,
  Image
} from "react-native";
import AudioRecorderPlayer from "react-native-audio-recorder-player";
import Recorder from "./Components/RecorderComponent";
import i18n from "../../AppGlobalConfig/Localization/i18n";
import { withNavigation } from "react-navigation";
import * as Animatable from "react-native-animatable";
import Dialog from "react-native-dialog";

export default class RecorderView extends React.Component {
  static navigationOptions = {
    title: "IMV8 CORE",
    headerMode: "none"
  };

  constructor(props) {
    super(props);
  }

  componentDidMount() {}

  render() {
    return (
      <Animatable.View
        animation="fadeInRight"
        delay={1000}
        duration={500}
        style={[styles.container]}
      >
        <StatusBar backgroundColor="#333" barStyle="light-content" />
        <Recorder styles={styles.container} />
      </Animatable.View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center"
  },
  containerRow: {
    flexDirection: "row"
  },
  timerItem: {
    fontSize: 80,
    color: "white"
  }
});
