import React, { Component } from "react";
import { StyleSheet, View, Image, TouchableOpacity, Text } from "react-native";
import { GlobalStyles } from "../../../resources/style/style";
import Dialog from "react-native-dialog";

export default class AlertDialog extends Component {
  constructor() {
    super();
    this.state = {
      dialogVisible: false,
      name: ""
    };
  }

  showDialog = async () => {
    console.log("entro al show");
    console.log(this.state.dialogVisible);

    await this.setState({ dialogVisible: true });
  };

  handleCancel = () => {
    this.props.closeAlert();
  };

  rename = async () => {
    this.props.setValueName(this.state.name);
    this.props.closeAlert();
  };

  updateName = value => {
    this.setState({ name: value });
  };

  render() {
    return (
      <View>
        <Dialog.Container visible={this.props.visible} contentStyle={{}}>
          <Dialog.Title>Rename Affirmation</Dialog.Title>
          <Dialog.Description>
            Quisque at eros Namid varius nisi, atdignisim purus
          </Dialog.Description>
          <Dialog.Input
            onChangeText={this.updateName}
            value={this.state.text}
            wrapperStyle={{ borderWidth: 1 }}
          />
          <Dialog.Button label="Cancel" onPress={this.handleCancel} />
          <Dialog.Button label="Rename" onPress={this.rename} />
        </Dialog.Container>
      </View>
    );
  }
}

const styles = StyleSheet.create({});
